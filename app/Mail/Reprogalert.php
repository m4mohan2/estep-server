<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Reprogalert extends Mailable
{
    use Queueable, SerializesModels;
    public $analyst_name;
    public $proposal_id;
    public $logo;
    public $copyright;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->analyst_name = $data['analyst_name'];
        $this->proposal_id = $data['proposal_id'];
        $this->logo = env('LOGO_URL');

        $copyYear = 2019; // Set your website start date
        $curYear = date('Y'); // Keeps the second year updated

        $copyright =  $copyYear . (($copyYear != $curYear) ? '-' . $curYear : '');

        $this->copyright = $copyright;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Budget Reprogram Request')->view('emails.reprogalert');
        
        $this->withSwiftMessage(function ($data) {
            $data->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
        });
    }
}