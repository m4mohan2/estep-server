<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Profile</title>
</head>
<body style="padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:14px;">


<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left" valign="top" style="padding-top:20px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Categoría</td>
                </tr>
                <tr>
                    <td align="left" valign="top"><label>{{ $user->category }}</label></td>
                </tr>
                <tr>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Nombre del Administrador o Director</td>
                </tr>
                <tr>
                    <td align="left" valign="top"><label>{{ $user->name_of_director }}</label></td>
                </tr>
                <tr>
                  <td align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Nombre del Administrador o Director</td>
                </tr>
                <tr>
                    <td align="left" valign="top"><label>{{ $user->entity_name }}</label></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top">&nbsp;</td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="padding-right:15px;" width="60%"><label style="padding-bottom:10px; display:block;"><strong>Fecha de Incorporación</strong></label>
                        <label>{{ $user->dateofincorporation != null ? date('m-d-Y', strtotime($user->dateofincorporation)) : '' }}</label></td>
                    <td align="left" valign="top" style="padding-left:15px;"><label style="padding-bottom:10px; display:block;"><strong>Fecha de Fundación</strong></label>
                        <label>{{ $user->dateofregis != null ? date('m-d-Y', strtotime($user->dateofregis)) : '' }}</label></td>
                </tr>
            </table>
        </td>
    </tr>
    
    <tr>
        <td align="left" valign="top" style="padding-top:20px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Número de Incorporación</td></tr>
                 <tr>
                    <td align="left" valign="top"><label>{{$user->registrationnumber}}</label></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="padding-top:20px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Certificado de Exención Contributiva</td></tr>
                 <tr>
                    <td align="left" valign="top"><label>{{$user->registrationnumber == 1 ? "Si"  : "No"}}</label></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="padding-top:20px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="padding-right:15px;" width="60%"><label style="padding-bottom:10px; display:block;"><strong>Dirección de correo electrónico</strong></label>
                        <label>{{ $user->email}}</label></td>
                    <td align="left" valign="top" style="padding-left:15px;"><label style="padding-bottom:10px; display:block;"><strong>Número de teléfono</strong></label>
                        <label>{{ $user->phone }}</label></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="padding-top:20px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Dirección física</td></tr>
                 <tr>
                    <td align="left" valign="top"><label>{{$user->physicaladdr1}}</label></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="padding-top:20px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="padding-right:15px;" width="60%"><label style="padding-bottom:10px; display:block;"><strong>Ciudad</strong></label>
                        <label>{{ $city}}</label></td>
                    <td align="left" valign="top" style="padding-left:15px;"><label style="padding-bottom:10px; display:block;"><strong>Estado</strong></label>
                        <label>{{ $user->postal_state }}</label></td>
                    <td align="left" valign="top" style="padding-left:15px;"><label style="padding-bottom:10px; display:block;"><strong>Código postal</strong></label>
                        <label>{{ $user->zip }}</label></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="padding-top:20px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Direccion postal</td></tr>
                 <tr>
                    <td align="left" valign="top"><label>{{$user->postal_addr2}}</label></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="left" valign="top" style="padding-top:20px;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="padding-right:15px;" width="60%"><label style="padding-bottom:10px; display:block;"><strong>Ciudad</strong></label>
                        <label>{{ $postal_city}}</label></td>
                    <td align="left" valign="top" style="padding-left:15px;"><label style="padding-bottom:10px; display:block;"><strong>Estado</strong></label>
                        <label>{{ $user->postal_state }}</label></td>
                    <td align="left" valign="top" style="padding-left:15px;"><label style="padding-bottom:10px; display:block;"><strong>Código postal</strong></label>
                        <label>{{ $user->postal_zip }}</label></td>
                </tr>
            </table>
        </td>
    </tr>
    @if (count($members)) > 0
     <tr>
      <td align="left" valign="top"><h2 style="text-decoration:underline;text-align: center;">LISTA DE MIEMBROS</h2></td>
    </tr>
    <tr>
        <td align="left" valign="top" style="border-bottom: dashed 2px #225fb7;">&nbsp;</td>
    </tr>
    <tr>
      <td align="left" valign="top">
        <table width="100%">
          <thead>
            <tr>
              <th align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Nombre de miembro</th>
              <th align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Designación</th>
              <th align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Teléfono</th>
              <th align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Email</th>
              <th align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Dirección</th>
              <th align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Ciudad</th>
              <th align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Estado</th>
              <th align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Código postal</th>
            </tr>
          </thead>
          <tbody>
          @foreach($members as $each)
            <tr>
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $each->member_name}}</label></td>
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $each->designation }}</label>
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $each->telephone }}</label>
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $each->email }}</label>
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $each->address }}</label>
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $each->cityinfo->city }}</label>
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $each->state }}</label>
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $each->zip }}</label>
            </tr>
          @endforeach
          </tbody>
        </table>
      </td>
    </tr>
  @endif


</table>



</body>
</html>
