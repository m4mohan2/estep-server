<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Storage;

class Proposal extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        
        if($this->application_status == 0){
            $app_status_txt = 'Draft';
        }
        elseif($this->application_status == 1){
            $app_status_txt = 'Submitted';
        }
        elseif($this->application_status == 2){
            $app_status_txt = 'Waiting for approval';
        }
        elseif($this->application_status == 3){
            $app_status_txt = 'Need more Info';
        }
        elseif($this->application_status == 4){
            $app_status_txt = 'Rejected';
        }
        elseif($this->application_status == 5){
            $app_status_txt = 'Accepted';
        }

        return [
            'id' => $this->id,
            'proposal_unique_id' => $this->proposal_unique_id,
            'app_status_txt' => $app_status_txt,
            'created_at' => date("m/d/Y", strtotime($this->created_at)),
            'updated_at' => date("m/d/Y", strtotime($this->updated_at)),
        ];
    }
}
