<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProposalDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposal_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->comment('User parent Id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('proposal_id')->comment('Proposal parrent Id');
            $table->foreign('proposal_id')->references('id')->on('proposals')->onDelete('cascade');
            $table->string('document_type')->comment('Type of Document')->nullable();
            $table->text('desc')->comment('Description')->nullable();
            $table->string('file_name')->comment('File name')->nullable();
            $table->string('file_path')->comment('File path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposal_documents');
    }
}
