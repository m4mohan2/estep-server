<?php

namespace App\Http\Controllers;

use App\City;
use App\EntityMembers;
use App\Mail\Activestatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get the authenticated User.
     *
     * @return Response
     */
    public function profile()
    {
        return response()->json(['user' => Auth::user()], 201);
    }

    /**
     * Get all User.
     *
     * @return Response
     */
    public function allUsers(Request $request)
    {
        //return response()->json([$request->searchString], 201);die;
        //return response()->json([$request->get('export')], 201);die;

        $excelData    = array();
        $pageSize     = $request->input('pageSize') ? $request->input('pageSize') : '10';
        $language     = $request->get('language');
        $searchString = $request->get('searchString');

        $export = $request->get('export') == 1 ? true : false;

        $query = User::with(['role', 'city'])
            ->where('user_type', '=', '1');

        if ($searchString != '') {

            $query->where(function ($q) use ($searchString) {
                $q->where('entity_name', 'like', '%' . $searchString . '%')
                    ->orWhere('email', 'like', '%' . $searchString . '%')
                    ->orWhere('city', 'like', '%' . $searchString . '%')
                    ->orWhere('phone', 'like', '%' . $searchString . '%');
            });
        }

        if ($export) {

            $data = $query->get();
            $pcnt = 0;

            foreach ($data as $uval) {

                $cityName = City::find($uval->city);

                if ($uval->activeStatus == 1) {
                    $status   = 'Approved';
                    $statusEs = 'Aprobado';
                } else {
                    $status   = 'Not Approved';
                    $statusEs = 'No aprovado';
                }
                if ($language == 'en') {
                    $excelData[$pcnt]['Id']            = $uval->id;
                    $excelData[$pcnt]['EntityName']    = $uval->entity_name;
                    $excelData[$pcnt]['EntityAddress'] = $uval->physicaladdr1;
                    $excelData[$pcnt]['Phone']         = $uval->phone;
                    $excelData[$pcnt]['Email']         = $uval->email;
                    $excelData[$pcnt]['City']          = $cityName->city;
                    $excelData[$pcnt]['Status']        = $status;

                } elseif ($language == 'es') {
                    $excelData[$pcnt]['CarnéDeIdentidad'] = $uval->id;
                    $excelData[$pcnt]['NombreDeLaEntidad'] = $uval->entity_name;
                    $excelData[$pcnt]['Direcciónfísica'] = $uval->physicaladdr1;
                    $excelData[$pcnt]['Teléfono']         = $uval->phone;
                    $excelData[$pcnt]['Email']             = $uval->email;
                    $excelData[$pcnt]['Ciudad']            = $cityName->city;
                    $excelData[$pcnt]['Estado']            = $statusEs;
                }
                $pcnt++;
            }

            return response()->json([
                'excelData' => $excelData,
                'success'   => true,
            ], 200);

        } else {

            $query->orderBy('id', 'DESC');
            $data = $query->paginate($pageSize);

            return response()->json($data);
        }

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function editStatus(Request $request)
    {
        $this->validate($request, [
            'activeStatus' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $model               = User::find($request->input('id'));
            $model->activeStatus = $request->input('activeStatus');

            $model->save();
            $modelId = $model->id;
            //print_r($model);die;

            \DB::commit();
            $message = 'Status has been updated successfully!';
            $success = true;
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 201);

    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request)
    {
        //return response()->json(['user' => Auth::user()], 201);
        //validate incoming request
        $this->validate($request, [
            'email' => 'max:255|unique:users,email, ' . $request->input('id'),
        ]);

        $user_type = Auth::user()->user_type;

        \DB::beginTransaction();
        try {

            $user         = User::find($request->input('id'));
            $activeStatus = $user->activeStatus;
            $email        = $user->email;
            $name         = $user->entity_name;

            $user->email               = $request->input('email');
            $user->category            = $request->input('category');
            $user->name_of_director    = $request->input('name_of_director');
            $user->entity_name         = $request->input('entity_name');
            $user->dateofincorporation = date("Y-m-d", strtotime($request->input('dateofincorporation')));
            $user->dateofregis         = date("Y-m-d", strtotime($request->input('dateofregis')));
            //$user->taxidnumber = $request->input('taxidnumber');
            $user->registrationnumber  = $request->input('registrationnumber');
            $user->taxexmpt            = $request->input('taxexmpt');
            $user->taxexmptconfno      = $request->input('taxexmptconfno');
            $user->dateofissue         = date("Y-m-d", strtotime($request->input('dateofissue')));
            $user->phone               = $request->input('phone');
            $user->phone_ext           = $request->input('phone_ext');
            $user->mobile              = $request->input('mobile');
            $user->physicaladdr1       = $request->input('physicaladdr1');
            $user->physicaladdr2       = $request->input('physicaladdr2');
            $user->city                = $request->input('city');
            $user->state               = $request->input('state1');
            $user->zip                 = $request->input('zip');
            $user->telephone           = $request->input('telephone');
            $user->fax                 = $request->input('fax');
            $user->website             = $request->input('website') != '' ? $request->input('protocol') . "://" . $request->input('website') : '';
            $user->issameadd           = $request->input('issameadd');
            $user->postal_addr1        = $request->input('postal_addr');
            $user->postal_addr2        = $request->input('postal_addr2');
            $user->postal_city         = $request->input('postal_city');
            $user->postal_state        = $request->input('postal_state');
            $user->postal_zip          = $request->input('postal_zip');
            $user->captchaverified     = $request->input('captchaverified');
            $user->ipaddr              = $request->input('ipaddr');
            $user->activeStatus        = $request->input('activeStatus');
            $user->service_description = $request->input('service_description');
            $user->service_offered     = $request->input('service_offered');

            $cityName = DB::table('cities')->where('id', $request->input('city'))->pluck('city');

            //return response()->json($cityName[0]);

            $gmap_physicaladdr1 = str_replace(" ", "+", $request->input('physicaladdr1'));
            $gmap_physicaladdr2 = str_replace(" ", "+", $request->input('physicaladdr2'));
            $gmap_city          = str_replace(" ", "+", $cityName[0]);
            $gmap_state         = 'PR';
            $gmap_zip           = $request->input('zip');

            $gmap_fulladdress = $gmap_physicaladdr1 . '+' . $gmap_physicaladdr2 . '+' . $gmap_city . '+' . $gmap_state . '+' . $gmap_zip;

            $API_KEY = "AIzaSyCEoKaMHSeBnIzNpzEq9c5ZPWImE7lCIZ8";

            $googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?key=' . $API_KEY . '&address=' . urlencode($gmap_fulladdress);

            $gmap_result     = file_get_contents($googleApiUrl);
            $gmap_result_arr = json_decode(utf8_encode($gmap_result), true);

            //return response()->json($gmap_result_arr);

            if (isset($gmap_result_arr['results'][0]) && $gmap_result_arr['status'] == "OK") {
                $user->latitude  = $gmap_result_arr['results'][0]['geometry']['location']['lat'];
                $user->longitude = $gmap_result_arr['results'][0]['geometry']['location']['lng'];
            }

            if ($user->save()) {

                if ($user_type == 1) {
                    if ($request->hasFile('logo_name')) {

                        $file    = $request->file('logo_name');
                        $user_id = $user->id;

                        $fileCount = $this->moveLogo($file, $user_id);
                    }
                }

                if ($user_type == 0 && $activeStatus == 0 && $request->input('activeStatus') == 1) {
                    Mail::to($email)->send(new Activestatus($name));
                }

                \DB::commit();

                $this->insertAuditTrail('AUDIT_TRAIL.REGISTRATION', 'AUDIT_TRAIL.REGISTRATION_EDIT', 'Entity Code:  ' . $user->entity_code);

                //return successful response
                return response()->json(['message' => 'User Profile updated Successfully!!'], 201);
            } else {
                return response()->json(['message' => 'User Profile update Failed!'], 409);
            }

        } catch (\Exception $e) {
            //return error message
            //return response()->json(['message' => 'User Registration Failed!'], 409);
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    /**
     * Get all User.
     *
     * @return Response
     */
    public function getAllUsers(Request $request)
    {
        $userSearch = $request->get('search');

        $roles = DB::table('roles')
            ->select(DB::raw('id,roleName,roleKey'))
            ->orderBy('id')
            ->get();

        $i       = 0;
        $dataArr = array();
        foreach ($roles as $rkey => $rval) {
            if ($userSearch == 'admin') {
                $users = DB::table('users as u')
                    ->select(DB::raw('u.id, u.entity_name, u.admin_first_name, u.admin_last_name, r.roleKey, r.roleName'))
                    ->leftJoin('roles as r', 'r.id', '=', 'u.roleId')
                    ->where('u.roleId', '=', $rval->id)
                    ->where('u.user_type', '0')
                    ->whereNotIn('u.roleId', [1])
                    ->orderBy('users.admin_first_name')
                    ->orderBy('users.entity_name')
                    ->get();
            } else {
                $users = DB::table('users')
                    ->select(DB::raw('users.id, users.entity_name, users.admin_first_name, users.admin_last_name, roles.roleKey, roles.roleName'))
                    ->leftJoin('roles', 'roles.id', '=', 'users.roleId')
                    ->where('roleId', '=', $rval->id)
                    ->orderBy('users.admin_first_name')
                    ->orderBy('users.entity_name')
                    ->get();
            }

            if (count($users) > 0) {
                $dataUser = array();
                foreach ($users as $userkey => $userval) {
                    $dataUser[$userkey]['id'] = $userval->id;
                    if ($userval->entity_name == null) {
                        $dataUser[$userkey]['name'] = $userval->admin_first_name . ' ' . $userval->admin_last_name;
                    } else {
                        $dataUser[$userkey]['name'] = $userval->entity_name;
                    }
                }

                $dataArr[$i]['roleName'] = $rval->roleName;
                $dataArr[$i]['users']    = $dataUser;

                $i++;
            }
        }

        return response()->json([
            'data'    => $dataArr,
            'resCode' => 200,
            'success' => true,
        ], 200);
    }

    /**
     * Get all Assigned Users.
     *
     * @return Response
     */
    public function getAssignedUsers(Request $request)
    {
        $roles = DB::table('roles')
            ->select(DB::raw('id,roleName,roleKey'))
            ->whereNotIn('roleKey', ['system-admin'])
            ->orderBy('id')
            ->get();

        $dataArr = array();
        $i       = 0;
        foreach ($roles as $rkey => $rval) {

            $users = DB::table('users as u')
                ->select(DB::raw('u.id, u.entity_name, u.admin_first_name, u.admin_last_name, r.roleKey, r.roleName'))
                ->leftJoin('roles as r', 'r.id', '=', 'u.roleId')
                ->where('u.roleId', '=', $rval->id)
                ->where('u.user_type', '0')
                ->whereNotIn('u.roleId', [1])
                ->orderBy('u.admin_first_name')
                ->orderBy('u.entity_name')
                ->get();

            if (count($users) > 0) {
                $dataUser = array();
                foreach ($users as $userkey => $userval) {
                    $dataUser[$userkey]['id'] = $userval->id;
                    if ($userval->entity_name == null) {
                        $dataUser[$userkey]['name'] = $userval->admin_first_name . ' ' . $userval->admin_last_name;
                    } else {
                        $dataUser[$userkey]['name'] = $userval->entity_name;
                    }
                }

                $dataArr[$i]['roleName'] = $rval->roleName;
                $dataArr[$i]['users']    = $dataUser;

                $i++;
            }
        }

        return response()->json([
            'data'    => $dataArr,
            'resCode' => 200,
            'success' => true,
        ], 200);
    }

    /**
     * Get Admin User.
     *
     * @return Response
     */
    public function adminUserList(Request $request)
    {
        //return response()->json($request->search, 201);die;
        $searchString = $request->get('search');

        if ($request->search != '') {
            $users = User::with([
                'role', 'state', 'city',
            ])->where('user_type', '=', '0')
                ->Where('admin_first_name', 'like', '%' . $searchString . '%')
                ->orWhere('admin_last_name', 'like', '%' . $searchString . '%')
                ->orWhere('admin_phone', 'like', '%' . $searchString . '%')
                ->orWhere('email', 'like', $searchString)
                ->orderBy('id', 'DESC')
                ->get();

        } else {
            $users = User::with([
                'role', 'state', 'city',
            ])->where('user_type', '=', '0')->get();
        }

        return response()->json([
            'data'       => $users,
            'totalCount' => $users->count(),
            'page'       => 0,
            'resCode'    => 200,
            'success'    => true,
            'totalPage'  => 2,
        ], 200);
    }

    /**
     * Get Admin User.
     *
     * @return Response
     */
    public function allAdminUsersPaging(Request $request)
    {
        $pageSize = $request->input('pageSize');
        //return response()->json($request->search, 201);die;
        $searchString = $request->get('search');

        if ($request->search != '') {
            $users = User::with([
                'role', 'state', 'city',
            ])->where('user_type', '=', '0')
                ->Where('admin_first_name', 'like', '%' . $searchString . '%')
                ->orWhere('admin_last_name', 'like', '%' . $searchString . '%')
                ->orWhere('admin_phone', 'like', '%' . $searchString . '%')
                ->orWhere('email', 'like', $searchString)
                ->orderBy('id', 'DESC')
                ->paginate($pageSize);

        } else {
            $users = User::with([
                'role', 'state', 'city',
            ])->where('user_type', '=', '0')->paginate($pageSize);
        }

        return response()->json([
            'data'       => $users,
            'totalCount' => $users->count(),
            'success'    => true,
        ], 200);
    }

    /**
     * Store a newly created role in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addAdminUser(Request $request)
    {
        $this->validate($request, [
            'email'            => 'required|email|unique:users',
            'password'         => 'required|confirmed',
            'admin_state'      => 'required',
            'admin_city'       => 'required',
            'admin_address'    => 'required',
            'admin_phone'      => 'required',
            'admin_last_name'  => 'required',
            'admin_first_name' => 'required',
            'activeStatus'     => 'required',
            'roleId'           => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $model                   = new User;
            $model->admin_state      = $request->input('admin_state');
            $model->admin_city       = $request->input('admin_city');
            $model->admin_address    = $request->input('admin_address');
            $model->admin_phone      = $request->input('admin_phone');
            $model->admin_last_name  = $request->input('admin_last_name');
            $model->admin_first_name = $request->input('admin_first_name');
            $model->activeStatus     = $request->input('activeStatus');
            $model->roleId           = $request->input('roleId');
            $model->is_verified      = 1;

            $model->email    = $request->input('email');
            $plainPassword   = $request->input('password');
            $model->password = app('hash')->make($plainPassword);
            $model->vpass    = $request->input('password');

            $model->user_type  = 0;
            $model->created_by = $request->input('created_by');

            $model->save();
            $modelId = $model->id;

            //print_r($model);die;
            $admin_name = $request->input('admin_first_name') . ' ' . $request->input('admin_last_name');

            $this->insertAuditTrail('AUDIT_TRAIL.ADMIN_USER', 'AUDIT_TRAIL.ADD_ADMIN_USER', 'Admin User:  ' . $admin_name);

            \DB::commit();
            $message = 'Record has been added successfully!';
            $success = true;
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;

        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 201);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function updateAdminUser(Request $request, $id)
    {
        $this->validate($request, [
            'admin_state'      => 'required',
            'admin_city'       => 'required',
            'admin_address'    => 'required',
            'admin_phone'      => 'required',
            'admin_last_name'  => 'required',
            'admin_first_name' => 'required',
            'activeStatus'     => 'required',
            'roleId'           => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $model                   = User::find($id);
            $model->admin_state      = $request->input('admin_state');
            $model->admin_city       = $request->input('admin_city');
            $model->admin_address    = $request->input('admin_address');
            $model->admin_phone      = $request->input('admin_phone');
            $model->admin_last_name  = $request->input('admin_last_name');
            $model->admin_first_name = $request->input('admin_first_name');
            $model->activeStatus     = $request->input('activeStatus');
            $model->roleId           = $request->input('roleId');

            if ($request->input('password') != '') {
                $plainPassword   = $request->input('password');
                $model->password = app('hash')->make($plainPassword);
                $model->vpass    = $request->input('password');
            }

            $model->save();
            $modelId = $model->id;
            //print_r($model);die;

            $admin_name = $request->input('admin_first_name') . ' ' . $request->input('admin_last_name');

            $this->insertAuditTrail('AUDIT_TRAIL.ADMIN_USER', 'AUDIT_TRAIL.EDIT_ADMIN_USER', 'Admin User:  ' . $admin_name);

            \DB::commit();
            $message = 'Record has been updated successfully!';
            $success = true;
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 201);

    }

    /**
     * Get one user.
     *
     * @return Response
     */
    public function singleUser($id)
    {
        try {
            $user = User::findOrFail($id);

            return response()->json(['user' => $user], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }
    }

    public function post_change_password(Request $request)
    {
        $this->validate($request, [
            'oldPassword'     => 'required',
            'password'        => 'required|same:password',
            'confirmPassword' => 'required|same:password',
        ]);

        \DB::beginTransaction();
        try {
            $data = $request->all();

            $model = Auth::guard('api')->user();
            if (Hash::check($data['oldPassword'], $model->password)) {

                $plainPassword = $data['password'];

                $model->password = app('hash')->make($plainPassword);
                $model->vpass    = $plainPassword;
                if ($model->save()) {
                    \DB::commit();
                    $message = 'Your new password has been updated.';
                    $success = true;
                    $resCode = 200;
                } else {
                    $message = 'Please try again.';
                    $success = false;
                    $resCode = 400;
                }
            } else {
                $message = 'Please enter correct current password.';
                $success = false;
                $resCode = 400;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $resCode = 400;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);

        return response()->json($data_msg);
    }

    /**
     * Store a newly created Entity Member.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addEntityMember(Request $request)
    {
        $this->validate($request, [
            'member_name' => 'required',
            'designation' => 'required',
            'telephone'   => 'required',
            'address'     => 'required',
            'city'        => 'required',
            'state'       => 'required',
            'zip'         => 'required',
        ]);

        $user_id = Auth::user()->id;

        \DB::beginTransaction();

        try {

            $existUser = EntityMembers::where('designation', '=', $request->input('designation'))
                ->where('email', '=', $request->input('email'))
                ->where('user_id', '=', $user_id)
                ->first();

            if ($existUser === null) {

                $model              = new EntityMembers;
                $model->user_id     = $user_id;
                $model->member_name = $request->input('member_name');
                $model->designation = $request->input('designation');
                $model->telephone   = $request->input('telephone');
                $model->email       = $request->input('email');
                $model->address     = $request->input('address');
                $model->city        = $request->input('city');
                $model->state       = $request->input('state');
                $model->zip         = $request->input('zip');

                $model->save();
                $modelId = $model->id;

                $this->insertAuditTrail('AUDIT_TRAIL.ENTITY_MEMBER', 'AUDIT_TRAIL.ADD_ENTITY_MEMBER', 'Member Name:  ' . $request->input('member_name'));

                \DB::commit();
                $message = 'Entity member has been added successfully!';
                $success = true;
                $resCode = 200;

            } else {
                $message = 'Entity member is already exist.';
                $success = false;
                $resCode = 200;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $resCode = 400;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);

    }

    /**
     * Update the specified Entity Member.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function updateEntityMember(Request $request, $id)
    {
        $this->validate($request, [
            'member_name' => 'required',
            'designation' => 'required',
            'telephone'   => 'required',
            'address'     => 'required',
            'city'        => 'required',
            'state'       => 'required',
            'zip'         => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $existUser = EntityMembers::where('designation', '=', $request->input('designation'))
                ->where('email', '=', $request->input('email'))
                ->where('user_id', '=', Auth::user()->id)
                ->where('id', '!=', $id)
                ->first();

            if ($existUser === null) {

                $model              = EntityMembers::find($id);
                $model->member_name = $request->input('member_name');
                $model->designation = $request->input('designation');
                $model->telephone   = $request->input('telephone');
                $model->email       = $request->input('email');
                $model->address     = $request->input('address');
                $model->city        = $request->input('city');
                $model->state       = $request->input('state');
                $model->zip         = $request->input('zip');

                $model->save();

                $this->insertAuditTrail('AUDIT_TRAIL.ENTITY_MEMBER', 'AUDIT_TRAIL.EDIT_ENTITY_MEMBER', 'Member Name:  ' . $request->input('member_name'));

                \DB::commit();
                $message = 'Entity member updated successfully!';
                $success = true;
                $resCode = 200;

            } else {
                $message = 'Entity member is already exist.';
                $success = false;
                $resCode = 200;
            }

        } catch (\Exception $e) {

            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
            $resCode = 400;
        }

        return response()->json([
            'message' => $message,
            'success' => $success,
        ], $resCode);
    }

    /**
     * Get one Entity Member.
     *
     * @return Response
     */
    public function singleMember($id)
    {
        try {
            $member = EntityMembers::findOrFail($id);

            return response()->json(['member' => $member], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Member not found!'], 404);
        }
    }

    /**
     * Get Entity Member List.
     *
     * @return Response
     */
    public function getMembers($id)
    {
        try {
            $members = EntityMembers::with(['cityinfo'])->where('user_id', '=', $id)->get();

            return response()->json(['members' => $members], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Member not found!'], 404);
        }
    }

    /**
     * Delete Entity Member.
     *
     * @return Response
     */
    public function deleteMember($id)
    {
        $member = EntityMembers::find($id);
        $member->delete();

        $this->insertAuditTrail('AUDIT_TRAIL.ENTITY_MEMBER', 'AUDIT_TRAIL.DELETE_ENTITY_MEMBER', 'Member Name:  ' . $member->member_name);

        return response()->json([
            'message' => 'Member removed successfully',
            'success' => true,
        ], 200);
    }

    protected function moveLogo($file, $user_id)
    {

        $fileCount = count((array) $file);

        try {
            $user = User::find($user_id);

            $notMoveFileArr = array();

            $originalFileName = $file->getClientOriginalName();
            $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
            $extension        = strtolower($file->getClientOriginalExtension());
            $fileName         = date('YmdHmsu') . '-' . $filteredOrgName;

            $fileSize = $file->getSize();

            $fileSize = number_format($fileSize / 1048576, 2);

            $destinationPath = "upload/entity_logo/";
            $fullFilePath    = $destinationPath . $fileName;

            if ($file->move($destinationPath, $fileName)) {

                if ($fileSize > 6) {

                    $oldFullFilePath = $fullFilePath;

                    $fileName     = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                    $fullFilePath = $destinationPath . $fileName;

                    if ($extension == 'pdf') {

                        shell_exec('ps2pdf -dPDFSETTINGS=/default ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                    } else if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                        shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                    } /*else{
                    rename( $orgFullFilePath, $fullFilePath);
                    }*/

                    if (File::exists($oldFullFilePath)) {
                        File::delete($oldFullFilePath);
                    }
                }

                $user->org_logo_name = $filteredOrgName;
                $user->logo_name     = $fileName;

                $user->save();

            } else {
                $notMoveFileArr[] = $originalFileName;
            }
            return $notMoveFileArr;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

}
