<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::disableForeignKeyConstraints();
        Schema::table('proposals', function (Blueprint $table) {
            $table->bigInteger('project_id')->default(0)->after('user_id')->comment('Project table primary key');
            //$table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->double('requested_grant', 15, 8)->nullable()->after('completed_step')->comment('Grant Requested by Proponent');
            $table->double('donation_grant', 15, 8)->nullable()->after('requested_grant')->comment('Grant suggested by Analyst');
            $table->double('approved_grant', 15, 8)->nullable()->after('donation_grant')->comment('Approved Grant');
            $table->integer('open_status')->nullable()->after('application_status')->comment('Proposal reopen status');
            $table->tinyInteger('proposalAgree')->default(0)->after('open_status');
        });
        //Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->dropColumn('project_id');
            $table->dropColumn('requested_grant');
            $table->dropColumn('donation_grant');
            $table->dropColumn('approved_grant');
            $table->dropColumn('open_status');
            $table->dropColumn('proposalAgree');
        });
    }
}
