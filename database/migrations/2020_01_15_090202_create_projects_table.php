<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('projectUniqueId')->nullable();
            $table->string('projectNameEn')->nullable();
            $table->string('projectNameEs')->nullable();
            //$table->date('startDate')->nullable();
            //$table->date('endDate')->nullable();
            $table->timestamp('startDate')->default(\DB::raw('CURRENT_TIMESTAMP'))->nullable();
            $table->timestamp('endDate')->default(\DB::raw('CURRENT_TIMESTAMP'))->nullable();
            $table->string('period')->nullable()->comment('Financial Year');
            $table->integer('status')->nullable()->comment('0=>Inactive,1=>Active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
