<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;


class StateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth'); 
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        //$states = State::all();
        $states = State::with([
                    'cities'
            ])->get();

        return response()->json([
            'data' => $states ,
            'totalCount' => $states->count(), 
            'resCode' => 200,
            'success'=> true,
        ], 200);
    }

    public function create(Request $request)
    {
        $model = new State;
        $model->state = $request->state;
        $model->state_code = $request->state_code;

        $model->save();
        return response()->json($model);
    }

    public function show($id)
    {
        $state = State::find($id);
        return response()->json($state);
    }

    public function update(Request $request, $id)
    { 
        $model= State::find($id);

        $model->state = $request->input('state');
        $model->state_code = $request->input('state_code');
        $model->save();
        return response()->json($model);
    }
    
    public function destroy($id)
    {
        $model = State::find($id);
        $model->delete();
        return response()->json('state removed successfully');
    }
}
