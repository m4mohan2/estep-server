<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusColumnProposalDocs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposal_documents', function (Blueprint $table) {
            $table->enum('file_status',[0,1,2,3])->default(0)->after('proposal_id')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposal_documents', function (Blueprint $table) {
            $table->dropColumn('file_status');
        });
    }
}
