<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;


class CityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');    
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $citys = City::all();

        return response()->json([
            'data' => $citys ,
            'totalCount' => $citys->count(), 
            'resCode' => 200,
            'success'=> true,
        ], 200);
    }

    public function getCitySearch(Request $request) {

        //$citys = City::all();
        $citys = City::limit(1000)->get();
        $search = $request->get('search');

        if ($search!='') {
            //  $citys = City::where("state_code", "LIKE","%$search%")->get();         
            $citys = City::where('state_code','=',$search)->get();         
        }

        return response()->json([
            'data' => $citys ,
            'totalCount' => $citys->count(), 
            'resCode' => 200,
            'success'=> true,
        ], 200);   
    }

    public function create(Request $request)
    {
        $model = new City;
        $model->city = $request->city;
        $model->state_code = $request->state_code;

        $model->save();
        return response()->json($model);
    }

    public function show($id)
    {
        $city = City::find($id);
        return response()->json($city);
    }

    public function update(Request $request, $id)
    { 
        $model= City::find($id);

        $model->city = $request->input('city');
        $model->state_code = $request->input('state_code');
        $model->save();
        return response()->json($model);
    }
    
    public function destroy($id)
    {
        $model = City::find($id);
        $model->delete();
        return response()->json('city removed successfully');
    }
}
