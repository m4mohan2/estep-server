<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique()->notNullable();
            $table->string('password');
            $table->tinyInteger('user_type')->nullable()->comment('0=>Admin User,1=>Entity User');
            $table->string('category')->nullable()->comment('entity category');
            $table->string('name_of_director')->nullable()->comment('Name of Administrator or Director');
            $table->string('entity_name')->nullable();
            $table->string('entity_code')->nullable();
            $table->datetime('dateofincorporation')->nullable()->comment('Date of registration at Department of State of the Government of Puerto Rico');
            $table->datetime('taxidnumber')->nullable()->comment('IRS Federal Employer Identification Number');
            $table->string('registrationnumber')->nullable()->comment('Registration number at Department of State of the Government of Puerto Rico');
            $table->enum('taxexmpt',[0,1])->default(0)->nullable()->comment('1=>Yes,0=>No');
            $table->string('taxexmptconfno')->nullable()->comment('Tax Exemption Confirmation No.');
            $table->datetime('dateofissue')->nullable()->comment('Date of Issuance of Tax Exemption');
            $table->string('phone')->nullable();
            $table->string('phone_ext')->nullable();
            $table->string('mobile')->nullable();
            $table->string('physicaladdr1')->nullable();
            $table->string('physicaladdr2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('telephone')->nullable();
            $table->string('fax')->nullable();
            $table->string('website')->nullable();
            $table->enum('issameadd',[0,1])->default(0)->nullable();
            $table->string('postal_addr1')->nullable();
            $table->string('postal_addr2')->nullable();
            $table->string('postal_city')->nullable();
            $table->string('postal_state')->nullable();
            $table->string('postal_zip')->nullable();
            $table->integer('captchaverified')->nullable();
            $table->ipAddress('ipaddr')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
