<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="author" content="INT" />
	<meta name="description" content="Tangent" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title>Quaterly Report</title>
</head>

<body>
	<table align="center" border="0" cellpadding="0" cellspacing="0" style="padding: 0; margin: 0 auto; width:100%;">
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 10px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0;">
				<table align=" center" border="0" cellpadding="0" cellspacing="0"
					style="padding: 0; margin: 0 auto; width:100%;">
					<tr>
						<td style="padding: 0; margin: 0; width: 100px;">
							<img src="https://www.impactocomunitariopr.org/assets/images/logo.png" alt="" width="100px" />
						</td>
						<td
							style="padding: 0; margin: 0; text-align: center; font-size: 30px; line-height: 35px; font-weight: bold; font-family: Arial, Helvetica, sans-serif; color: #000; text-transform: uppercase;">
							Quarterly Report
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 20px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td width="50%"
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Entity Name</strong></div>
				<div>{{ $proposal->entity_name }}</div>
			</td>
			<td
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Entity Code</strong></div>
				<div>8158</div>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 10px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td width="50%"
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Application Date</strong></div>
				<div>04-04-2021{{ $proposal->community }}</div>
			</td>
			<td
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Proposal ID</strong></div>
				<div>{{ $proposal->proposal_unique_id }}</div>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 20px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2"
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Expense Summary</strong></div>
				<table align=" center" border="0" cellpadding="0" cellspacing="0"
					style="padding: 0; margin: 0 auto; width:100%; border:solid 1px #ccc; border-bottom: 0;">
					<tr>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
						</th>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
							Budget</th>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
							Q1</th>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
							Q2</th>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
							Q3</th>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
							Totals</th>
					</tr>
					<tr>
						<td valign="top" align="left" style="padding: 0; margin: 0; border-bottom:solid 1px #ccc;">
							<div style="padding: 10px; border-radius:5px;">Administrative</div>
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ 5,684.40
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc;  box-shadow: 0 0 3px #9a9a9a;">

						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">

						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">

						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ 0.00
						</td>
					</tr>
					<tr>
						<td valign="top" align="left" style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc;">
							Direct</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$
							8,526.60</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$
							0.00</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$
							0.00</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$
							0.00</td>
					</tr>
					<tr>
						<td valign="top" align="left" style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc;">
							Totals</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$
							14,211.00</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$
							0.00</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$
							0.00</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$
							0.00</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$
							0.00</td>
					</tr>
					<tr>
						<td valign="top" align="left" style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc;">
							Informed</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$
							0.00</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
					</tr>
					<tr>
						<td valign="top" align="left" style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc;">
							Net</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$
							14,211.00</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 20px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td width="50%"
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Report Status</strong></div>
				<div>Not Filled</div>
			</td>
			<td
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 20px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2"
				style="padding: 0; margin: 0; border:solid 1px #dfdfdf; font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">
				<div
					style="padding: 10px; color: #800020; font-size: 20px; background-color: #f7f7f7; border-bottom:solid 1px #dfdfdf;">
					Report Status
				</div>
				<div style="padding: 10px;">
					<table align=" center" border="0" cellpadding="0" cellspacing="0"
						style="padding: 0; margin: 0 auto; width:100%; border:solid 1px #ccc; border-bottom: 0;">
						<tr>
							<th valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
								Expenditure
							</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">Budget
							</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">Remain
								Amount</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">&nbsp;
							</th>
						</tr>
						<tr>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">SALARIO
								NETO COORDINADOR
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">$22164
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">$0
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								<div style="font-size: 18px; font-weight: bold; color: red;">&#9888;</div>
							</td>
						</tr>
						<tr>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								SALARIO
								NETO COORDINADOR
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								$22164
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">$0
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								<div style="font-size: 18px; font-weight: bold; color: green;">&#10003;</div>
							</td>
						</tr>
						<tr>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								SALARIO
								NETO COORDINADOR
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								$22164
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">$0
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								<div style="font-size: 18px; font-weight: bold; color: green;">&#10003;</div>
							</td>
						</tr>
						<tr>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								SALARIO
								NETO COORDINADOR
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								$22164
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">$0
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								<div style="font-size: 18px; font-weight: bold; color: green;">&#10003;</div>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2"
				style="padding: 0; margin: 0; border:solid 1px #dfdfdf; font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">
				<div
					style="padding: 10px; color: #800020; font-size: 20px; background-color: #f7f7f7; border-bottom:solid 1px #dfdfdf;">
					Direct Summary
				</div>
				<div style="padding: 10px;">
					<table align=" center" border="0" cellpadding="0" cellspacing="0"
						style="padding: 0; margin: 0 auto; width:100%; border:solid 1px #ccc; border-bottom: 0;">
						<tr>
							<th valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
								Expenditure
							</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">Budget
							</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">Remain
								Amount</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">&nbsp;
							</th>
						</tr>
						<tr>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">SALARIO
								NETO COORDINADOR
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">$22164
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">$0
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								<div style="font-size: 18px; font-weight: bold; color: green;">&#10003;</div>
							</td>
						</tr>
						<tr>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								SALARIO
								NETO COORDINADOR
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								$22164
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">$0
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								<div style="font-size: 18px; font-weight: bold; color: red;">&#9888;</div>
							</td>
						</tr>
						<tr>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								SALARIO
								NETO COORDINADOR
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								$22164
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">$0
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								<div style="font-size: 18px; font-weight: bold; color: green;">&#10003;</div>
							</td>
						</tr>
						<tr>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								SALARIO
								NETO COORDINADOR
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								$22164
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">$0
							</td>
							<td valign="top" align="center"
								style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
								<div style="font-size: 18px; font-weight: bold; color: red;">&#9888;</div>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2"
				style="padding: 0; margin: 0; border:solid 1px #dfdfdf; font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">
				<div
					style="padding: 10px; color: #800020; font-size: 20px; background-color: #f7f7f7; border-bottom:solid 1px #dfdfdf;">
					Evidence
				</div>
				<div style="padding: 10px;">
					<table align="center" border="0" cellpadding="0" cellspacing="0"
						style="padding: 0; margin: 0 auto; width:100%;">
						<tr>
							<td width="50%"
								style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
								<div style="padding: 0 0 5px 0;"><strong>Quarter</strong></div>
								<div>Q1</div>
							</td>
							<td
								style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding: 0; margin: 0; height: 20px;">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td width="50%"
								style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
								<div style="padding: 0 0 5px 0;"><strong>Classification</strong></div>
								<div>Bills</div>
							</td>
							<td
								style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
								<div style="padding: 0 0 5px 0;"><strong>Upload Documents</strong></div>
								<div>Bills</div>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding: 0; margin: 0; height: 20px;">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding: 0; margin: 0;">
								<table align=" center" border="0" cellpadding="0" cellspacing="0"
									style="padding: 0; margin: 0 auto; width:100%; border:solid 1px #ccc; border-bottom: 0;">
									<tr>
										<th width="10%" valign="top" align="center"
											style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
											ID
										</th>
										<th width="40%" valign="top" align="center"
											style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
											Evidence
										</th>
										<th width="10%" valign="top" align="center"
											style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
											R/O
										</th>
										<th width="40%" valign="top" align="center"
											style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
											Document
										</th>
									</tr>
									<tr>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											2081
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											Programmatic Report -- Required
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											R
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											Informe Programatico Legislatura ESCAPE julio a octubre 2020.pdf
										</td>
									</tr>
									<tr>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											2081
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											Programmatic Report -- Required
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											R
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											Informe Programatico Legislatura ESCAPE julio a octubre 2020.pdf
										</td>
									</tr>
									<tr>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											2081
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											Programmatic Report -- Required
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											R
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											Informe Programatico Legislatura ESCAPE julio a octubre 2020.pdf
										</td>
									</tr>
									<tr>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											2081
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											Programmatic Report -- Required
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											R
										</td>
										<td valign="top" align="center"
											style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
											Informe Programatico Legislatura ESCAPE julio a octubre 2020.pdf
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
</body>

</html>