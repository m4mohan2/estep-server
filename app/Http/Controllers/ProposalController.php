<?php
namespace App\Http\Controllers;

use App\AssignUserToProposal;
use App\Http\Resources\ProposalCollection;
use App\Mail\Assignusernotification;
use App\Mail\Reprogalert;
use App\Mail\Proposaladd;
use App\Mail\Proposalapprove;
use App\Mail\Proposalreject;
use App\Mail\Tightbudgetnotiadmin;
use App\Mail\Tightbudgetnotification;
use App\PareoCategory;
use App\PareoType;
use App\Proposal;
use App\ProposalDocument;
use App\Role;
use App\ReportedExpenses;
use App\BudgetBreakdown;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class ProposalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index1($search = null)
    {

        if ($search != '') {
            $proposals = Proposal::with(['cityinfo'])->where('proposal_unique_id', '=', $search)->paginate(5);
        } else {
            $proposals = Proposal::with(['cityinfo'])->paginate(5);
        }

        // with collection method
        /*return response()->json([
        'data' => new ProposalCollection($proposals) ,
        'totalCount' => $proposals->count(),
        'resCode' => 200,
        'success'=> true,
        ], 200);*/

        $news = new ProposalCollection($proposals);
        return response()->json($news);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pageSize = $request->input('pageSize');
        //$pageSize = 10;

        $searchQuery = $request->get('searchQuery');
        $export      = $request->get('export');

        $searchString = $request->get('searchString');
        $language     = $request->get('language');

        $application_status = $request->get('status');
        $startDate          = $request->get('startDate');
        $endDate            = $request->get('endDate');
        $myAssigned         = $request->get('myAssigned');
        $project_id         = $request->get('project_id');

        $user_id        = Auth::user()->id;
        $user_type      = Auth::user()->user_type;
        $loggedUserRole = Auth::user()->role->roleKey;

        //return response()->json($loggedUserRole);
        //$Allproposals = Proposal::with(['cityinfo']);

        $select = DB::table('proposals')
            ->leftJoin('cities', 'cities.id', '=', 'proposals.city')
            ->leftJoin('projects', 'projects.id', '=', 'proposals.project_id')
            ->leftJoin('users', 'proposals.user_id', '=', 'users.id');

        if ($project_id != '') {
            $select->where(function ($query) use ($project_id) {
                $query->where('proposals.project_id', '=', $project_id);
            });
        }

        if ($searchQuery == "true") {
            if ($user_type == 1) {
                // only for proponent

                if ($user_id != '') {
                    $select->where(function ($query) use ($user_id) {
                        $query->where('proposals.user_id', '=', $user_id);
                    });
                }

                if ($searchString != '') {

                    $select->where(function ($query) use ($searchString) {
                        $query->orWhere('proposals.proposal_unique_id', 'like', '%' . $searchString . '%')
                            ->orWhere('cities.city', 'like', '%' . $searchString . '%')
                            ->orWhere('users.entity_name', 'like', '%' . $searchString . '%');
                    });
                }

                $select->select('proposals.notifyUser', 'proposals.id', 'proposals.proposal_unique_id', 'proposals.application_status', 'proposals.created_at', 'proposals.updated_at', 'proposals.date', 'cities.city', 'proposals.project_id', 'projects.projectNameEn', 'projects.projectNameEs', 'proposals.requested_grant', 'proposals.approved_grant', 'proposals.reprogramStatus', 'proposals.reprogramRequest', 'proposals.reprogramCount', 'users.entity_name', 'users.email')->orderBy('proposals.updated_at', 'DESC');

            } else {
                // only for all admins

                if ($loggedUserRole != 'system-admin' && $loggedUserRole != 'sub-admin' && $loggedUserRole != 'analysts' && $loggedUserRole != 'analyst-2') {
                    // Not for system-admin AND sub-admin
                    //$select->leftJoin('assign_user_to_proposals AS aup', 'proposals.id', '=', 'aup.proposal_id')
                    //->where('aup.user_id', '=', $user_id);
                }

                if ($myAssigned == "true" && ($loggedUserRole == 'analysts' || $loggedUserRole == 'analyst-2')) {
                    $select->leftJoin('assign_user_to_proposals AS aup', 'proposals.id', '=', 'aup.proposal_id')
                        ->where('aup.user_id', '=', $user_id);
                }

                if ($searchString != '') {
                    $select->where('proposals.proposal_unique_id', 'like', '%' . $searchString . '%')
                        ->orWhere('cities.city', 'like', '%' . $searchString . '%')
                        ->orWhere('users.entity_name', 'like', '%' . $searchString . '%');

                }
                if ($startDate != '' && $endDate != '') {
                    $select->whereBetween('proposals.updated_at', [$startDate, $endDate]);
                }

                if ($application_status != '') {
                    $select->where('proposals.application_status', $application_status);
                }

                if ($loggedUserRole == 'inspectors') {
                    // only for Inspectors
                    $select->where('proposals.application_status', '=', '5');
                }

                $select->where('proposals.application_status', '!=', '0');

                $select->select('proposals.notifyUser', 'proposals.id', 'proposals.proposal_unique_id','proposals.edit_read', 'proposals.application_status', 'proposals.created_at', 'proposals.updated_at', 'proposals.date', 'cities.city', 'proposals.project_id', 'projects.projectNameEn', 'projects.projectNameEs', 'proposals.requested_grant', 'proposals.approved_grant', 'proposals.reprogramStatus', 'proposals.reprogramRequest', 'proposals.reprogramCount', 'users.entity_name', 'users.email', 'users.physicaladdr1', 'users.phone')
                    ->orderBy('proposals.updated_at', 'DESC');

            }

        } else {
            if ($user_type == 1) {
                // only for proponent

                $select
                    ->where('proposals.user_id', '=', $user_id)
                    ->select('proposals.notifyUser', 'proposals.id', 'proposals.proposal_unique_id', 'proposals.edit_read', 'proposals.application_status', 'proposals.created_at', 'proposals.updated_at', 'proposals.date', 'cities.city', 'proposals.project_id', 'projects.projectNameEn', 'projects.projectNameEs', 'proposals.requested_grant', 'proposals.approved_grant', 'proposals.reprogramStatus', 'proposals.reprogramRequest', 'proposals.reprogramCount', 'users.entity_name', 'users.email', 'users.physicaladdr1', 'users.phone')
                    ->orderBy('proposals.updated_at', 'DESC');

            } else {
                // only for all admins

                if ($loggedUserRole == 'system-admin' || $loggedUserRole == 'sub-admin') {
                    // only for system-admin AND sub-admin
                    $select
                        ->where('proposals.application_status', '!=', '0')
                        ->select('proposals.notifyUser', 'proposals.id', 'proposals.proposal_unique_id', 'proposals.edit_read', 'proposals.application_status', 'proposals.created_at', 'proposals.updated_at', 'proposals.date', 'cities.city', 'proposals.project_id', 'projects.projectNameEn', 'projects.projectNameEs', 'proposals.requested_grant', 'proposals.approved_grant', 'proposals.reprogramStatus', 'proposals.reprogramRequest', 'proposals.reprogramCount', 'users.entity_name', 'users.email', 'users.physicaladdr1', 'users.phone')
                        ->orderBy('proposals.updated_at', 'DESC');

                } else {
                    // only for assign user

                    if ($loggedUserRole == 'inspectors') {
                        // // only for Inspectors
                        $select
                            ->leftJoin('assign_user_to_proposals AS aup', 'proposals.id', '=', 'aup.proposal_id')
                            ->where('aup.user_id', '=', $user_id)
                            ->where('proposals.application_status', '=', '5')
                            ->where('proposals.application_status', '!=', '0')
                            ->select('proposals.notifyUser', 'proposals.id', 'proposals.proposal_unique_id', 'proposals.edit_read', 'proposals.application_status', 'proposals.created_at', 'proposals.updated_at', 'proposals.date', 'cities.city', 'proposals.project_id', 'projects.projectNameEn', 'projects.projectNameEs', 'proposals.requested_grant', 'proposals.approved_grant', 'proposals.reprogramStatus', 'proposals.reprogramRequest', 'proposals.reprogramCount', 'users.entity_name', 'users.email', 'users.physicaladdr1', 'users.phone')
                            ->orderBy('proposals.updated_at', 'DESC');

                    } else {
                        if ($loggedUserRole == 'analysts' || $loggedUserRole == 'analyst-2') {
                            // only for Anaylist and Analyst 2
                            if ($myAssigned == "true") {
                                $select
                                    ->leftJoin('assign_user_to_proposals AS aup', 'proposals.id', '=', 'aup.proposal_id')
                                    ->where('aup.user_id', '=', $user_id)
                                    ->where('proposals.application_status', '!=', '0')
                                    ->select('proposals.notifyUser', 'proposals.id', 'proposals.proposal_unique_id', 'proposals.edit_read', 'proposals.application_status', 'proposals.created_at', 'proposals.updated_at', 'proposals.date', 'cities.city', 'proposals.project_id', 'projects.projectNameEn', 'projects.projectNameEs', 'proposals.requested_grant', 'proposals.approved_grant', 'proposals.reprogramStatus', 'proposals.reprogramRequest', 'proposals.reprogramCount', 'users.entity_name', 'users.email', 'users.physicaladdr1', 'users.phone')
                                    ->orderBy('proposals.updated_at', 'DESC');
                            } else {
                                $select
                                    ->where('proposals.application_status', '!=', '0')
                                    ->select('proposals.notifyUser', 'proposals.id', 'proposals.proposal_unique_id', 'proposals.edit_read', 'proposals.application_status', 'proposals.created_at', 'proposals.updated_at', 'proposals.date', 'cities.city', 'proposals.project_id', 'projects.projectNameEn', 'projects.projectNameEs', 'proposals.requested_grant', 'proposals.approved_grant', 'proposals.reprogramStatus', 'proposals.reprogramRequest', 'proposals.reprogramCount', 'users.entity_name', 'users.email', 'users.physicaladdr1', 'users.phone')
                                    ->orderBy('proposals.updated_at', 'DESC');
                            }

                        } else {
                            // only for others

                            $select
                                ->leftJoin('assign_user_to_proposals AS aup', 'proposals.id', '=', 'aup.proposal_id')
                                //->where('aup.user_id', '=', $user_id)
                                ->where('proposals.application_status', '!=', '0')
                                ->select('proposals.notifyUser', 'proposals.id', 'proposals.proposal_unique_id', 'proposals.edit_read','proposals.application_status', 'proposals.created_at', 'proposals.updated_at', 'proposals.date', 'cities.city', 'proposals.project_id', 'projects.projectNameEn', 'projects.projectNameEs', 'proposals.requested_grant', 'proposals.approved_grant', 'proposals.reprogramStatus', 'proposals.reprogramRequest', 'proposals.reprogramCount', 'users.entity_name', 'users.email', 'users.physicaladdr1', 'users.phone')
                                ->orderBy('proposals.updated_at', 'DESC');
                        }

                    }

                }
            }
        }

        if ($export) {
            $proposals = $select->get();
        } else {
            $proposals = $select->paginate($pageSize);
        }

        /*** To display application status in text format***/
        $excelData = array();
        $pcnt      = 0;

        foreach ($proposals as $pval) {

            //$analyst = $this->assignUser($pval->id);

            $assignUser = DB::table('assign_user_to_proposals AS aup')
                ->leftJoin('users', 'users.id', '=', 'aup.user_id')
                ->where('aup.proposal_id', '=', $pval->id)
                ->where('aup.status', '=', 1)
                ->orderBy('aup.updated_at', 'DESC')
                ->select('users.id', 'users.admin_first_name', 'users.admin_last_name')
                ->first();

            $proposals[$pcnt]->assignUser = $assignUser;

            if ($pval->application_status == 0) {
                $proposals[$pcnt]->app_status_txt    = 'Draft';
                $proposals[$pcnt]->app_status_txt_es = 'Pendiente';
            } elseif ($pval->application_status == 1) {
                $proposals[$pcnt]->app_status_txt    = 'Submitted';
                $proposals[$pcnt]->app_status_txt_es = 'Presentada';
            } elseif ($pval->application_status == 2) {
                $proposals[$pcnt]->app_status_txt    = 'Waiting for approval';
                $proposals[$pcnt]->app_status_txt_es = 'A la espera de la aprobación';
            } elseif ($pval->application_status == 3) {
                $proposals[$pcnt]->app_status_txt    = 'Need more Info';
                $proposals[$pcnt]->app_status_txt_es = 'Necesita más información';
            } elseif ($pval->application_status == 4) {
                $proposals[$pcnt]->app_status_txt    = 'Rejected';
                $proposals[$pcnt]->app_status_txt_es = 'Rechazado';
            } elseif ($pval->application_status == 5) {
                $proposals[$pcnt]->app_status_txt    = 'Accepted';
                $proposals[$pcnt]->app_status_txt_es = 'Aceptado';
            } elseif ($pval->application_status == 6) {
                $proposals[$pcnt]->app_status_txt    = 'Evaluation Complete';
                $proposals[$pcnt]->app_status_txt_es = 'Evaluación completa';
            } elseif ($pval->application_status == 7) {
                $proposals[$pcnt]->app_status_txt    = 'Grant Declined';
                $proposals[$pcnt]->app_status_txt_es = 'Concesión rechazada';
            }

            if ($language == 'en') {

                $excelData[$pcnt]['ApplicationId']     = $pval->proposal_unique_id;
                $excelData[$pcnt]['EntityName']        = $pval->entity_name;
                $excelData[$pcnt]['EntityEmail']       = $pval->email;
                $excelData[$pcnt]['EntityAddress']     = $pval->physicaladdr1;
                $excelData[$pcnt]['EntityPhone']       = $pval->phone;
                $excelData[$pcnt]['City']              = $pval->city;
                $excelData[$pcnt]['ProjectName']       = $pval->projectNameEn;
                $excelData[$pcnt]['ApplicationStatus'] = $proposals[$pcnt]->app_status_txt;
                $excelData[$pcnt]['AssignUser']        = $assignUser;
                $excelData[$pcnt]['ApproveGrant']      = $proposals[$pcnt]->approved_grant;
                $excelData[$pcnt]['CreatedDate']       = $pval->created_at;
                $excelData[$pcnt]['UpdatedDate']       = $pval->updated_at;

            } elseif ($language == 'es') {

                $excelData[$pcnt]['IdDeAplicación']       = $pval->proposal_unique_id;
                $excelData[$pcnt]['Nomdelentité']         = $pval->entity_name;
                $excelData[$pcnt]['CorreoElecrónico']     = $pval->email;
                $excelData[$pcnt]['Direcciónfísica']     = $pval->physicaladdr1;
                $excelData[$pcnt]['Númerodeteléfono']    = $pval->phone;
                $excelData[$pcnt]['Ciudad']                = $pval->city;
                $excelData[$pcnt]['NombreDelProyecto']     = $pval->projectNameEs;
                $excelData[$pcnt]['EstadoDeLaAplicación'] = $proposals[$pcnt]->app_status_txt_es;
                $excelData[$pcnt]['AsignarUsuario']        = $assignUser;
                $excelData[$pcnt]['Approuverlasubvention'] = $proposals[$pcnt]->approved_grant;
                $excelData[$pcnt]['FechaDeCreación']      = $pval->created_at;
                $excelData[$pcnt]['FechaActualizada']      = $pval->updated_at;
            }

            $pcnt++;
        }

        //print_r($proposalArr);
        //print_r($proposals);
        //die;

        return response()->json([
            'data'       => $proposals,
            'excelData'  => $excelData,
            'totalCount' => $proposals->count(),
            'resCode'    => 200,
            'success'    => true,
        ], 200);
    }

    public function getBlockedProjetList(Request $request)
    {
        //$project_id = $request->get('project_id');
        /*$proposalExit = DB::table('proposals')
            ->leftJoin('projects', 'projects.id', '=', 'proposals.project_id')
            ->where('proposals.user_id', '=', Auth::user()->id)
            ->where('proposals.user_id', '!=', 2)
            ->where('proposals.project_id', '!=', $project_id)
            ->select('projects.id')
            ->distinct()
            ->get();*/


            $proposalExit = DB::table('proposals')
            ->leftJoin('projects', 'projects.id', '=', 'proposals.project_id')
            ->where('proposals.user_id', '=', Auth::user()->id)
            ->select('projects.id')
            ->get();
        return response()->json([
            'data'    => $proposalExit,
            'resCode' => 200,
            'success' => true,
        ], 200);
    }

    /**
     * Store a newly created proposal in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        //return response()->json($request->necessity_statement_file->getClientOriginalName());

        /*$proposalExit = Proposal::where('user_id', '=', $request->input('user_id'))
        ->where('project_id', '=', $request->input('project_id'))
        ->where('application_status','!=','5')
        ->orderBy('id', 'DESC')
        ->get();

        if( $proposalExit->count() > 0 ) {

        return response()->json([
        'message' => 'Proposal already exist for this project',
        'success'=> false,
        'count'=> $proposalExit->count()
        ], 400);
        } */

        //return response()->json($request->population);
        $this->validate($request, [
            'user_id'               => 'required',
            'city'                  => 'required',
            'community'             => 'required',
            'categories'            => 'required',
            'necessity_statement'   => 'required',
            //'programmatic_approach' => 'required',
        ]);

        //print_r($_POST);die;
        \DB::beginTransaction();
        try {

            $model = new Proposal;
            //$model->proposal_unique_id  =  bin2hex(openssl_random_pseudo_bytes(15));
            $model->user_id               = $request->input('user_id');
            $model->project_id            = $request->input('project_id');
            $model->city                  = $request->input('city');
            $model->town                  = $request->input('town');
            $model->population            = $request->input('population');
            $model->community             = $request->input('community');
            $model->categories            = $request->input('categories');
            $model->necessity_statement   = $request->input('necessity_statement');
            $model->programmatic_approach = $request->input('programmatic_approach');
            $model->pareo_type            = $request->input('pareo_type');
            $model->pareo_origin          = $request->input('pareo_origin');
            $model->pareo_category        = $request->input('pareo_category');

            //$model->save();
            if ($model->save()) {
                $model->proposal_unique_id = 500000 + $model->id;
                $model->save();
            }

            $modelId = $model->id;

            if ($request->hasFile('necessity_statement_file')) {

                $file          = $request->file('necessity_statement_file');
                $user_id       = $model->user_id;
                $proposal_id   = $model->id;
                $document_type = 'necessity_statement_file';

                $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type);
            }

            //print_r($model);die;

            \DB::commit();
            $message = "MESSAGE.PROPOSAL_ADD_SUCCESS";
            $success = true;
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = 'error';
            return response()->json(['message' => $e->getMessage()], 409);
        }

        //Return message
        return response()->json([
            'message' => $message,
            'data'    => $modelId,
            'success' => $success,
        ], 201);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Proposal  $role
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $role = Role::find(Auth::user()->roleId);
        $loggedUserRole = $role->roleKey;

        $proposal = Proposal::with('project', 'proposalDocuments', 'proposalUser.city', 'assignUser', 'budgetBreakdown', 'reportedExpenses', 'proposalMember', 'logs.logUser')->find($id);

        
        if($loggedUserRole!= 'companies'){
            $model = Proposal::find($id);
            $model->edit_read = 0;
            $model->save();
        }


        return response()->json([
            'data'       => $proposal,
            'totalCount' => $proposal->count(),
            'resCode'    => 200,
            'success'    => true,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proposal  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return response()->json($request->population);
        $this->validate($request, [
            /*'goal_to_achieve' => 'required',
        'changes_or_benefits' => 'required',
        'specific_activities' => 'required',
        'who_will_affected' => 'required',
        'how_will_pay' => 'required',
        'resources_required' => 'required',
        'work_plan' => 'required',
        'collecting_relevant_data' => 'required',
        'how_often_organization_evaluates' => 'required',
        'who_evaluates' => 'required',
        'statements_describes_type_of_evaluations ' => 'required',
        'type_of_evaluation ' => 'required',
        'type_of_data' => 'required',
        'type_of_evaluator' => 'required',
        'inventory_of_property' => 'required',
        'name' => 'required',
        'date' => 'required',*/
        ]);

        \DB::beginTransaction();
        try {

            $role = Role::find(Auth::user()->roleId);

            $loggedUserRole = $role->roleKey;
            //return response()->json($loggedUserRole);

            $model = Proposal::find($id);
            //print_r(json_decode(stripslashes($model->population),true)); die;
            if ($request->input('step') > $model->completed_step) {
                $model->completed_step = $request->input('step');
            }
            //$model->application_status = 0;

            // 1nd step
            if ($request->input('step') == 1) {

                if ($request->input('project_id') !== 'undefined') {
                    $model->project_id = $request->input('project_id');
                }
                if ($request->input('city') !== 'undefined') {
                    $model->city = $request->input('city');
                }

                $model->town                = $request->input('town');
                $model->population          = $request->input('population');
                $model->community           = $request->input('community');
                $model->categories          = $request->input('categories');
                $model->necessity_statement = $request->input('necessity_statement');

                if ($request->input('programmatic_approach') !== 'undefined') {
                    $model->programmatic_approach = $request->input('programmatic_approach');
                }
                if ($request->input('pareo_type') !== 'undefined') {
                    $model->pareo_type = $request->input('pareo_type');
                }
                $model->pareo_origin = $request->input('pareo_origin');

                if ($request->input('pareo_category') !== 'undefined') {
                    $model->pareo_category = $request->input('pareo_category');
                }

                $model->population_status   = $request->input('population_status');
                $model->necessity_status    = $request->input('necessity_status');
                $model->programmatic_status = $request->input('programmatic_status');

                if ($request->hasFile('necessity_statement_file')) {

                    $file          = $request->file('necessity_statement_file');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'necessity_statement_file';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type);
                }

                $this->insertAuditTrail('AUDIT_TRAIL.PROPOSAL', 'AUDIT_TRAIL.STEP1', 'Application Id:  ' . $model->proposal_unique_id);
            }

            // 2nd step
            if ($request->input('step') == 2) {

                $model->goal_to_achieve     = $request->input('goal_to_achieve');
                $model->changes_or_benefits = $request->input('changes_or_benefits');
                $model->specific_activities = $request->input('specific_activities');
                $model->who_will_affected   = $request->input('who_will_affected');
                $model->how_will_pay        = $request->input('how_will_pay');
                $model->resources_required  = $request->input('resources_required');
                $model->work_plan           = $request->input('work_plan');

                $model->goal_status      = $request->input('goal_status') != '' ? $request->input('goal_status') : 0;
                $model->changes_status   = $request->input('changes_status') != '' ? $request->input('changes_status') : 0;
                $model->specific_status  = $request->input('specific_status') != '' ? $request->input('specific_status') : 0;
                $model->affected_status  = $request->input('affected_status') != '' ? $request->input('affected_status') : 0;
                $model->pay_status       = $request->input('pay_status') != '' ? $request->input('pay_status') : 0;
                $model->resources_status = $request->input('resources_status') != '' ? $request->input('resources_status') : 0;
                $model->work_plan_status = $request->input('work_plan_status') != '' ? $request->input('work_plan_status') : 0;

                if ($request->hasFile('work_plan_file')) {

                    $file          = $request->file('work_plan_file');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'work_plan_file';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type);
                }

                $this->insertAuditTrail('AUDIT_TRAIL.PROPOSAL', 'AUDIT_TRAIL.STEP2', 'Application Id:  ' . $model->proposal_unique_id);
            }

            // 3rd step
            if ($request->input('step') == 3) {
                $model->collecting_relevant_data                 = $request->input('collecting_relevant_data');
                $model->how_often_organization_evaluates         = $request->input('how_often_organization_evaluates');
                $model->who_evaluates                            = $request->input('who_evaluates');
                $model->statements_describes_type_of_evaluations = $request->input('statements_describes_type_of_evaluations');
                if ($request->input('type_of_evaluation') !== 'undefined') {
                    $model->type_of_evaluation = $request->input('type_of_evaluation');
                }
                if ($request->input('type_of_data') !== 'undefined') {
                    $model->type_of_data = $request->input('type_of_data');
                }
                if ($request->input('type_of_evaluator') !== 'undefined') {
                    $model->type_of_evaluator = $request->input('type_of_evaluator');
                }

                $model->collecting_status                               = $request->input('collecting_status') != '' ? $request->input('collecting_status') : 0;
                $model->who_evaluates_status                            = $request->input('who_evaluates_status') != '' ? $request->input('who_evaluates_status') : 0;
                $model->organization_evaluates_status                   = $request->input('organization_evaluates_status') != '' ? $request->input('organization_evaluates_status') : 0;
                $model->statements_describes_type_of_evaluations_status = $request->input('statements_describes_type_of_evaluations_status') != '' ? $request->input('statements_describes_type_of_evaluations_status') : 0;
                $model->type_of_evaluation_status                       = $request->input('type_of_evaluation_status') != '' ? $request->input('type_of_evaluation_status') : 0;
                $model->type_of_data_status                             = $request->input('type_of_data_status') != '' ? $request->input('type_of_data_status') : 0;
                $model->type_of_evaluator_status                        = $request->input('type_of_evaluator_status') != '' ? $request->input('type_of_evaluator_status') : 0;

                $this->insertAuditTrail('AUDIT_TRAIL.PROPOSAL', 'AUDIT_TRAIL.STEP3', 'Application Id:  ' . $model->proposal_unique_id);

            }

            // 4th step / Prop
            if ($request->input('step') == 4) {
                $model->inventory_of_property = $request->input('inventory_of_property');
                $model->inventory_file_status = $request->input('inventory_file_status') != '' ? $request->input('inventory_file_status') : 0;

                $monthly_payment = $request->input('monthly_payment');
                $rented          = $request->input('rented') === "true" ? 1 : 0;

                $user_id       = $model->user_id;
                $document_type = 'property_inventory_file';
                $proposal_id   = $model->id;
                if ($request->hasFile('property_inventory_file')) {

                    $file = $request->file('property_inventory_file');
                    //$document_type = preg_replace("![^a-z0-9]+!i", "_", strtolower($model->inventory_of_property));

                    $fileCount = $this->moveFileProp($file, $user_id, $proposal_id, $document_type, $model->inventory_of_property, $monthly_payment, $rented);
                } else {
                    if ($model->inventory_of_property != "") {
                        $this->saveToDocuments($user_id, $proposal_id, $document_type, $model->inventory_of_property, $monthly_payment, $rented);
                    }
                }

                $this->insertAuditTrail('AUDIT_TRAIL.PROPOSAL', 'AUDIT_TRAIL.STEP5', 'Application Id:  ' . $model->proposal_unique_id);
            }

            // 5th step /Budget
            if ($request->input('step') == 5) {
                $user_type          = Auth::user()->user_type;
                $proposal_id        = $model->id;
                $application_status = $model->application_status;
                $requested_grant    = $model->requested_grant;
                $approved_grant     = $model->approved_grant;

                $admArr    = json_decode($request->input('adm_budget'));
                $directArr = json_decode($request->input('direct_budget'));
                $datetime  = date('Y-m-d H:i:s');


                if (!empty($admArr)) {
                    foreach ($admArr as $admVal) {
                        if ($admVal->id != '') {
                            DB::table('budget_breakdowns')
                                ->where('id', $admVal->id)
                                ->update([
                                    'description'    => $admVal->adm_expenditure_item,
                                    'budget_amount'  => $admVal->adm_est_expense,
                                    'tight_budget'   => $admVal->adm_tight_budget,
                                    'reprog_budget1' => $admVal->adm_reprog_budget1,
                                    'reprog_budget2' => $admVal->adm_reprog_budget2,
                                    'reprog_budget3' => $admVal->adm_reprog_budget3,
                                    'reprog_budget4' => $admVal->adm_reprog_budget4,
                                    'reprog_budget5' => $admVal->adm_reprog_budget5,
                                    'updated_at'     => $datetime,
                                ]);

                               
                                $reportExpensesDtl = ReportedExpenses::select('*', DB::raw('SUM(expense_amount) As expenseAmount'))
                                ->where('bb_id', $admVal->id)
                                ->where('proposal_id', $proposal_id)
                                ->get();

                                if($reportExpensesDtl[0]->expenseAmount>0){

                                        // Tight Budget status checking
                                        if($model->application_status ==5 && $model->reprogramStatus ==1 && $model->reprogramCount ==0 && $model->reprogramRequest ==0){
                                            
                                            $remainAmount = $admVal->adm_tight_budget - $reportExpensesDtl[0]->expenseAmount;   
                                           
                                        }elseif($model->application_status ==5 && $model->reprogramStatus ==0 && $model->reprogramCount ==1 && $model->reprogramRequest ==0){
                                            
                                            $remainAmount = $admVal->adm_reprog_budget1 - $reportExpensesDtl[0]->expenseAmount;   
                                           
                                        }elseif($model->application_status ==5 && $model->reprogramStatus ==0 && $model->reprogramCount ==2 && $model->reprogramRequest ==0){
                                       
                                            $remainAmount = $admVal->adm_reprog_budget2 - $reportExpensesDtl[0]->expenseAmount;
                            
                                        }elseif($model->application_status ==5 && $model->reprogramStatus ==0 && $model->reprogramCount ==3 && $model->reprogramRequest ==0){
                                       
                                            $remainAmount = $admVal->adm_reprog_budget3 - $reportExpensesDtl[0]->expenseAmount;
                            
                                        }elseif($model->application_status ==5 && $model->reprogramStatus ==0 && $model->reprogramCount ==4 && $model->reprogramRequest ==0){
                                            
                                            $remainAmount = $admVal->adm_reprog_budget4 - $reportExpensesDtl[0]->expenseAmount;
                                            
                                        }elseif($model->application_status ==5 && $model->reprogramStatus ==0 && $model->reprogramCount ==5 && $model->reprogramRequest ==0){
                                        
                                            $remainAmount = $admVal->adm_reprog_budget5 - $reportExpensesDtl[0]->expenseAmount;
                                        }
                                    
                                }else{

                                    $remainAmount = 0;
                                }

                                DB::table('budget_breakdowns')
                                ->where('id', $admVal->id)
                                ->update(['remain_amount' => $remainAmount]);

                               /* if (count($reportExpensesDtl)>0) {

                                    if( ($model->requested_grant != $model->approved_grant) && 
                                        $model->approved_grant>0 && 
                                        (
                                            $model->application_status ==1 || 
                                            $model->application_status ==2 || 
                                            $model->application_status ==5
                                        )
                                    )
                                    {
                                        // Tight Budget status checking
                                        $remainAmount = $budgetAmount->tight_budget - $request->expense_amount;
                                    } else {
                                        $remainAmount = $budgetAmount->budget_amount - $request->expense_amount;
                                    }

                                    DB::table('budget_breakdowns')
                                    ->where('id',  $admVal->id)
                                    ->update(['remain_amount' => $remainAmount]);
                    
                                } */
                    
                               
                        
                        } else {
                            $tight_budget = ($admVal->adm_tight_budget != '' || $admVal->adm_tight_budget != null) ? $admVal->adm_tight_budget : 0;

                            $reprog_budget1 = ($admVal->adm_reprog_budget1 != '' || $admVal->adm_reprog_budget1 != null) ? $admVal->adm_reprog_budget1 : 0;

                            $reprog_budget2 = ($admVal->adm_reprog_budget2 != '' || $admVal->adm_reprog_budget2 != null) ? $admVal->adm_reprog_budget2 : 0;

                            $reprog_budget3 = ($admVal->adm_reprog_budget3 != '' || $admVal->adm_reprog_budget3 != null) ? $admVal->adm_reprog_budget3 : 0;

                            $reprog_budget4 = ($admVal->adm_reprog_budget4 != '' || $admVal->adm_reprog_budget4 != null) ? $admVal->adm_reprog_budget4 : 0;
                            $reprog_budget5 = ($admVal->adm_reprog_budget5 != '' || $admVal->adm_reprog_budget5 != null) ? $admVal->adm_reprog_budget5 : 0;

                            DB::insert('insert into budget_breakdowns (proposal_id, budget_type, description, budget_amount, tight_budget, reprog_budget1, reprog_budget2, reprog_budget3, reprog_budget4, reprog_budget5, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$proposal_id, 'Administrative', $admVal->adm_expenditure_item, $admVal->adm_est_expense, $tight_budget, $reprog_budget1, $reprog_budget2, $reprog_budget3, $reprog_budget4, $reprog_budget5, $datetime, $datetime]);
                        }
                    }
                }
                if (!empty($directArr)) {
                    foreach ($directArr as $directVal) {
                        if ($directVal->id != '') {
                            DB::table('budget_breakdowns')
                                ->where('id', $directVal->id)
                                ->update([
                                    'description'    => $directVal->direct_expenditure_item,
                                    'budget_amount'  => $directVal->direct_est_expense,
                                    'tight_budget'   => $directVal->direct_tight_budget,
                                    'reprog_budget1' => $directVal->direct_reprog_budget1,
                                    'reprog_budget2' => $directVal->direct_reprog_budget2,
                                    'reprog_budget3' => $directVal->direct_reprog_budget3,
                                    'reprog_budget4' => $directVal->direct_reprog_budget4,
                                    'reprog_budget5' => $directVal->direct_reprog_budget5,
                                    'updated_at'     => $datetime,
                                ]);


                               
                                $reportExpensesDtl = ReportedExpenses::select('*', DB::raw('SUM(expense_amount) As expenseAmount'))
                                ->where('bb_id', $directVal->id)
                                ->where('proposal_id', $proposal_id)
                                ->get();
                            

                                if($reportExpensesDtl[0]->expenseAmount>0){

                                        // Tight Budget status checking
                                        if($model->application_status ==5 && $model->reprogramStatus ==1 && $model->reprogramCount ==0 && $model->reprogramRequest ==0){
                                       
                                            $remainAmount =  $directVal->direct_tight_budget - $reportExpensesDtl[0]->expenseAmount;
                            
                                        }elseif($model->application_status ==5 && $model->reprogramStatus ==0 && $model->reprogramCount ==1 && $model->reprogramRequest ==0){
                                            
                                            $remainAmount = $directVal->direct_reprog_budget1 - $reportExpensesDtl[0]->expenseAmount;
                                           
                                        }elseif($model->application_status ==5 && $model->reprogramStatus ==0 && $model->reprogramCount ==2 && $model->reprogramRequest ==0){
                                       
                                            $remainAmount = $directVal->direct_reprog_budget2 - $reportExpensesDtl[0]->expenseAmount;
                            
                                        }elseif($model->application_status ==5 && $model->reprogramStatus ==0 && $model->reprogramCount ==3 && $model->reprogramRequest ==0){
                                       
                                            $remainAmount = $directVal->direct_reprog_budget3 - $reportExpensesDtl[0]->expenseAmount;
                            
                                        }elseif($model->application_status ==5 && $model->reprogramStatus ==0 && $model->reprogramCount ==4 && $model->reprogramRequest ==0){
                                            
                                            $remainAmount = $directVal->direct_reprog_budget4 - $reportExpensesDtl[0]->expenseAmount;
                                            
                                        }elseif($model->application_status ==5 && $model->reprogramStatus ==0 && $model->reprogramCount ==5 && $model->reprogramRequest ==0){
                                        
                                            $remainAmount = $directVal->direct_reprog_budget5 - $reportExpensesDtl[0]->expenseAmount;
                                        }

                            
                                }else{

                                    $remainAmount = 0;
                                }

                                DB::table('budget_breakdowns')
                                        ->where('id', $directVal->id)
                                        ->update(['remain_amount' => $remainAmount]);

                        } else {
                            $tight_budget = ($directVal->direct_tight_budget != '' || $directVal->direct_tight_budget != null) ? $directVal->direct_tight_budget : 0;

                            $reprog_budget1 = ($directVal->direct_reprog_budget1 != '' || $directVal->direct_reprog_budget1 != null) ? $directVal->direct_reprog_budget1 : 0;

                            $reprog_budget2 = ($directVal->direct_reprog_budget2 != '' || $directVal->direct_reprog_budget2 != null) ? $directVal->direct_reprog_budget2 : 0;

                            $reprog_budget3 = ($directVal->direct_reprog_budget3 != '' || $directVal->direct_reprog_budget3 != null) ? $directVal->direct_reprog_budget3 : 0;

                            $reprog_budget4 = ($directVal->direct_reprog_budget4 != '' || $directVal->direct_reprog_budget4 != null) ? $directVal->direct_reprog_budget4 : 0;

                            $reprog_budget5 = ($directVal->direct_reprog_budget5 != '' || $directVal->direct_reprog_budget5 != null) ? $directVal->direct_tight_budget5 : 0;

                            DB::insert('insert into budget_breakdowns (proposal_id, budget_type, description, budget_amount, tight_budget, reprog_budget1, reprog_budget2, reprog_budget3, reprog_budget4, reprog_budget5, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$proposal_id, 'Direct', $directVal->direct_expenditure_item, $directVal->direct_est_expense, $tight_budget, $reprog_budget1, $reprog_budget2, $reprog_budget3, $reprog_budget4, $reprog_budget5, $datetime, $datetime]);
                        }
                    }
                }

                $model->administrative_budget_file_status = $request->input('administrative_budget_file_status') != '' ? $request->input('administrative_budget_file_status') : 0;
                $model->direct_budget_file_status         = $request->input('direct_budget_file_status') != '' ? $request->input('direct_budget_file_status') : 0;

                $model->reprogramStatus = 1;

                if ($request->hasFile('adm_expence_file')) {

                    $file          = $request->file('adm_expence_file');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'adm_expence_file';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, 'Administrative');
                }

                if ($request->hasFile('direct_expence_file')) {

                    $file          = $request->file('direct_expence_file');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'direct_expence_file';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, 'Direct');
                }

                //Tight Budget Notification
                if ($user_type == 1 && ($application_status == 1 || $application_status == 2) && $requested_grant != $approved_grant && $approved_grant > 0 && $approved_grant != null) {
                    //get Analyst email id
                    $analyst = DB::table('assign_user_to_proposals AS aup')
                        ->leftJoin('users', 'users.id', '=', 'aup.user_id')
                        ->where('aup.proposal_id', '=', $model->id)
                        ->select('users.email', 'users.admin_first_name', 'users.admin_last_name')
                        ->first();

                    $project = DB::table('projects')
                        ->select(DB::raw('projectNameEn,projectNameEs'))
                        ->where('id', '=', $model->project_id)
                        ->first();

                    $superadmin = DB::table('users')
                        ->select(DB::raw('email'))
                        ->where('roleId', '=', 1)
                        ->first();

                    $analyst_email = $analyst->email;
                    //$analyst_email = 'avijit.panja@indusnet.co.in';

                    $entityDetails = DB::table('users')->where('id', $model->user_id)->first();

                    $data['entity_name']  = $entityDetails->entity_name;
                    $data['analyst_name'] = $analyst->admin_first_name;
                    $data['project_name'] = $project->projectNameEn;
                    $data['proposal_id']  = $model->proposal_unique_id;

                    $admin_email = $superadmin->email;

                    $emailArr = array($analyst_email, $admin_email);

                    Mail::to($emailArr)->send(new Tightbudgetnotiadmin($data));
                }

                $this->insertAuditTrail('AUDIT_TRAIL.PROPOSAL', 'AUDIT_TRAIL.STEP6', 'Application Id:  ' . $model->proposal_unique_id);
            }

            // 6th step / Legal
            if ($request->input('step') == 6) {
                $model->legal        = $request->input('legal');
                $model->legal_status = $request->input('legal_status') != '' ? $request->input('legal_status') : 0;

                $this->insertAuditTrail('AUDIT_TRAIL.PROPOSAL', 'AUDIT_TRAIL.STEP3', 'Application Id:  ' . $model->proposal_unique_id);
            }

            // 7th step / Requirements
            if ($request->input('step') == 7) {
                $model->requirement_file_status = $request->input('requirement_file_status') != '' ? $request->input('requirement_file_status') : 0;
                $desc                           = ($request->input('desc') != '' || $request->input('desc') != null) ? $request->input('desc') : '';

                $reqFileArr = json_decode($request->input('reqFileArr'));
                if (!empty($reqFileArr)) {
                    foreach ($reqFileArr as $directVal) {
                        DB::table('proposal_documents')
                            ->where('id', $directVal->id)
                            ->update([
                                'file_status' => $directVal->isChecked == true ? '1' : '0',
                            ]);
                    }

                }

                if ($request->hasFile('puerto_rico_state_department')) {

                    $file          = $request->file('puerto_rico_state_department');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'puerto_rico_state_department';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }
                if ($request->hasFile('internal_revenue_services')) {

                    $file          = $request->file('internal_revenue_services');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'internal_revenue_services';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }

                if ($request->hasFile('puerto_rico_state_insurance_fund_corporation')) {

                    $file          = $request->file('puerto_rico_state_insurance_fund_corporation');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'puerto_rico_state_insurance_fund_corporation';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }

                if ($request->hasFile('municipal_income_collection_center')) {

                    $file          = $request->file('municipal_income_collection_center');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'municipal_income_collection_center';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }

                if ($request->hasFile('department_of_finance')) {

                    $file          = $request->file('department_of_finance');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'department_of_finance';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }

                if ($request->hasFile('department_of_labor_and_human_resources')) {

                    $file          = $request->file('department_of_labor_and_human_resources');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'department_of_labor_and_human_resources';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }

                if ($request->hasFile('health_department')) {

                    $file          = $request->file('health_department');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'health_department';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }

                if ($request->hasFile('administration_of_regulations_and_permits')) {

                    $file          = $request->file('administration_of_regulations_and_permits');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'administration_of_regulations_and_permits';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }

                if ($request->hasFile('puerto_rico_fire_fighter_body')) {

                    $file          = $request->file('puerto_rico_fire_fighter_body');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'puerto_rico_fire_fighter_body';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }

                if ($request->hasFile('custody_agency')) {

                    $file          = $request->file('custody_agency');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'custody_agency';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }

                if ($request->hasFile('additional_documents')) {

                    $file          = $request->file('additional_documents');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'additional_documents';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }
                if ($request->hasFile('seguro_social')) {

                    $file          = $request->file('seguro_social');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'seguro_social';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }
                if ($request->hasFile('administration_for_child_support')) {

                    $file          = $request->file('administration_for_child_support');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'administration_for_child_support';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }
                if ($request->hasFile('puerto_rico_police')) {

                    $file          = $request->file('puerto_rico_police');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'puerto_rico_police';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }
                if ($request->hasFile('negotiated_conventions_of_san_jun')) {

                    $file          = $request->file('negotiated_conventions_of_san_jun');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'negotiated_conventions_of_san_jun';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }
                if ($request->hasFile('higher_education_council')) {

                    $file          = $request->file('higher_education_council');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'higher_education_council';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }
                if ($request->hasFile('osfl')) {

                    $file          = $request->file('osfl');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'osfl';

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);
                }

                if ($loggedUserRole === 'companies' && $request->input('accessDenied') == 1) {
                    $model->application_status = '1'; // Submitted
                    $model->completed_step     = 9;
                    $this->createProposalLogs($model->id, 'SUBMIT');
                }

                $this->insertAuditTrail('AUDIT_TRAIL.PROPOSAL', 'AUDIT_TRAIL.STEP4', 'Application Id:  ' . $model->proposal_unique_id);
            }

            // 8th step / Evidence
            if ($request->input('step') == 8) {

                $model->evidence_doc_status = $request->input('evidence_doc_status') != '' ? $request->input('evidence_doc_status') : 0;

                if ($request->hasFile('evidence_doc_file')) {

                    $file          = $request->file('evidence_doc_file');
                    $user_id       = $model->user_id;
                    $proposal_id   = $model->id;
                    $document_type = 'evidence_doc_file';
                    //$document_type = preg_replace("![^a-z0-9]+!i", "_", strtolower($model->inventory_of_property));

                    $fileCount = $this->moveFile($file, $user_id, $proposal_id, $document_type, $request->input('descEvidence'));
                }

                $this->insertAuditTrail('AUDIT_TRAIL.PROPOSAL', 'AUDIT_TRAIL.STEP5', 'Application Id:  ' . $model->proposal_unique_id);
            }

            // 9th step
            if ($request->input('step') == 9) {
                $users = DB::table('users')
                    ->select(DB::raw('entity_name,email'))
                    ->where('id', '=', $model->user_id)
                    ->first();

                $project = DB::table('projects')
                    ->select(DB::raw('projectNameEn,projectNameEs'))
                    ->where('id', '=', $model->project_id)
                    ->first();

                $entity_name  = $users->entity_name;
                $project_name = $project->projectNameEn;
                $proposal_id  = $model->proposal_unique_id;
                $email        = $users->email;

                $model->name          = $request->input('name');
                $model->proposalAgree = $request->input('proposalAgree') == true ? '1' : '0';

                if ($loggedUserRole === 'companies') {
                    $model->application_status = '1'; // Submitted

                    $this->createProposalLogs($model->id, 'SUBMIT');

                }

                if ($request->input('evaluationstatus') == true) {
                    //return response()->json($model->population_status);
                    if (
                        $model->population_status == 0 ||
                        $model->necessity_status == 0 ||
                        $model->programmatic_status == 0 ||
                        $model->goal_status == 0 ||
                        $model->changes_status == 0 ||
                        $model->specific_status == 0 ||
                        $model->affected_status == 0 ||
                        $model->pay_status == 0 ||
                        $model->work_plan_status == 0 ||
                        $model->resources_status == 0 ||
                        $model->collecting_status == 0 ||
                        $model->organization_evaluates_status == 0 ||
                        $model->who_evaluates_status == 0 ||
                        $model->statements_describes_type_of_evaluations_status == 0 ||
                        $model->type_of_evaluation_status == 0 ||
                        $model->type_of_data_status == 0 ||
                        $model->type_of_evaluator_status == 0 ||
                        $model->administrative_budget_file_status == 0 ||
                        $model->direct_budget_file_status == 0 ||
                        $model->requirement_file_status == 0 ||
                        $model->inventory_file_status == 0 ||
                        $model->evidence_doc_status == 0 ||
                        $model->legal_status == 0
                    ) {
                        return response()->json([
                            'message' => 'Some Evaluation are still pending ! Please try again.',
                            'success' => false,
                        ], 400);
                    }

                    $model->application_status = '6'; // Evaluation Complete
                    $model->evaluateStatus     = '1'; // Evaluation Complete
                    $this->createProposalLogs($model->id, 'EVALUATION');
                }

                $model->date = date('Y-m-d', strtotime(str_replace("/", "-", $request->input('date'))));

                Mail::to($email)->send(new Proposaladd($entity_name, $project_name, $proposal_id));

                $this->insertAuditTrail('AUDIT_TRAIL.PROPOSAL', 'AUDIT_TRAIL.STEP7', 'Application Id:  ' . $model->proposal_unique_id);
            }

            //print_r($model);die;

            if( $loggedUserRole==='companies'){
                $model->edit_read = 1;

            }else{
                $model->edit_read = 0; 
            }
          
            $model->save();
            $modelId = $model->id;
            //print_r($model);die;

            \DB::commit();
            $message = 'Record has been updated successfully!';
            $success = true;
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }
        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 201);

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proposal  $proposal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proposal = Proposal::find($id);
        $proposal->delete();
        return response()->json([
            'message' => 'proposal removed successfully',
            'success' => true,
        ], 201);
    }

    protected function saveToDocuments($user_id, $proposal_id, $document_type, $desc = '', $monthly_payment = '', $rented = '')
    {
        try {
            $pd      = new ProposalDocument;
            $pdArr[] = [
                'user_id'         => $user_id,
                'proposal_id'     => $proposal_id,
                'document_type'   => $document_type,
                'desc'            => $desc,
                'monthly_payment' => $monthly_payment,
                'rented'          => $rented,
                'org_file_name'   => '',
                'file_name'       => '',
                'file_path'       => '',
                'created_at'      => date('Y-m-d H:i:s'),
                'updated_at'      => date('Y-m-d H:i:s'),
            ];
            $pd::insert($pdArr);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    protected function moveFileProp($file, $user_id, $proposal_id, $document_type, $desc = '', $monthly_payment = '', $rented = '')
    {

        $fileCount = count((array) $file);

        try {
            $pd = new ProposalDocument;

            $notMoveFileArr = array();

            for ($i = 0; $i < $fileCount; $i++) {
                $originalFileName = $file[$i]->getClientOriginalName();
                $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
                $extension        = strtolower($file[$i]->getClientOriginalExtension());
                $fileName         = date('YmdHmsu') . '-' . $filteredOrgName;

                $fileSize = $file[$i]->getSize();

                $fileSize = number_format($fileSize / 1048576, 2);

                $destinationPath = "upload/proposal_documents/" . $proposal_id . "/";
                $fullFilePath    = $destinationPath . $fileName;

                if ($file[$i]->move($destinationPath, $fileName)) {

                    /* shell_exec ('gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile='.$destinationPath.'test.pdf '.$fullFilePath.''); */

                    if ($fileSize > 6) {

                        $oldFullFilePath = $fullFilePath;

                        $fileName     = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                        $fullFilePath = $destinationPath . $fileName;

                        if ($extension == 'pdf') {

                            shell_exec('ps2pdf -dPDFSETTINGS=/default ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } else if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                            shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } /*else{
                        rename( $orgFullFilePath, $fullFilePath);
                        }*/

                        if (File::exists($oldFullFilePath)) {
                            File::delete($oldFullFilePath);
                        }
                    }

                    $pdArr[] = [
                        'user_id'         => $user_id,
                        'proposal_id'     => $proposal_id,
                        'document_type'   => $document_type,
                        'desc'            => $desc,
                        'monthly_payment' => $monthly_payment,
                        'rented'          => $rented,
                        'org_file_name'   => $originalFileName,
                        'file_name'       => $fileName,
                        'file_path'       => $fullFilePath,
                        'created_at'      => date('Y-m-d H:i:s'),
                        'updated_at'      => date('Y-m-d H:i:s'),
                    ];

                } else {
                    $notMoveFileArr[] = $originalFileName;
                }
            }
            $pd::insert($pdArr);
            return $notMoveFileArr;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    protected function moveFile($file, $user_id, $proposal_id, $document_type, $desc = '')
    {

        $fileCount = count((array) $file);

        try {
            $pd = new ProposalDocument;

            $notMoveFileArr = array();

            for ($i = 0; $i < $fileCount; $i++) {
                $originalFileName = $file[$i]->getClientOriginalName();
                $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
                $extension        = strtolower($file[$i]->getClientOriginalExtension());
                $fileName         = date('YmdHmsu') . '-' . $filteredOrgName;

                $fileSize = $file[$i]->getSize();

                $fileSize = number_format($fileSize / 1048576, 2);

                $destinationPath = "upload/proposal_documents/" . $proposal_id . "/";
                $fullFilePath    = $destinationPath . $fileName;

                if ($file[$i]->move($destinationPath, $fileName)) {

                    /* shell_exec ('gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile='.$destinationPath.'test.pdf '.$fullFilePath.''); */

                    if ($fileSize > 6) {

                        $oldFullFilePath = $fullFilePath;

                        $fileName     = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                        $fullFilePath = $destinationPath . $fileName;

                        if ($extension == 'pdf') {

                            shell_exec('ps2pdf -dPDFSETTINGS=/default ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } else if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                            shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } /*else{
                        rename( $orgFullFilePath, $fullFilePath);
                        }*/

                        if (File::exists($oldFullFilePath)) {
                            File::delete($oldFullFilePath);
                        }
                    }

                    $pdArr[] = [
                        'user_id'       => $user_id,
                        'proposal_id'   => $proposal_id,
                        'document_type' => $document_type,
                        'desc'          => $desc,
                        'org_file_name' => $originalFileName,
                        'file_name'     => $fileName,
                        'file_path'     => $fullFilePath,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s'),
                    ];

                } else {
                    $notMoveFileArr[] = $originalFileName;
                }
            }
            $pd::insert($pdArr);
            return $notMoveFileArr;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function moveFileByAdmin(Request $request)
    {
        try {
            $pd = new ProposalDocument;

            $file          = $request->file('admin_attach_file');
            $fileCount     = count((array) $file);
            $user_id       = $request->input('user_id');
            $proposal_id   = $request->input('proposal_id');
            $document_type = 'attach_by_admin';
            $desc          = $request->input('desc');

            for ($i = 0; $i < $fileCount; $i++) {
                $originalFileName = $file[$i]->getClientOriginalName();
                $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
                $extension        = strtolower($file[$i]->getClientOriginalExtension());
                $fileName         = date('YmdHmsu') . '-' . $filteredOrgName;

                $fileSize = $file[$i]->getSize();

                $fileSize = number_format($fileSize / 1048576, 2);

                $destinationPath = "upload/proposal_documents/" . $proposal_id . "/";
                $fullFilePath    = $destinationPath . $fileName;

                if ($file[$i]->move($destinationPath, $fileName)) {

                    /* shell_exec ('gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile='.$destinationPath.'test.pdf '.$fullFilePath.''); */

                    if ($fileSize > 4) {

                        $oldFullFilePath = $fullFilePath;

                        $fileName     = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                        $fullFilePath = $destinationPath . $fileName;

                        if ($extension == 'pdf') {

                            shell_exec('ps2pdf -dPDFSETTINGS=/default ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } else if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                            shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } /*else{
                        rename( $orgFullFilePath, $fullFilePath);
                        }*/

                        if (File::exists($oldFullFilePath)) {
                            File::delete($oldFullFilePath);
                        }
                    }

                    $pdArr[] = [
                        'user_id'       => $user_id,
                        'proposal_id'   => $proposal_id,
                        'document_type' => $document_type,
                        'desc'          => $desc,
                        'org_file_name' => $originalFileName,
                        'file_name'     => $fileName,
                        'file_path'     => $fullFilePath,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s'),
                    ];

                } else {
                    $notMoveFileArr[] = $originalFileName;
                }
            }

            $pd::insert($pdArr);
            // Logs
            $this->createProposalLogs($proposal_id, 'ATTACHMENT_ADD');

            return response()->json(['Success' => 'File Uploaded Successfully!!'], 200);

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function getAttachFileList(Request $request, $proposal_id)
    {
        try {
            /*$data = DB::table('proposals')
            ->leftJoin('proposal_documents', 'proposals.id', '=', 'proposal_documents.proposal_id')
            ->where('proposals.id', '=', $proposal_id)
            ->where('proposal_documents.document_type', '=', 'attach_by_admin')
            ->select('proposal_documents.file_name', 'proposal_documents.file_path', 'proposal_documents.desc', 'proposal_documents.id')
            ->orderBy('proposal_documents.id', 'DESC')
            ->get();*/

            $data = ProposalDocument::with('logUser')
                ->where('proposal_id', '=', $proposal_id)
                ->where('document_type', '=', 'attach_by_admin')
                ->orderBy('id', 'DESC')
                ->get();

            return response()->json([
                'data'    => $data,
                'success' => true,
            ], 201);
        } catch (\Exception $e) {
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = 'error';
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function fileDelete($id)
    {
        try {
            $pd = new ProposalDocument;

            $proposalDoc = $pd::find($id);

            $filename = $proposalDoc['file_path'];

            if (File::exists($filename)) {
                File::delete($filename);
            }
            //else{
            //return response()->json('File does not exist');
            //}

            $proposalDoc->delete();
            return response()->json([
                'message' => 'Proposal document removed successfully',
                'success' => true,
            ], 201);

        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    /**
     * Reject Proposal.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function rejectProposal(Request $request, $id)
    {
        \DB::beginTransaction();
        try {

            $model = Proposal::find($id);

            if (isset($model->approved_grant) && $model->approved_grant > 0) {

                $model->application_status = '4';

                $users = DB::table('users')
                    ->select(DB::raw('entity_name,email'))
                    ->where('id', '=', $model->user_id)
                    ->first();

                $project = DB::table('projects')
                    ->select(DB::raw('projectNameEn,projectNameEs'))
                    ->where('id', '=', $model->project_id)
                    ->first();

                $entity_name  = $users->entity_name;
                $project_name = $project->projectNameEn;
                $proposal_id  = $model->proposal_unique_id;
                $email        = $users->email;

                $model->save();
                $modelId = $model->id;
                //print_r($model);die;

                $this->createProposalLogs($id, 'REJECTED');

                Mail::to($email)->send(new Proposalreject($entity_name, $project_name, $proposal_id));

                \DB::commit();
                $message = 'Proposal rejected successfully!';
                $success = true;
            } else {
                return response()->json([
                    'message' => 'GRANT_BEFORE',
                    'success' => false,
                ], 400);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }

        //Return message
        return response()->json([
            'applicationStatus' => $model->application_status,
            'message'           => $message,
            'success'           => $success,
        ], 201);

    }

    /**
     * Accept Proposal.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function acceptProposal(Request $request, $id)
    {
        \DB::beginTransaction();
        try {

            $model = Proposal::find($id);

            if (isset($model->approved_grant) && $model->approved_grant > 0) {


                $quaterDtl =   DB::table('quaterly_reports')
                ->where('proposal_id', $id)
                ->first();

                if($quaterDtl){
                   
                }else{
                    $administrative_budget = ($model->approved_grant * 40) / 100;
                    $direct_budget         = ($model->approved_grant * 60) / 100;
                    $datetime              = date('Y-m-d H:i:s');
                    DB::insert('insert into quaterly_reports (proposal_id, administrative_budget, direct_budget, created_at, updated_at) values (?, ?, ?, ?, ?)', [$model->id, $administrative_budget, $direct_budget, $datetime, $datetime]);
                }


                $model->application_status = '5';

                $users = DB::table('users')
                    ->select(DB::raw('entity_name,email'))
                    ->where('id', '=', $model->user_id)
                    ->first();

                $project = DB::table('projects')
                    ->select(DB::raw('projectNameEn,projectNameEs'))
                    ->where('id', '=', $model->project_id)
                    ->first();

                $entity_name  = $users->entity_name;
                $project_name = $project->projectNameEn;
                $proposal_id  = $model->proposal_unique_id;
                $email        = $users->email;

                $model->save();
                $modelId = $model->id;
                //print_r($model);die;
                $this->createProposalLogs($id, 'ACCEPTED');

                //Mail::to($email)->send(new Proposalapprove($entity_name, $project_name, $proposal_id));

                \DB::commit();
                $message = 'Proposal accepted successfully!';
                $success = true;
            } else {
                return response()->json([
                    'message' => 'GRANT_BEFORE',
                    'success' => false,
                ], 400);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }

        //Return message
        return response()->json([
            'applicationStatus' => $model->application_status,
            'message'           => $message,
            'success'           => $success,
        ], 201);

    }

    /**
     * Accept Budget Reprogram By Analysts.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function reprogramProposal(Request $request, $id)
    {
        \DB::beginTransaction();
        try {

            $model = Proposal::find($id);

            if ($model->reprogramCount < 6) {

                //Proposal::find($id)->increment('reprogramCount');

                $model->increment('reprogramCount');
                $model->save();
                //print_r($model);die;

                if ($model->reprogramRequest <= 5) {
                    $model->reprogramRequest = 0;
                    $model->reprogramStatus  = 0;

                    $model->save();
                }

                $this->createProposalLogs($id, 'REPROGRAM');

                \DB::commit();
                $message = 'Proposal reprogram accepted successfully!';
                $success = true;
            } else {
                return response()->json([
                    'message' => 'OVER_LIMIT',
                    'success' => false,
                ], 400);
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }

        //Return message
        return response()->json([
            'reprogramCount' => $model->reprogramCount,
            'message'        => $message,
            'success'        => $success,
        ], 201);

    }

    /**
     * Request Budget Reprogram by Proponent.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function requestreprogramProposal(Request $request, $id)
    {
        \DB::beginTransaction();
        try {

            $model = Proposal::find($id);

            if ($model->reprogramCount <= 5) {

                $model->reprogramRequest = 1;

                $analyst = DB::table('assign_user_to_proposals AS aup')
                    ->leftJoin('users', 'users.id', '=', 'aup.user_id')
                    ->where('aup.proposal_id', '=', $id)
                    ->select('users.email', 'users.admin_first_name', 'users.admin_last_name')
                    ->first();

                $data['analyst_name'] = $analyst->admin_first_name . ' ' . $analyst->admin_last_name;
                $data['proposal_id']  = $model->proposal_unique_id;
                $analystEmailId       = $analyst->email;
                //$analystEmailId = 'mohank.das@indusnet.co.in';

                if ($model->save()) {
                    $this->createProposalLogs($id, 'REQUEST_REPROGRAM');

                    Mail::to($analystEmailId)->send(new Reprogalert($data));
                }

                \DB::commit();
                $message = 'Proposal reprogram request successfully!';
                $success = true;
            } else {
                return response()->json([
                    'message' => 'OVER_LIMIT',
                    'success' => false,
                ], 400);
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }

        //Return message
        return response()->json([
            'reprogramCount' => $model->reprogramCount,
            'message'        => $message,
            'success'        => $success,
        ], 201);

    }

    /**
     * Request Reprogram.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function rejectGrant(Request $request, $id)
    {
        \DB::beginTransaction();
        try {

            $model = Proposal::find($id);

            if (isset($model->approved_grant) && $model->approved_grant > 0) {

                $model->application_status = '7';

                $model->save();
                //print_r($model);die;

                $this->createProposalLogs($id, 'REJECT_GRANT');

                \DB::commit();
                $message = 'Proposal grant rejected successfully!';
                $success = true;
            } else {
                return response()->json([
                    'message' => 'REJECT_GRANT',
                    'success' => false,
                ], 400);
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 201);

    }

    /**
     * Request Reprogram.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function paidStatus(Request $request, $id)
    {
        \DB::beginTransaction();
        try {

            $model = Proposal::find($id);

            if (isset($model->approved_grant) && $model->approved_grant > 0) {

                $model->paidStatus = 1;

                $model->save();
                //print_r($model);die;

                $this->createProposalLogs($id, 'PAID');

                \DB::commit();
                $message = 'Proposal updated successfully!';
                $success = true;
            } else {
                return response()->json([
                    'message' => 'PAID',
                    'success' => false,
                ], 400);
            }

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 201);

    }

    /**
     * Request Reprogram.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function waitingStatus(Request $request, $id)
    {
        \DB::beginTransaction();
        try {

            $model = Proposal::find($id);

            //if (isset($model->approved_grant) && $model->approved_grant > 0) {

            $model->application_status = '2';

            $model->save();
            //print_r($model);die;

            $this->createProposalLogs($id, 'PAID');

            \DB::commit();
            $message = 'Proposal updated successfully!';
            $success = true;
            /*} else {
        return response()->json([
        'message' => 'PAID',
        'success' => false,
        ], 400);
        }*/

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message = $allErrors . ' ' . 'Please try again!';
            $success = false;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $success,
        ], 201);

    }

    /**
     * Update grant in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateGrant(Request $request, $id)
    {
        $roleKey = Auth::user()->role->roleKey;
        $user_id = Auth::user()->id;

        try {
            $model = Proposal::find($id);

            //return response()->json([$model->assigned_user , $request->input('assigned_user')]);

            if ($request->input('requested_grant') == "null" || $request->input('requested_grant') == '') {
                //$model->requested_grant = 0;
            } else {
                $model->requested_grant = $request->input('requested_grant');
            }

            if ($request->input('donation_grant') == "null" || $request->input('donation_grant') == '') {
                //$model->donation_grant = 0;
            } else {
                // Logs
                if ($model->donation_grant != (int) $request->input('donation_grant')) {
                    $this->createProposalLogs($id, 'DONATION_GRANT');
                }

                $model->donation_grant = $request->input('donation_grant');
            }

            if ($request->input('approved_grant') == 0 || $request->input('approved_grant') == "null" || $request->input('approved_grant') == '') {
                //$model->approved_grant = 0;
            } else {

                //if ($model->application_status == '6') {
                if ($model->evaluateStatus == '1') {

                    // Logs
                    //return response()->json([$model->approved_grant , (int)$request->input('approved_grant')]);

                    if ($model->approved_grant != (int) $request->input('approved_grant')) {
                        $this->createProposalLogs($id, 'APPROVED_GRANT');
                        $model->approved_grant     = $request->input('approved_grant');
                        $model->application_status = '2';
                    }

                } else {
                    //Return message
                    return response()->json([
                        'message' => 'EVAL_BEFORE',
                        'success' => false,
                    ], 400);
                }
            }

            $model->open_status = $request->input('open_status') === "true" ? 1 : 0;

            $prev_assigned_user = DB::table('assign_user_to_proposals')->where('proposal_id', $id)->pluck('user_id');

            if (count($prev_assigned_user) == 0) {
                $prev_assigned_user[0] = 0;
            }

            /*return response()->json([
            'Old' => $prev_assigned_user[0],
            'New' => (int) $request->input('assigned_user'),
            ]);*/

            if ($request->input('assigned_user') !== 'undefined' && $request->input('assigned_user') !== 0 && (int) $request->input('assigned_user') != $prev_assigned_user[0]) {

                $modelAs = new AssignUserToProposal;

                DB::table('assign_user_to_proposals')->where('proposal_id', '=', $id)->delete();

                $modelAs->proposal_id = $id;
                $modelAs->user_id     = $request->input('assigned_user');
                $modelAs->createdBy   = $user_id;
                $modelAs->status      = 1;
                $modelAs->created_at  = date('Y-m-d H:i:s');
                $modelAs->updated_at  = date('Y-m-d H:i:s');

                $analystDetails = DB::table('users')->where('id', $request->input('assigned_user'))->first();

                $projectDetails = DB::table('projects')->where('id', $model->project_id)->first();

                //return response()->json([$analystDetails]);

                $data['analyst_name'] = $analystDetails->admin_first_name . ' ' . $analystDetails->admin_last_name;
                $data['project_name'] = $projectDetails->projectNameEn;
                $data['proposal_id']  = $model->proposal_unique_id;
                $analystEmailId       = $analystDetails->email;
                //$analystEmailId = 'avijit.panja@indusnet.co.in';

                if ($modelAs->save()) {
                    $this->createProposalLogs($id, 'ASSIGN');
                    //Mail::to($analystEmailId)->send(new Assignusernotification($data));
                }
            }

            $model->notifyUser = $request->input('notifyUser') === "true" ? 1 : 0;

            if ($model->save()) {

                if ($model->requested_grant != $model->approved_grant && $request->input('approved_grant') > 0) {

                    if ($request->input('notifyUser') == "true") {

                        $entityDetails = DB::table('users')->where('id', $model->user_id)->first();

                        $projectDetails = DB::table('projects')->where('id', $model->project_id)->first();

                        $data['entity_name']      = $entityDetails->entity_name;
                        $data['project_name']     = $projectDetails->projectNameEn;
                        $data['proposal_id']      = $model->proposal_unique_id;
                        $data['requested_amount'] = $model->requested_grant;
                        $data['approved_amount']  = $model->approved_grant;

                        Mail::to($entityDetails->email)->send(new Tightbudgetnotification($data));
                    }
                }
            }

            $message = 'Grant has been updated successfully!';
            return response()->json([
                'message' => $message,
                //'notifyUser' => $request->input('notifyUser'),
                //'notifyUserModel' => $model->notifyUser,
                'success' => true,
            ], 201);

        } catch (\Exception $e) {
            $allErrors = $e->getMessage();
            $message   = $allErrors . ' ' . 'Please try again!';
            return response()->json([
                'message' => $message,
                'success' => false,
            ], 405);
        }
    }

    public function assignUser(Request $request, $proposal_id)
    {
        $assignUser = DB::table('assign_user_to_proposals AS aup')
            ->leftJoin('users', 'users.id', '=', 'aup.user_id')
            ->where('aup.proposal_id', '=', $proposal_id)
            ->where('aup.status', '=', 1)
            ->select('users.admin_first_name', 'users.admin_last_name')
            ->get();

        return $assignUser[0]->admin_first_name . ' ' . $assignUser[0]->admin_last_name;
    }

    public function deleteBudgetBreak($id)
    {
        try {
            DB::table('budget_breakdowns')->delete($id);

            return response()->json([
                'message' => "Removed Successfully!!",
                'success' => true,
            ], 201);

        } catch (\Exception $e) {
            $allErrors = $e->getMessage();
            $message   = $allErrors . ' ' . 'Please try again!';
            return response()->json([
                'message' => $message,
                'success' => false,
            ], 409);
        }
    }

    /**
     * Display Pareo Types.
     *
     * @return \Illuminate\Http\Response
     */
    public function pareoType()
    {
        try {
            $pareoTypes = PareoType::with(['PareoCategory'])->get();

            return response()->json([
                'data'    => $pareoTypes,
                'success' => true,
            ], 201);

        } catch (\Exception $e) {
            $allErrors = $e->getMessage();
            $message   = $allErrors . ' ' . 'Please try again!';
            return response()->json([
                'message' => $message,
                'success' => false,
            ], 409);
        }
    }

    /**
     * Display Pareo Category.
     *
     * @return \Illuminate\Http\Response
     */
    public function pareoCategory($id)
    {
        try {
            $pareoCategories = PareoCategory::where('type_id', $id)->get();

            return response()->json([
                'data'    => $pareoCategories,
                'success' => true,
            ], 201);

        } catch (\Exception $e) {
            $allErrors = $e->getMessage();
            $message   = $allErrors . ' ' . 'Please try again!';
            return response()->json([
                'message' => $message,
                'success' => false,
            ], 409);
        }
    }

}
