<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Proposalapprove extends Mailable
{
    use Queueable, SerializesModels;
    public $entity_name;
    public $project_name;
    public $proposal_id;
    public $logo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($entity_name,$project_name,$proposal_id)
    {
        $this->entity_name = $entity_name;
        $this->project_name = $project_name;
        $this->proposal_id = $proposal_id;
        $this->logo = env('LOGO_URL');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Application Approve')->view('emails.proposalapprove');

        $data['entity_name'] = $this->entity_name;
        $data['project_name'] = $this->project_name;
        $data['proposal_id'] = $this->proposal_id;
        $data['logo'] = $this->logo;

        $this->withSwiftMessage(function ($data) {
            $data->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
        });
    }
}