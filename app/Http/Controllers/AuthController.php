<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\Accountverifyconf;
use App\Mail\Forgotpassword;
use App\Mail\Register;
use App\ProposalDocument;
use App\User;
use App\QuaterlyReport;

use App\Proposal;
use App\EntityMembers;

//import auth facades
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
//use App\Proposal;
use Illuminate\Support\Str;
use Tymon\JWTAuth\JWTAuth;


//use Mail; 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class AuthController extends Controller
{
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    public function __construct(JWTAuth $jwt)
    {
        $this->jwt = $jwt;
    }
    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postLogin(Request $request)
    {
        //$decrypted = $this->decrypt($request->EncryptionData);
        //return response()->json(['message' => $decrypted], 201);

        $this->validate($request, [
            'email'    => 'required|email|max:255',
            'password' => 'required',
        ]);

        try {

            $user = User::where('email', '=', $request->input('email'))
                ->where('is_verified', '1')
                ->where('activeStatus', '1')
                ->get();

            $userStatus = $user->count();

            if (!$token = $this->jwt->attempt($request->only('email', 'password'))) {
                return response()->json([
                    'message' => 'Email and password are not correct',
                    'success' => false,
                ], 400);
            } elseif ($userStatus != 1) {
                return response()->json([
                    'message' => 'Account is not verified!!',
                    'success' => false,
                ], 400);
            }

        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json([
                'message' => 'Token Expired',
                'success' => false,
            ], 500);

        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json([
                'message' => 'Token Invalid',
                'success' => false,
            ], 500);

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json([
                'message' => 'Token Absent!' . $e->getMessage(),
                'success' => false,
            ], 500);

        }

        if ($token) {
            $this->insertAuditTrail('AUDIT_TRAIL.LOGIN', 'AUDIT_TRAIL.LOGIN_SUCCESSFULLY');
        }

        return $this->respondWithToken($token);
        //return response()->json(compact('token'));
    }

    public function refresh()
    {
        try {
            $this->jwt->setToken($this->jwt->getToken());

            if ($this->jwt->invalidate()) {
                return $this->json([
                    'token' => $this->jwt->refresh(),
                ]);
            }

            return $this->json([], 403, $this->_lang['token_incorrect']);

        } catch (JWTException $e) {
            return $this->json([], 500, $e->getMessage());
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param  Request  $request
     * @return Response
     */
    public function loginXXX(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            'email'    => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function postRegister(Request $request)
    {
        //validate incoming request
        $this->validate($request, [
            //'name' => 'required|string',
            'email'            => 'required|email|unique:users',
            'category'         => 'required',
            'name_of_director' => 'required',
            'entity_name'      => 'required',
        ]);

        try {

            $user = new User;

            //$user->entity_code  =  bin2hex(openssl_random_pseudo_bytes(12));

            $user->email = $request->input('email');
            //$plainPassword = $request->input('password');
            $plainPassword             = Str::random(6);
            $user->password            = app('hash')->make($plainPassword);
            $user->user_type           = 1;
            $user->roleId              = 2; // always general user role
            $user->active_token        = Str::random(32);
            $user->category            = $request->input('category');
            $user->name_of_director    = $request->input('name_of_director');
            $user->entity_name         = $request->input('entity_name');
            $user->dateofincorporation = date("Y-m-d", strtotime($request->input('dateofincorporation')));
            $user->dateofregis         = date("Y-m-d", strtotime($request->input('dateofregis')));
            //$user->taxidnumber = $request->input('taxidnumber');
            $user->registrationnumber = $request->input('registrationnumber');
            $user->taxexmpt           = $request->input('taxexmpt');
            $user->taxexmptconfno     = $request->input('taxexmptconfno');
            if ($request->input('dateofissue') !== null) {
                $user->dateofissue = date("Y-m-d", strtotime($request->input('dateofissue')));
            }
            $user->phone           = $request->input('phone');
            $user->phone_ext       = $request->input('phone_ext');
            $user->mobile          = $request->input('mobile');
            $user->physicaladdr1   = $request->input('physicaladdr1');
            $user->physicaladdr2   = $request->input('physicaladdr2');
            $user->city            = $request->input('city');
            $user->state           = $request->input('state1');
            $user->zip             = $request->input('zip');
            $user->telephone       = $request->input('telephone');
            $user->fax             = $request->input('fax');
            $user->website         = $request->input('website');
            $user->issameadd       = $request->input('issameadd');
            $user->postal_addr1    = $request->input('postal_addr');
            $user->postal_addr2    = $request->input('postal_addr2');
            $user->postal_city     = $request->input('postal_city');
            $user->postal_state    = $request->input('postal_state');
            $user->postal_zip      = $request->input('postal_zip');
            $user->captchaverified = $request->input('captchaverified');
            $user->ipaddr          = $request->input('ipaddr');
            $user->vpass           = $plainPassword;

            $cityName = DB::table('cities')->where('id', $request->input('city'))->pluck('city');

            $gmap_physicaladdr1 = str_replace(" ", "+", $request->input('physicaladdr1'));
            $gmap_physicaladdr2 = str_replace(" ", "+", $request->input('physicaladdr2'));
            //$gmap_city = str_replace(" ","+",DB::table('cities')->where('id', $request->input('city'))->pluck('city'));
            $gmap_city  = str_replace(" ", "+", $cityName[0]);
            $gmap_state = 'PR';
            $gmap_zip   = $request->input('zip');

            $gmap_fulladdress = $gmap_physicaladdr1 . '+' . $gmap_physicaladdr2 . '+' . $gmap_city . '+' . $gmap_state . '+' . $gmap_zip;

            $API_KEY = "AIzaSyCEoKaMHSeBnIzNpzEq9c5ZPWImE7lCIZ8";

            //$gmap_result = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($gmap_fulladdress)."&key=".$API_KEY);

            $googleApiUrl = 'https://maps.googleapis.com/maps/api/geocode/json?key=' . $API_KEY . '&address=' . urlencode($gmap_fulladdress);

            $gmap_result     = file_get_contents($googleApiUrl);
            $gmap_result_arr = json_decode(utf8_encode($gmap_result), true);

            //$user->latitude = $gmap_result_arr['results'][0]['geometry']['location']['lat'];
            //$user->longitude = $gmap_result_arr['results'][0]['geometry']['location']['lng'];

            if (isset($gmap_result_arr['results'][0]) && $gmap_result_arr['status'] == "OK") {
                $user->latitude  = $gmap_result_arr['results'][0]['geometry']['location']['lat'];
                $user->longitude = $gmap_result_arr['results'][0]['geometry']['location']['lng'];
            }

            $data = array();
            if ($user->save()) {
                if ($user->save()) {
                    $code              = strtoupper(substr(str_replace(' ', '', $request->input('entity_name')), 0, 4));
                    $user->entity_code = $code . str_pad($user->id, 6, '0', STR_PAD_LEFT);
                    $user->save();
                }

                $name     = $request->input('entity_name');
                $email    = $request->input('email');
                $password = $plainPassword;
                $link     = env('FRONT_APP_URL') . '/pre-auth/accountverify/' . $user->entity_code . '/' . $user->active_token;
                Mail::to($email)->send(new Register($name, $email, $password, $link));

                return response()->json([
                    'message' => 'Registered Successfully!!'
                    , 'success' => true]
                    , 201);
            } else {
                return response()->json([
                    'message' => 'User Registration Failed!'
                    , 'success' => false]
                    , 409);
            }

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => $e->getMessage()], 409);
        }

    }

    public function fileUpload(Request $request)
    {

        $file          = $request->file('esFrontUrl');
        $user_id       = '1';
        $proposal_id   = '2';
        $document_type = 'Photo_id';
        $desc          = 'test';

        $notMovedFile = $this->moveFile($file, $user_id, $proposal_id, $document_type, $desc);

        try {
            return response()->json(['notMovedFiles' => $notMovedFile, 'message' => 'Success'], 201);
        } catch (\Exception $e) {
            return response()->json(['message' => 'File Upload Failed!'], 409);
        }
    }

    protected function moveFile($file, $user_id, $proposal_id, $document_type, $desc)
    {

        $fileCount = count((array) $file);

        try {
            $pd = new ProposalDocument;

            $notMoveFileArr = array();

            for ($i = 0; $i < $fileCount; $i++) {
                $originalFileName = $file[$i]->getClientOriginalName();
                $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
                $extension        = strtolower($file[$i]->getClientOriginalExtension());
                $fileName         = date('YmdHmsu') . '-' . $filteredOrgName;

                $fileSize = $file[$i]->getSize();

                $fileSize = number_format($fileSize / 1048576, 2);

                $destinationPath = "upload/proposal_documents/" . $proposal_id . "/";
                $fullFilePath    = $destinationPath . $fileName;

                if ($file[$i]->move($destinationPath, $fileName)) {

                    /* shell_exec ('gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH -sOutputFile='.$destinationPath.'test.pdf '.$fullFilePath.''); */

                    if ($fileSize > 4) {

                        $oldFullFilePath = $fullFilePath;

                        $fileName     = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                        $fullFilePath = $destinationPath . $fileName;

                        if ($extension == 'pdf') {

                            shell_exec('ps2pdf -dPDFSETTINGS=/default ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } else if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                            shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } /*else{
                        rename( $orgFullFilePath, $fullFilePath);
                        }*/

                        if (File::exists($oldFullFilePath)) {
                            File::delete($oldFullFilePath);
                        }
                    }

                    $pdArr[] = [
                        'user_id'       => $user_id,
                        'proposal_id'   => $proposal_id,
                        'document_type' => $document_type,
                        'desc'          => $desc,
                        'file_name'     => $fileName,
                        'file_path'     => $fullFilePath,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s'),
                    ];

                } else {
                    $notMoveFileArr[] = $originalFileName;
                }
            }
            $pd::insert($pdArr);
            return $notMoveFileArr;
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function fileDelete($id)
    {
        try {
            $pd = new ProposalDocument;

            $proposalDoc = $pd::find($id);

            $filename = $proposalDoc['file_path'];

            if (File::exists($filename)) {
                File::delete($filename);
                $proposalDoc->delete();
                return response()->json('Proposal document removed successfully');
            } else {
                return response()->json('File does not exist');
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function fileList($proposal_id)
    {
        try {
            $proposalDocs = ProposalDocument::where('proposal_id', '=', $proposal_id)->get();

            //dd($proposalDocs->getSql(), $proposalDocs->getBindings());

            return response()->json($proposalDocs);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function fileShow($id)
    {
        try {
            $proposalDoc = ProposalDocument::find($id);
            return response()->json($proposalDoc);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    /**
     * Logout JWT
     * @param Request $request
     * @return array
     * @throws \Tymon\JWTAuth\Exceptions\JWTException
     */
    public function postLogout(Request $request)
    {
        //$token =  $request->header('Authorization');
        /*$this->jwt->parseToken()->invalidate();
        return response()->json([
        'message' => 'Successfully logged out',
        'success'=> true
        ], 200);*/

        try {
            //if(Auth::user()->id){
            //$this->insertAuditTrail('AUDIT_TRAIL.LOGOUT','AUDIT_TRAIL.LOGOUT_SUCCESSFULLY');
            //}

            $this->jwt->parseToken()->invalidate();

            return response()->json([
                'message' => 'Successfully logged out',
                'success' => true,
            ], 200);
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            return response()->json([
                'message' => 'Token Expired',
                'success' => false,
            ], 500);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
            return response()->json([
                'message' => 'Token invalid',
                'success' => false,
            ], 500);
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json([
                'message' => 'Token Absent',
                'error'   => $e->getMessage(),
                'success' => false,
            ], 500);
        }
    }

    public function sendRegisterEmail()
    {
        try {
            $mailBody = "Welcome..";
            $to       = 'avijit.panja@indusnet.co.in';
            $name     = "Avijit Panja";
            $email    = "avijit.panja@indusnet.co.in";
            $password = "123";
            $link     = "https://www.impactocomunitariopr.org/pre-auth/verify/";
            Mail::to($to)->send(new Register($name, $email, $password, $link));

            return response()->json([
                'message' => 'Email send Successfully!!',
                'success' => true,
            ], 200);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function get_active_account($id, $token)
    {

        if ($id == "" && $token == "") {
            return response()->json(['error' => 'Oops! Something went wrong in this url.'], 409);
        }
        //$id = base64_decode($id);
        $model = User::where('entity_code', $id)->where('active_token', $token)->first();
        $email = $model->email;
        $name  = $model->entity_name;

        if (empty($model)) {
            return response()->json(['error' => 'Requested url is no longer valid. Please try again.'], 409);
        } else {
            $model->active_token = null;
            $model->is_verified  = '1';
            //$model->activeStatus = 1;
            if ($model->save()) {
                Mail::to($email)->send(new Accountverifyconf($name));
            }

            return response()->json([
                'message' => 'Your email has been verified successfully. Admin will activate your account shortly.',
                'success' => true,
            ], 200);
        }
    }

    public function get_log_list(Request $request)
    {
        $startDate = $request->get('startDate');
        $endDate   = $request->get('endDate');
        $userid    = $request->get('userid');

        $results = DB::table('audit_trails')
            ->leftJoin('users', 'audit_trails.user_id', '=', 'users.id')
            ->select('audit_trails.action_page', 'audit_trails.action_desc', 'audit_trails.action_on', 'audit_trails.ip_address', 'audit_trails.created_at', 'users.entity_name', 'users.admin_first_name', 'users.admin_last_name')
            ->whereBetween('audit_trails.created_at', [$startDate, $endDate])
            ->where('audit_trails.user_id', $userid)
            ->orderBy('audit_trails.id', 'DESC')
            ->paginate(10);

        return response()->json($results);
    }

    public function forgotPassword(Request $request)
    {
        $emailId = $request->input('email');

        $user = User::where('email', '=', $emailId)
            ->get();

        $token = Str::random(32);

        if (count($user) < 1) {
            return response()->json(['message' => 'Email does not exists'], 200);
        }

        DB::table('users')
            ->where('email', $emailId)
            ->update(['password_reset_token' => $token]);

        try {
            $name = $user[0]->entity_name;

            if (isset($user[0]->entity_code) && $user[0]->entity_code != null) {
                $link = 'https://www.impactocomunitariopr.org/pre-auth/forgotpasswordverify/' . $user[0]->entity_code . '/' . $token;
            } else {
                $link = 'https://www.impactocomunitariopr.org/pre-auth/forgotpasswordverify/' . $user[0]->id . '/' . $token;
            }
            Mail::to($emailId)->send(new Forgotpassword($name, $link));

            return response()->json(['message' => 'Reset password mail sent successfully!!'], 200);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => $e->getMessage()], 409);
        }

    }

    public function forgotPasswordVerify($id, $token)
    {

        if ($id == "" && $token == "") {
            return response()->json(['error' => 'Oops! Something went wrong in this url.'], 409);
        }
        //$id = base64_decode($id);
        $model = User::where('entity_code', $id)
            ->orWhere('id', $id)
            ->where('password_reset_token', $token)
            ->first();
        if (empty($model)) {
            return response()->json(['error' => 'Requested url is no longer valid. Please try again.'], 409);
        } else {
            /* $model->password_reset_token = NULL;
            $model->save(); */

            return response()->json([
                'message' => 'Reset your password',
                'success' => true,
            ], 200);
        }
    }

    public function postNewPassword(Request $request, $entity_code)
    {
        try {
            $model = User::where('entity_code', $entity_code)->orWhere('id', $entity_code)->first();

            $plainPassword = $request->input('password');

            $model->password = app('hash')->make($plainPassword);
            $model->vpass    = $plainPassword;

            $model->save();

            return response()->json([
                'message' => 'Password Reset Successfully!!',
                'success' => true,
            ], 200);

        } catch (\Exception $e) {
            //return error message
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    /**
     * Get all Users whoes proposal get approved.
     *
     * @return Response
     */
    public function getUsersWithApprovedProposal(Request $request)
    {
        $city     = $request->input('city');
        $category = $request->input('category');
        $keyword = $request->input('keyword');

        $select = DB::table('users AS u')
            ->leftJoin('proposals AS p', 'u.id', '=', 'p.user_id')
            ->leftJoin('projects AS pr', 'p.project_id', '=', 'pr.id')
            ->leftJoin('cities AS c', 'u.city', '=', 'c.id')
            ->where('p.application_status', '!=', '0')
            ->where('u.user_type', '=', 1);

        $select->select('u.entity_name', 'u.entity_code', 'p.application_status', 'p.proposal_unique_id', 'pr.projectNameEn', 'pr.projectNameEs', 'u.physicaladdr1', 'u.physicaladdr2', 'c.city', 'u.zip', 'u.email', 'u.phone', 'u.website', 'u.mobile', 'u.service_description', 'u.service_offered', 'u.logo_name', 'u.category', 'u.latitude', 'u.longitude', 'p.categories', 'p.population', 'p.community');    

        if ($city != '') {
            $select->where(function ($query) use ($city){
                $query->where('u.city', '=', $city);
            });
        } 

        if ($category != '') {
            $select->where(function ($query) use ($category){
                $query->where('u.category', '=', $category);
            });
        } 

        if ($keyword != '') {

            $select->where(function($query) use ($keyword) {
                $query->orWhere('u.entity_name', 'like', '%' . $keyword . '%')
                        ->orWhere('p.community', 'like', '%' . $keyword . '%')
                        ->orWhere('u.service_offered', 'like', '%' . $keyword . '%'); 
            });
        } 

        $users = $select->groupBy('u.id')->get();


       /* $select = DB::table('users AS u')
            ->leftJoin('proposals AS p', 'u.id', '=', 'p.user_id')
            ->leftJoin('projects AS pr', 'p.project_id', '=', 'pr.id')
            ->leftJoin('cities AS c', 'u.city', '=', 'c.id')
            ->where('p.application_status', '!=', '0')
            ->where('u.user_type', '=', 1);

        if ($city != '' && $category != '' && $keyword != '') {
            $users = $select
                ->where('u.city', '=', $city)
                ->where('u.category', '=', $category)
                ->where('u.entity_name', 'like', '%' . $keyword . '%')
                ->orWhere('p.community', 'like', '%' . $keyword . '%')
                ->orWhere('u.service_offered', 'like', '%' . $keyword . '%')
                ->select('u.entity_name', 'u.entity_code', 'p.application_status', 'p.proposal_unique_id', 'pr.projectNameEn', 'pr.projectNameEs', 'u.physicaladdr1', 'u.physicaladdr2', 'c.city', 'u.zip', 'u.email', 'u.phone', 'u.website', 'u.mobile', 'u.service_description', 'u.service_offered', 'u.logo_name', 'u.category', 'u.latitude', 'u.longitude', 'p.categories', 'p.population', 'p.community')
                ->groupBy('u.id')
                ->get();
        } else if ($city != '') {
            $users = $select
                ->where('u.city', '=', $city)
                ->select('u.entity_name', 'u.entity_code', 'p.application_status', 'p.proposal_unique_id', 'pr.projectNameEn', 'pr.projectNameEs', 'u.physicaladdr1', 'u.physicaladdr2', 'c.city', 'u.zip', 'u.email', 'u.phone', 'u.website', 'u.mobile', 'u.service_description', 'u.service_offered', 'u.logo_name', 'u.category', 'u.latitude', 'u.longitude', 'p.categories', 'p.population', 'p.community')
                ->groupBy('u.id')
                ->get();
        } else if ($category != '') {
            $users = $select
                ->where('u.category', '=', $category)
                ->select('u.entity_name', 'u.entity_code', 'p.application_status', 'p.proposal_unique_id', 'pr.projectNameEn', 'pr.projectNameEs', 'u.physicaladdr1', 'u.physicaladdr2', 'c.city', 'u.zip', 'u.email', 'u.phone', 'u.website', 'u.mobile', 'u.service_description', 'u.service_offered', 'u.logo_name', 'u.category', 'u.latitude', 'u.longitude', 'p.categories', 'p.population', 'p.community')
                ->groupBy('u.id')
                ->get();
        } else if ($keyword != '') {
            $users = $select
                ->where('u.entity_name', 'like', '%' . $keyword . '%')
                ->orWhere('p.community', 'like', '%' . $keyword . '%')
                ->orWhere('u.service_offered', 'like', '%' . $keyword . '%')
                ->select('u.entity_name', 'u.entity_code', 'p.application_status', 'p.proposal_unique_id', 'pr.projectNameEn', 'pr.projectNameEs', 'u.physicaladdr1', 'u.physicaladdr2', 'c.city', 'u.zip', 'u.email', 'u.phone', 'u.website', 'u.mobile', 'u.service_description', 'u.service_offered', 'u.logo_name', 'u.category', 'u.latitude', 'u.longitude', 'p.categories', 'p.population', 'p.community')
                ->groupBy('u.id')
                ->get();
        } else {

            $users = $select
                ->select('u.entity_name', 'u.entity_code', 'p.application_status', 'p.proposal_unique_id', 'pr.projectNameEn', 'pr.projectNameEs', 'u.physicaladdr1', 'u.physicaladdr2', 'c.city', 'u.zip', 'u.email', 'u.phone', 'u.website', 'u.mobile', 'u.service_description', 'u.service_offered', 'u.logo_name', 'u.category', 'u.latitude', 'u.longitude', 'p.categories', 'p.population', 'p.community')
                ->groupBy('u.id')
                ->get();

        }*/

        //Return message
        return response()->json([
            'data'    => $users,
            'success' => true,
        ], 200);

    }

    /**
     * Get all Users whoes proposal get approved.
     *
     * @return Response
     */
    public function getUsersWithApprovedProposalPaging(Request $request)
    {
        $pageSize = $request->input('pageSize');
        $city     = $request->input('cityId');
        $category = $request->input('category');
        $keyword = $request->input('keyword');

        //return response()->json($city);

        $select = DB::table('users AS u')
            ->leftJoin('proposals AS p', 'u.id', '=', 'p.user_id')
            ->leftJoin('projects AS pr', 'p.project_id', '=', 'pr.id')
            ->leftJoin('cities AS c', 'u.city', '=', 'c.id')
            ->where('p.application_status', '!=', '0')
            ->where('u.user_type', '=', 1);

        $select->select('u.entity_name', 'u.entity_code', 'p.application_status', 'p.proposal_unique_id', 'pr.projectNameEn', 'pr.projectNameEs', 'u.physicaladdr1', 'u.physicaladdr2', 'c.city', 'u.zip', 'u.email', 'u.phone', 'u.website', 'u.mobile', 'u.service_description', 'u.service_offered', 'u.logo_name', 'u.category', 'u.latitude', 'u.longitude', 'p.categories', 'p.population', 'p.community');    

        if ($city != '') {
            $select->where(function ($query) use ($city){
                $query->where('u.city', '=', $city);
            });
        } 

        if ($category != '') {
            $select->where(function ($query) use ($category){
                $query->where('u.category', '=', $category);
            });
        } 

        if ($keyword != '') {

            $select->where(function($query) use ($keyword) {
                $query->orWhere('u.entity_name', 'like', '%' . $keyword . '%')
                        ->orWhere('p.community', 'like', '%' . $keyword . '%')
                        ->orWhere('u.service_offered', 'like', '%' . $keyword . '%');
                        //->orWhere('p.population', 'like', '%' . $keyword . '%'); 
                        //->whereJsonContains('population', [['value' => $keyword , 'population' => true ]]);
            });
        } 

        //{"population":true,"id":1,"value":"0-3 Infants"}

        $users = $select->groupBy('u.id')->paginate($pageSize);

        //Return message
        return response()->json([
            'data'       => $users,
            'totalCount' => $users->count(),
            'success'    => true,
        ], 200);

    }

    public function getPdf($id, $language)
    {

        // return response()->json('PDF');
        $pdf = App::make('dompdf.wrapper');
        //$pdf->loadHTML('<h1>Test</h1>');

        $legalArr = $evidenceArr = array();

        $proposal = Proposal::with('proposalDocuments', 'proposalUser', 'assignUser', 'budgetBreakdown')->find($id);

        $project = DB::table('projects')
            ->where('id', '=', $proposal['project_id'])
            ->select('projectNameEn', 'projectNameEs')
            ->first();

        $city = DB::table('cities')
            ->where('id', '=', $proposal['city'])
            ->select('city')
            ->first();

        $pareo_types = DB::table('pareo_types')
            ->where('id', '=', $proposal['pareo_type'])
            ->select('type_name')
            ->first();

        $pareo_category = DB::table('pareo_categories')
            ->where('id', '=', $proposal['pareo_category'])
            ->select('category_name')
            ->first();

        $population = json_decode($proposal['population']);

        $evaluations = json_decode($proposal['statements_describes_type_of_evaluations']);
        $legal       = json_decode($proposal['legal']);

        if (!empty($legal)) {

            foreach ($legal as $legal_val) {
                if ($legal_val->status) {
                    $legalArr[] = Config::get('constants.PROPOSAL.' . $language . '.' . $legal_val->value);
                }
            }
        }

        if (!empty($proposal['proposalDocuments'])) {
            foreach ($proposal['proposalDocuments'] as $document) {
                if ($document->document_type == 'evidence_doc_file') {
                    $evidenceArr[Config::get('constants.EVIDENCE.' . $language . '.' . $document->desc)] = $document->org_file_name;
                }
            }
        }

        //echo '<pre>';
        //print_r($evidenceArr); die;

        if ($language == 'en') {
            $pdf->loadView('pdf.proposal', array(
                'proposal'       => $proposal,
                'population'     => $population,
                'evaluations'    => $evaluations,
                'legalArr'       => $legalArr,
                'evidenceArr'    => $evidenceArr,
                'project'        => $project,
                'city'           => $city,
                'pareo_types'    => Config::get('constants.PROPOSAL.' . $language . '.' . $pareo_types->type_name),
                'pareo_category' => Config::get('constants.PROPOSAL.' . $language . '.' . $pareo_category->category_name),
            ));
        } else {
            $pdf->loadView('pdf.proposalEs', array(
                'proposal'       => $proposal,
                'population'     => $population,
                'evaluations'    => $evaluations,
                'legalArr'       => $legalArr,
                'evidenceArr'    => $evidenceArr,
                'project'        => $project,
                'city'           => $city,
                'pareo_types'    => Config::get('constants.PROPOSAL.' . $language . '.' . $pareo_types->type_name),
                'pareo_category' => Config::get('constants.PROPOSAL.' . $language . '.' . $pareo_category->category_name),
            ));
        }

        //$pdf = PDF::loadView('pdf.proposal', $data);
        //return $pdf->stream();

        $pdfName = preg_replace("![^a-z0-9]+!i", "-", strtolower($proposal['proposal_unique_id']));

        return $pdf->download('proposal-'. $pdfName .'.pdf');
    }

    public function getQuaterlyPdf($proposal_id,$quaterStatus,$languageText)
    {
        // return response()->json('PDF');
        $pdf = App::make('dompdf.wrapper');
        //$pdf->loadHTML('<h1>Test</h1>');

        $legalArr = $evidenceArr = array();

        //$proposal1 = Proposal::with('proposalDocuments', 'proposalUser', 'assignUser', 'budgetBreakdown')->find($proposal_id);

        $proposal = DB::table('proposals')
                ->leftJoin('projects', 'projects.id', '=', 'proposals.project_id')
                ->leftJoin('users', 'users.id', '=', 'proposals.user_id')
                ->leftJoin('quaterly_reports AS qr', 'qr.proposal_id', '=', 'proposals.id')
                ->where('proposals.id', '=', $proposal_id)
                ->select('proposals.id', 'proposals.proposal_unique_id', 'proposals.application_status', 'proposals.created_at', 'proposals.updated_at', 'projects.projectNameEn', 'projects.projectNameEs', 'projects.startDate', 'projects.endDate', 'projects.period', 'users.entity_name', 'users.entity_code', 'qr.administrative_budget', 'qr.direct_budget', 'qr.ab_c1', 'qr.ab_c2', 'qr.ab_c3', 'qr.db_c1', 'qr.db_c2', 'qr.db_c3', 'qr.reportstatus1', 'qr.reportstatus2', 'qr.reportstatus3')
                ->first();

            $proposal->gross   = $proposal->administrative_budget + $proposal->direct_budget;
            $proposal->q1total = $proposal->ab_c1 + $proposal->db_c1;
            $proposal->q2total = $proposal->ab_c2 + $proposal->db_c2;
            $proposal->q3total = $proposal->ab_c3 + $proposal->db_c3;
            //$proposal->q4total = $proposal->ab_c4 + $proposal->db_c4;
            $proposal->alltotal = $proposal->q1total + $proposal->q2total + $proposal->q3total;
            $proposal->net      = $proposal->gross - $proposal->alltotal;
            $proposal->ab_total = $proposal->ab_c1 + $proposal->ab_c2 + $proposal->ab_c3;
            $proposal->db_total = $proposal->db_c1 + $proposal->db_c2 + $proposal->db_c3;

            if ($languageText == 'en') {
                if ($proposal->reportstatus1 == 0) {
                    $proposal->report_status_text = 'Not Filed';
                }elseif ($proposal->reportstatus1 == 1) {
                    $proposal->report_status_text = 'Submitted';
                }elseif ($proposal->reportstatus1 == 2) {
                    $proposal->report_status_text = 'More Information ';
                }else{
                    $proposal->report_status_text = 'Approved';
                }
            }else{
                if ($proposal->reportstatus1 == 0) {
                    $proposal->report_status_text = 'No archivado';
                }elseif ($proposal->reportstatus1 == 1) {
                    $proposal->report_status_text = 'Enviada';
                }elseif ($proposal->reportstatus1 == 2) {
                    $proposal->report_status_text = 'Más información';
                }else{
                    $proposal->report_status_text = 'Aprobada';
                }
            }


        // summary start
            $expenses_s= $this->getExpenseData($proposal_id,$quaterStatus,'Administrative');
        // summary end


        // Direct start
            $expenses_d= $this->getExpenseData($proposal_id,$quaterStatus,'Direct');
        // Direct end

        // get evidence
            $evidence = DB::table('evidence_documents AS ed')
                ->leftJoin('evidences AS ev', 'ev.id', '=', 'ed.evidence_id')
                ->where('ed.proposal_id', '=', $proposal_id)
                ->where('ed.quarter', '=', 'Q' . $quaterStatus)
                ->select('ev.evidence', 'ev.RO', 'ed.id', 'ed.org_file_name', 'ed.file_name', 'ed.file_path')
                ->get();
        // end evidence
        


        if ($languageText == 'en') {
            $pdf->loadView('pdf.quaterly-reportEn', array(
                'proposal'       => $proposal,
                'expenses_s'     => $expenses_s,
                'expenses_d'     => $expenses_d,
                'evidences'       => $evidence,
                'quaterStatus'   => $quaterStatus
            ));
        } else {
            $pdf->loadView('pdf.quaterly-reportEs', array(
                'proposal'       => $proposal,
                'expenses_s'       => $expenses_s,
                'expenses_d'       => $expenses_d,
                'evidences'       => $evidence,
                'quaterStatus'  => $quaterStatus
            ));
        }


        //$pdf = PDF::loadView('pdf.proposal', $data);
        //return $pdf->stream();

        $pdfName = preg_replace("![^a-z0-9]+!i", "-", strtolower($proposal->proposal_unique_id));

        return $pdf->download($pdfName .'-quaterly-reports-'. $quaterStatus . '.pdf');
    }


    public function getExpenseData($proposal_id,$quaterStatus,$budgetType){

        $expenses = DB::table('reported_expenses AS re')
                ->leftJoin('budget_breakdowns AS bb', 'bb.id', '=', 're.bb_id')
                ->where('bb.proposal_id', '=', $proposal_id)
                ->where('bb.budget_type', '=', $budgetType)
                ->where('re.quarter', '=', 'Q'.$quaterStatus)
                ->select('re.id', 're.bb_id', 're.quarter', 're.date', 're.expense_amount', 're.alert', 're.no_check', 're.infavourof', 'bb.description', 'bb.budget_type', 'bb.budget_amount', 'bb.remain_amount', 'bb.tight_budget', 'bb.reprog_budget1', 'bb.reprog_budget2', 'bb.reprog_budget3', 'bb.reprog_budget4', 'bb.reprog_budget5','bb.proposal_id')
                ->groupBy('bb_id')
                ->get();
                //->toSql();

        

            $excelData   = array();
            $pcnt        = 0;
            $statusNote  = '';
            $statusLabel = '';
            
        foreach ($expenses as $pval) {

            if ($expenses[$pcnt]->remain_amount > 0) {

                $statusNote  = 'fa-check green';
                $statusLabel = 'bellow_alert';
            }

            if ($expenses[$pcnt]->remain_amount < 0 ) {
                $statusNote  = 'fa-exclamation-triangle red';
                $statusLabel = 'over_alert';
            }

            $expenses[$pcnt]->note  = $statusNote;
            $expenses[$pcnt]->label = $statusLabel;

            $pcnt++;
        }

        return $expenses;
    }

    public function getEntityPdf($id, $language)
    {

        // return response()->json('PDF');
        $pdf = App::make('dompdf.wrapper');
        //$pdf->loadHTML('<h1>Test</h1>');

        $legalArr = $evidenceArr = array();

        $user = User::findOrFail($id);

        $city = DB::table('cities')
            ->where('id', '=', $user->city)
            ->select('city')
            ->first();

        $postal_city = DB::table('cities')
            ->where('id', '=', $user->postal_city)
            ->select('city')
            ->first();

        $members = EntityMembers::with(['cityinfo'])->where('user_id', '=', $id)->get();    
        
        //echo '<pre>';print_r($members); die;

        if ($language == 'en') {
            $pdf->loadView('pdf.profile', array(
                'user'       => $user,
                'members'           => $members,
                'city'           => $city->city,
                'postal_city'           => $postal_city->city,
            ));
        } else {
            $pdf->loadView('pdf.profileEs', array(
                'user'       => $user,
                'members'           => $members,
                'city'           => $city->city,
                'postal_city'           => $postal_city->city,
            ));
        }

        //$pdf = PDF::loadView('pdf.profile', $data);
        //return $pdf->stream();

        $pdfName = preg_replace("![^a-z0-9]+!i", "-", strtolower($user->entity_name));

        return $pdf->download('profile-'. $pdfName .'.pdf');
    }

    public function user_migrate()
    {
        $users = DB::table('T_Entidad')->get();

        $cities = DB::table('cities')->get();

        foreach ($cities as $cityVal) {
            $city[strtoupper($cityVal->city)] = $cityVal->id;
        }

        //return response()->json($city);

        foreach ($users as $userVal) {
            $usr[] = $userVal->Email;
            // $code = strtoupper(substr(str_replace(' ', '', $request->input('entity_name')), 0, 4)) ;
            // $user->entity_code  =  $code . str_pad($user->id, 6, '0', STR_PAD_LEFT);

            if ($userVal->IndExensionContributiva == 'N') {
                $IndExensionContributiva = 0;
            } else {
                $IndExensionContributiva = 1;
            }

            $plainPassword = Str::random(2);
            $password      = app('hash')->make($plainPassword);

            DB::table('users_temp')->insert(
                ['roleId' => 1
                    , 'email' => $userVal->Email
                    , 'password' => $password
                    , 'activeStatus' => 1
                    , 'is_verified' => 1
                    , 'user_type' => 1
                    , 'entity_name' => $userVal->EntidadPersona
                    , 'dateofincorporation' => date('Y-m-d', strtotime($userVal->FechaCorporacion))
                    , 'taxexmpt' => 0
                    , 'dateofissue' => date('Y-m-d', strtotime($userVal->FechaExensionContributiva))
                    , 'phone' => $userVal->Telefono
                    , 'mobile' => $userVal->Celular
                    , 'physicaladdr1' => $userVal->Direccion1
                    , 'physicaladdr2' => $userVal->Direccion2
                    , 'city' => $city[$userVal->CiudadDesc]
                    , 'state' => 'PR'
                    , 'zip' => $userVal->ZonaPostal
                    , 'fax' => $userVal->Fax
                    , 'postal_addr1' => $userVal->PosDireccion1
                    , 'postal_addr2' => $userVal->PosDireccion2
                    , 'postal_city' => $city[$userVal->PosCiudadDesc]
                    , 'postal_state' => 'PR'
                    , 'postal_zip' => $userVal->PosZonaPostal
                    , 'service_description' => $userVal->Servicio
                    , 'created_at' => date('Y-m-d H:i:s', strtotime($userVal->DateCreated))
                    , 'updated_at' => date('Y-m-d H:i:s', strtotime($userVal->LastUpdateDate)),
                ]
            );
        }
        return response()->json($usr);
    }

    /**
     * Get Start registration request count datewise.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getSearchCount(Request $request)
    {
        try {
            $city = $request->get('search');

            if ($city != '') {
                $totalCount = DB::table('users')->select(DB::raw('count(*) as tot, `category`'))
                    ->where('city', '=', $city)
                    ->where('user_type', '=', 1)
                    //->where('category', '!=', null)
                    ->groupBy('category')
                    ->get();

                $totalOrg = DB::table('users')
                    ->where('city', '=', $city)
                    ->where('user_type', '=', 1)
                    //->where('category', '!=', null)
                    ->get();
            } else {
                $totalCount = DB::table('users')->select(DB::raw('count(*) as tot, `category`'))
                    ->where('user_type', '=', 1)
                    //->where('category', '!=', null)
                    ->groupBy('category')
                    ->get();
                $totalOrg = DB::table('users')
                    ->where('user_type', '=', 1)
                    //->where('category', '!=', null)
                    ->get();

            }

            //Return message
            return response()->json([
                'data'     => $totalCount,
                'totalOrg' => $totalOrg->count(),
                'success'  => true,
            ], 200);
        } catch (\Exception $e) {
            $allErrors = $e->getMessage();
            $message   = $allErrors . ' ' . 'Please try again!';
            return response()->json([
                'message' => $message,
                'success' => false,
            ], 405);
        }
    }

}
