<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToReportedExpenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reported_expenses', function (Blueprint $table) {
            $table->double('remain_amount', 15, 8)->default(0)->nullable()->after('expense_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reported_expenses', function (Blueprint $table) {
            $table->dropColumn('remain_amount');
        });
    }
}
