<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('news_id')->comment('News Parent Id');
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->string('doc_name')->comment('Document name')->nullable();
            $table->string('doc_path')->comment('Document path')->nullable();
            $table->enum('status',[0,1])->nullable()->comment('0=>Unpublish,1=>Published');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_documents');
    }
}
