<?php
namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware(['auth']);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        $models = Product::all();
        return response()->json($models);
    }

    public function create(Request $request)
    {
        $model              = new Product;
        $model->name        = $request->name;
        $model->price       = $request->price;
        $model->description = $request->description;

        $model->save();
        return response()->json($model);
    }

    public function show($id)
    {
        $model = Product::find($id);
        return response()->json($model);
    }

    public function update(Request $request, $id)
    {
        $model = Product::find($id);

        $model->name        = $request->input('name');
        $model->price       = $request->input('price');
        $model->description = $request->input('description');
        $model->save();
        return response()->json($model);
    }

    public function destroy($id)
    {
        $model = Product::find($id);
        $model->delete();
        return response()->json('product removed successfully');
    }
}
