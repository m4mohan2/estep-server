<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Tightbudgetnotiadmin extends Mailable
{
    use Queueable, SerializesModels;
    public $entity_name;
    public $analyst_name;
    public $project_name;
    public $proposal_id;
    public $logo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->entity_name = $data['entity_name'];
        $this->analyst_name = $data['analyst_name'];
        $this->project_name = $data['project_name'];
        $this->proposal_id = $data['proposal_id'];
        $this->logo = env('LOGO_URL');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Tight budget notification')->view('emails.tightbudgetnotiadmin');

        $data['entity_name'] = $this->entity_name;
        $data['analyst_name'] = $this->analyst_name;
        $data['project_name'] = $this->project_name;
        $data['proposal_id'] = $this->proposal_id;
        $data['logo'] = $this->logo;

        $this->withSwiftMessage(function ($data) {
            $data->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
        });
    }
}