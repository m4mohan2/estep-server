<?php namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class AssignUserToProposal extends Model
{ 
	public $timestamps = false;
    protected $fillable = ['status'];
}