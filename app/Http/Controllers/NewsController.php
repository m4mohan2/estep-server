<?php 
namespace App\Http\Controllers;
use App\News;
use App\Video;
use App\NewsDocument;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);

class NewsController extends Controller
{
    /**
     * Instantiate a new NewsController instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::with(['newsDocuments'])->orderBy('id', 'DESC')->paginate(10);
        return response()->json($news);
    }

     /**
     * Store a newly created news.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'NewsTitle' => 'required',
            'NewsBody' => 'required',
            'NewsTitleSpanish' => 'required',
            'NewsBodySpanish' => 'required',
            'Status' => 'required',
            'CreatedBy' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $model = New News;

            $users = DB::table('users')->select('id')->get();
            $datetime = date('Y-m-d H:i:s');

            $model->NewsTitle          =  $request->input('NewsTitle');
            $model->NewsBody           =  $request->input('NewsBody');
            $model->NewsTitleSpanish   =  $request->input('NewsTitleSpanish');
            $model->NewsBodySpanish    =  $request->input('NewsBodySpanish');
            $model->Status     =  $request->input('Status');
            $model->ip_address =  $this->get_client_ip();
            $model->CreatedBy  =  $request->input('CreatedBy');
            $model->UpdatedBy  =  $request->input('UpdatedBy');
            $model->created_at =  date('Y-m-d H:i:s');
            $model->updated_at =  date('Y-m-d H:i:s');

            $model->slug   =  preg_replace("![^a-z0-9]+!i", "-", strtolower($model->NewsTitle));


            $news = News::where('slug', '=', $model->slug)->first();
            if ($news === null) {
               
                $model->save();
                $modelId = $model->id;

                $file = $request->file('esNewsDocUrl');

                //return response()->json($file);

                $fileCount = $this->moveFile($file,$modelId);

                /* foreach($users AS $userVal){
                    DB::insert('insert into push_notifications (module_name, module_id, user_id, read_status, created_at, updated_at) values (?, ?, ?, ?, ?, ?)', ['News', $modelId,$userVal->id, 0, $datetime, $datetime]);
                } */

                $this->insertAuditTrail('AUDIT_TRAIL.NEWS','AUDIT_TRAIL.NEWS_ADD','News Title:  '.$request->input('NewsTitle'));

                \DB::commit();
                $message = 'News has been added successfully!';
                $alerttype = true;

            }else{
                $message = 'News is already exist.';
                $alerttype = false;
            }
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors.' '.'Please try again!';
            $alerttype = false;

        }

        //Return message
        return response()->json([
            'message'   => $message,
            'fileCount' => $fileCount,
            'success'   => $alerttype,
        ], 201);
    }

    /**
     * Update existing news.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $this->validate($request, [
            'NewsTitle' => 'required',
            'NewsBody' => 'required',
            'NewsTitleSpanish' => 'required',
            'NewsBodySpanish' => 'required',
            'Status' => 'required',
            'UpdatedBy' => 'required'
        ]);

        \DB::beginTransaction();
        try {

            $model = News::find($id);

            if(!empty($model)){
                $model->NewsTitle          =  $request->input('NewsTitle');
                $model->NewsBody           =  $request->input('NewsBody');
                $model->NewsTitleSpanish   =  $request->input('NewsTitleSpanish');
                $model->NewsBodySpanish    =  $request->input('NewsBodySpanish');
                $model->Status             =  $request->input('Status');
                $model->UpdatedBy          =  $request->input('UpdatedBy');
                $model->updated_at         =  date('Y-m-d H:i:s');
                
                $news = News::where('slug', '=', $model->slug)->where('id', '!=' , $id)->first();

                if ($news === null) {
                    // news doesn't exist

                    $model->save();

                    $file = $request->file('esNewsDocUrl');

                    $fileCount = $this->moveFile($file,$id);

                    $this->insertAuditTrail('AUDIT_TRAIL.NEWS','AUDIT_TRAIL.NEWS_EDIT','News Title:  '.$request->input('NewsTitle'));

                    \DB::commit();
                    $message = 'News has been updated successfully!';
                    $alerttype = true;

                }else{
                    $message = 'News is already exist.';
                    $alerttype = false;
                }
            }else{
                $message = 'News not exists!';
                $alerttype = true;
            }
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors . ' '.'Please try again!';
            $alerttype = false;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }

    public function show($id)
    {
        $news = News::with(['newsDocuments'])->find($id);
        return response()->json($news);
    }

    public function newsDetails($slug)
    {
        
        $news = News::with(['newsDocuments'])->where('slug', '=', $slug)->get();
        
        return response()->json($news);
    }


    public function delete($id){
        $news = News::find($id);
        $news->delete();

        $newsDocs = NewsDocument::where('news_id', '=', $id)->get();

        foreach($newsDocs AS $newsDocVal){
            $filename = $newsDocVal['doc_path'];

            if (File::exists($filename)) {
                File::delete($filename);
                NewsDocument::delete($newsDocVal['id']);
            }
        }

        $this->insertAuditTrail('AUDIT_TRAIL.NEWS','AUDIT_TRAIL.NEWS_DELETE','News Title:  '.$news->NewsTitle);

        return response()->json([
            'message' => 'News removed successfully',
            'success' => true,
        ], 200);
    }

    protected function moveFile($file,$news_id){

        $fileCount = count((array)$file);

        try {
            $nd = new NewsDocument;

            $notMoveFileArr = array();

            for ($i=0; $i<$fileCount; $i++) {
                    $originalFileName = $file[$i]->getClientOriginalName();
                    $filteredOrgName = preg_replace('/[^a-zA-Z0-9-_\.]/','', $originalFileName);
                    $extension = strtolower($file[$i]->getClientOriginalExtension());
                    $fileName = date('YmdHmsu').'-'.$filteredOrgName;

                    $fileSize = $file[$i]->getSize();

                    $fileSize = number_format($fileSize / 1048576,2);

                    $destinationPath = "upload/news_documents/".$news_id."/";
                    $fullFilePath = $destinationPath.$fileName;

                if($file[$i]->move($destinationPath,$fileName)){

                    if($fileSize > 6){

                        $oldFullFilePath = $fullFilePath;

                        $fileName = date('YmdHmsu').'-converted-'.$filteredOrgName;
                        $fullFilePath = $destinationPath.$fileName;

                        if($extension == 'pdf'){

                            shell_exec ('ps2pdf -dPDFSETTINGS=/default '.$oldFullFilePath.'  '.$fullFilePath.'');

                        }else if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff'){

                            shell_exec ('convert -quality 50% '.$oldFullFilePath.'  '.$fullFilePath.'');

                        } /*else{
                            rename( $orgFullFilePath, $fullFilePath);
                        }*/

                        if (File::exists($oldFullFilePath)) {
                            File::delete($oldFullFilePath);
                        }
                    }

                    $ndArr[] =[
                        'news_id'       => $news_id,
                        'doc_name'      => $fileName,
                        'doc_path'      => $fullFilePath,
                        'status'        => 1,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s')
                       ]; 
                       
                }else{
                    $notMoveFileArr[] = $originalFileName;
                }
            }
            $nd::insert($ndArr);
        return $fileCount;
        }catch(\Exception $e){
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function newsFileDelete($id){
        try {
            
            $newsDoc = NewsDocument::find($id);
            $filename = $newsDoc['doc_path'];

            if (File::exists($filename)) {
                File::delete($filename);
                return response()->json('News document removed successfully');
            }
            $newsDoc->delete();

        }catch(\Exception $e){
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }


     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function activeNews(Request $request)
    {
        $perPage = 10;
        
        $news = News::with(['newsDocuments'])
                    ->where('Status', '1')
                    ->orderBy('id', 'DESC')
                    ->paginate($perPage);
        return response()->json($news);
    }

    protected function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function videos()
    {        
        $news = Video::where('status', '=', '1')->get();        
        return response()->json($news);
    }
}