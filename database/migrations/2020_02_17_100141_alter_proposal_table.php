<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProposalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->enum('direct_budget_file_status',[0,1,2,3])->default(0)->after('inventory_of_property')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('administrative_budget_file_status',[0,1,2,3])->default(0)->after('inventory_of_property')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->dropColumn('budget_file_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->dropColumn('administrative_budget_file_status');
            $table->dropColumn('direct_budget_file_status');
        });
    }
}
