<?php namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class News extends Model
{ 
	public $timestamps = true;

	public function newsDocuments()
    {
        return $this->hasMany('App\NewsDocument');
    } 
    
}