<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;

use App\Mail\Register;

class EmailController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
       
        //
    }

    public function send() {
        try{
            $mailBody = "Welcome..";
            $to = 'avijit.panja@indusnet.co.in';
            $name = "Avijit Panja";
            $email = "avijit.panja@indusnet.co.in";
            $password = "123";
            $link = "http://34.196.238.149/staging_esteps/pre-auth/verify/";
            Mail::to($to)->send(new Register($name,$email,$password,$link));

            return response()->json([
                'message' => 'Email send Successfully!!',
                'success'=> true
            ], 200);

        }catch (\Exception $e) {
            //return error message
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

}
