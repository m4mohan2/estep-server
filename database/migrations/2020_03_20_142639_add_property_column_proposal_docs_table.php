<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPropertyColumnProposalDocsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposal_documents', function (Blueprint $table) {
            $table->tinyInteger('rented')->default(0)->after('desc');
            $table->string('monthly_payment')->nullable()->after('desc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposal_documents', function (Blueprint $table) {
            $table->dropColumn('rented');
            $table->dropColumn('monthly_payment');
        });
    }
}
