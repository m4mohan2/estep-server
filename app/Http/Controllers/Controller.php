<?php

namespace App\Http\Controllers;

//import auth facades
use Illuminate\Support\Facades\Auth;
use App\User;
use App\AuditTrail;
use App\ProposalLog;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
  
  public function __construct() {
    $this->middleware('OperationHandler');
  }
    //Add this method to the Controller class
	protected function respondWithToken($token)
	{

    $details = User::with([
                    'role'
            ])->where('id','=', Auth::user()->id )->first();

	    return response()->json([
	        'accessToken' => $token,
	        //'token_type' => 'bearer',
	        // 'expires_in' => Auth::factory()->getTTL() * 60,
          //'user' => Auth::user(),
          'user' => $details,
          'message' => "Login Successful",
          'success' => true
	    ], 200);
	}

  protected function decrypt($ciphertext) {
        $password = env('ENC_KEY', '');
        $ciphertext = base64_decode($ciphertext);
        if (substr($ciphertext, 0, 8) != "Salted__") {
            return false;
        }
        $salt = substr($ciphertext, 8, 8);
        $keyAndIV = $this->evpKDF($password, $salt);
        $decryptPassword = openssl_decrypt(
                substr($ciphertext, 16), 
                "aes-256-cbc",
                $keyAndIV["key"], 
                OPENSSL_RAW_DATA, // base64 was already decoded
                $keyAndIV["iv"]);

        return $decryptPassword;
    }

  protected function evpKDF($password, $salt, $keySize = 8, $ivSize = 4, $iterations = 1, $hashAlgorithm = "md5") {
      $targetKeySize = $keySize + $ivSize;
      $derivedBytes = "";
      $numberOfDerivedWords = 0;
      $block = NULL;
      $hasher = hash_init($hashAlgorithm);
      while ($numberOfDerivedWords < $targetKeySize) {
          if ($block != NULL) {
              hash_update($hasher, $block);
          }
          hash_update($hasher, $password);
          hash_update($hasher, $salt);
          $block = hash_final($hasher, TRUE);
          $hasher = hash_init($hashAlgorithm);

          // Iterations
          for ($i = 1; $i < $iterations; $i++) {
              hash_update($hasher, $block);
              $block = hash_final($hasher, TRUE);
              $hasher = hash_init($hashAlgorithm);
          }

          $derivedBytes .= substr($block, 0, min(strlen($block), ($targetKeySize - $numberOfDerivedWords) * 4));

          $numberOfDerivedWords += strlen($block)/4;
      }

      return array(
          "key" => substr($derivedBytes, 0, $keySize * 4),
          "iv"  => substr($derivedBytes, $keySize * 4, $ivSize * 4)
      );
    }

    protected function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    protected function insertAuditTrail($action_page,$action_desc,$action_on=null){
      $model = new AuditTrail;
      $model->action_page  =  $action_page;
      $model->action_desc = $action_desc;
      $model->action_on = $action_on;
      $model->ip_address = $this->get_client_ip();
      $model->user_id = Auth::user()->id;
      $model->save();
    }


    protected function createProposalLogs($proposal_id, $desc, $info=''){
      $model = new ProposalLog;
      $model->proposal_id = $proposal_id;
      $model->desc = $desc;
      $model->info = $info;
      $model->ip_address = $this->get_client_ip();
      $model->user_id = Auth::user()->id;
      $model->save();
    }

}
