<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('service_description')->nullable()->after('ipaddr');
            $table->text('service_offered')->nullable()->after('service_description');
            $table->string('org_logo_name')->nullable()->after('service_offered')->comment('Original File Name');
            $table->string('logo_name')->nullable()->after('org_logo_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('service_description');
            $table->dropColumn('service_offered');
            $table->dropColumn('org_logo_name');
            $table->dropColumn('logo_name');
        });
    }
}
