<div style="float:left; width:100%; text-align:center; background:#EEEEEE;padding:30px 0;">
     <div style="max-width:100%; width:600px; padding:0px; margin:0 auto;">
        <div style="width:100%; background:#1F2C47; padding:30px 20px; float:left; text-align:center">
        </div>
        <div style="font-size:15px;float:left; width:100%; background:#fff; font-family:Tahoma, Geneva, sans-serif; text-align:left; color:#1F2C47; padding:20px;">
            <p><strong>Hi {{ $name }} ,</strong></p>
            <p>Please check the login credential below.<p>
            <table align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%">
              <tbody>
                <tr>
                  <td><b>Email :</b></td>
                  <td align="left">{{ $email }}</td>
                </tr>
                <tr>
                  <td><b>Password :</b></td>
                  <td align="left">{{ $password }}</td>
                </tr>
              </tbody>
            </table>
            <p>&nbsp;</p>
            <p>Please activate your account account by clicking <a href="{{ $link }}" target="_blank">here</a><p>

            <p>Thanks and regards,<br />
          	 Admin</p>
        </div>
        <div style="width:100%; background:#1F2C47; padding:30px 20px; float:left; text-align:center;color:#989898">
            <p></p>
        </div>
     </div>
</div>