<!DOCTYPE html
	PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="author" content="INT" />
	<meta name="description" content="Tangent" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<title>Quaterly Report</title>
</head>

<body>
	<table align="center" border="0" cellpadding="0" cellspacing="0" style="padding: 0; margin: 0 auto; width:100%;">
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 10px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0;">
				<table align=" center" border="0" cellpadding="0" cellspacing="0"
					style="padding: 0; margin: 0 auto; width:100%;">
					<tr>
						<td style="padding: 0; margin: 0; width: 100px;">
							<img src="https://www.impactocomunitariopr.org/assets/images/logo.png" alt="" width="100px" />
						</td>
						<td
							style="padding: 0; margin: 0; text-align: center; font-size: 30px; line-height: 35px; font-weight: bold; font-family: Arial, Helvetica, sans-serif; color: #000; text-transform: uppercase;">
							Quarterly Report
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 20px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td width="50%"
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Entity Name</strong></div>
				<div>{{ $proposal->entity_name }}</div>
			</td>
			<td
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Entity Code</strong></div>
				<div>{{ $proposal->entity_code }}</div>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 10px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td width="50%"
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Application Date</strong></div>
				<div>{{ date('m-d-Y', strtotime($proposal->created_at)) }}</div>
			</td>
			<td
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Proposal ID</strong></div>
				<div>{{ $proposal->proposal_unique_id }}</div>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 20px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2"
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Expense Summary</strong></div>
				<table align=" center" border="0" cellpadding="0" cellspacing="0"
					style="padding: 0; margin: 0 auto; width:100%; border:solid 1px #ccc; border-bottom: 0;">
					<tr>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
						</th>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
							Budget</th>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
							Q1</th>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
							Q2</th>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
							Q3</th>
						<th valign="top" align="center"
							style="padding: 5px 0; margin: 0; border-bottom:solid 1px #ccc;">
							Totals</th>
					</tr>
					<tr>
						<td valign="top" align="left" style="padding: 0; margin: 0; border-bottom:solid 1px #ccc;">
							<div style="padding: 10px; border-radius:5px;">Administrative</div>
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->administrative_budget, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc;  box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->ab_c1, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->ab_c2, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->ab_c3, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->ab_total, 2, '.', ',') }}
						</td>
					</tr>
					<tr>
						<td valign="top" align="left" style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc;">
							Direct</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->direct_budget, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->db_c1, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->db_c2, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->db_c3, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->db_total, 2, '.', ',') }}
						</td>
					</tr>
					<tr>
						<td valign="top" align="left" style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc;">
							Totals</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->gross, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->q1total, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->q3total, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->alltotal, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->alltotal, 2, '.', ',') }}
						</td>
					</tr>
					<tr>
						<td valign="top" align="left" style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc;">
							Informed</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->alltotal, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
					</tr>
					<tr>
						<td valign="top" align="left" style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc;">
							Net</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
							$ {{ number_format($proposal->net, 2, '.', ',') }}
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
						<td valign="top" align="right"
							style="padding: 10px; margin: 0; border-bottom:solid 1px #ccc; box-shadow: 0 0 3px #9a9a9a;">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 20px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td width="50%"
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				<div style="padding: 0 0 5px 0;"><strong>Report Status</strong></div>
				<div>{{ $proposal->report_status_text }}</div>
			</td>
			<td
				style="padding: 0; margin: 0; font-size: 17px; line-height: 25px; font-family: Arial, Helvetica, sans-serif; color: #000;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 0; margin: 0; height: 20px;">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="2"
				style="padding: 0; margin: 0; border:solid 1px #dfdfdf; font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">
				<div
					style="padding: 10px; color: #800020; font-size: 20px; background-color: #f7f7f7; border-bottom:solid 1px #dfdfdf;">
					Administrative Summary
				</div>
				<div style="padding: 10px;">
					<table align=" center" border="0" cellpadding="0" cellspacing="0"
						style="padding: 0; margin: 0 auto; width:100%; border:solid 1px #ccc; border-bottom: 0;">
						<tr>
							<th valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
								Expenditure
							</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">Budget
							</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">Remain
								Amount</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">&nbsp;
							</th>
						</tr>

						@foreach ($expenses_s as $expense)
							<tr>
								<td valign="top" align="center"
									style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
									{{ $expense->description }}
								</td>
								<td valign="top" align="center"
									style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
									${{ number_format($expense->tight_budget, 2, '.', ',') }}
								</td>
								<td valign="top" align="center"
									style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
									${{ number_format($expense->remain_amount, 2, '.', ',') }}
								</td>
								<td valign="top" align="center"
									style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
									<div style="font-size: 18px; font-weight: bold; color: green;">&#10003;</div>
								</td>
							</tr>

						@endforeach

					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2"
				style="padding: 0; margin: 0; border:solid 1px #dfdfdf; font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">
				<div
					style="padding: 10px; color: #800020; font-size: 20px; background-color: #f7f7f7; border-bottom:solid 1px #dfdfdf;">
					Direct Summary
				</div>
				<div style="padding: 10px;">
					<table align=" center" border="0" cellpadding="0" cellspacing="0"
						style="padding: 0; margin: 0 auto; width:100%; border:solid 1px #ccc; border-bottom: 0;">
						<tr>
							<th valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
								Expenditure
							</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">Budget
							</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">Remain
								Amount</th>
							<th width="20%" valign="top" align="center"
								style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">&nbsp;
							</th>
						</tr>

						@foreach ($expenses_d as $expense)
							<tr>
								<td valign="top" align="center"
									style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
									{{ $expense->description }}
								</td>
								<td valign="top" align="center"
									style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
									${{ number_format($expense->tight_budget, 2, '.', ',') }}
								</td>
								<td valign="top" align="center"
									style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
									${{ number_format($expense->tight_budget, 2, '.', ',') }}
								</td>
								<td valign="top" align="center"
									style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
									<div style="font-size: 18px; font-weight: bold; color: green;">&#10003;</div>
								</td>
							</tr>
						@endforeach

					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2"
				style="padding: 0; margin: 0; border:solid 1px #dfdfdf; font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 24px;">
				<div
					style="padding: 10px; color: #800020; font-size: 20px; background-color: #f7f7f7; border-bottom:solid 1px #dfdfdf;">
					Evidence
				</div>
				<div style="padding: 10px;">
					<table align="center" border="0" cellpadding="0" cellspacing="0"
						style="padding: 0; margin: 0 auto; width:100%;">

						<tr>
							<td colspan="2" style="padding: 0; margin: 0; height: 20px;">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding: 0; margin: 0;">
								<table align=" center" border="0" cellpadding="0" cellspacing="0"
									style="padding: 0; margin: 0 auto; width:100%; border:solid 1px #ccc; border-bottom: 0;">
									<tr>
										<th width="10%" valign="top" align="center"
											style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
											ID
										</th>
										<th width="40%" valign="top" align="center"
											style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
											Evidence
										</th>
										<th width="10%" valign="top" align="center"
											style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
											R/O
										</th>
										<th width="40%" valign="top" align="center"
											style="padding: 5px; margin: 0; color: #013b85; border-bottom:solid 1px #ccc;">
											Document
										</th>
									</tr>

									@foreach ($evidences as $evidence)
										<tr>
											<td valign="top" align="center"
												style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
												{{ $evidence->id }}
											</td>
											<td valign="top" align="center"
												style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
												{{ $evidence->evidence }}
											</td>
											<td valign="top" align="center"
												style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
												{{ $evidence->RO }}
											</td>
											<td valign="top" align="center"
												style="padding: 5px; margin: 0; font-size: 15px; border-bottom:solid 1px #ccc;">
												{{ $evidence->org_file_name }}
											</td>
										</tr>
									@endforeach
									
								</table>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
</body>

</html>