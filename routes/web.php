<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'api'], function() use($router){
});

// API route group
$router->group(['prefix' => 'api'], function () use ($router) {

    
    $router->put('refresh', 'AuthController@refresh');
	$router->post('register', 'AuthController@postRegister');
	$router->post('login', 'AuthController@postLogin');
	$router->post('logout', 'AuthController@postLogout');
	$router->get('sendRegisterEmail', 'AuthController@sendRegisterEmail');
	$router->get('verify/{id}/{token}', 'AuthController@get_active_account');
	$router->post('forgotpassword', 'AuthController@forgotPassword');
	$router->get('forgotpasswordverify/{id}/{token}', 'AuthController@forgotPasswordVerify');
	$router->post('savepassword/{id}', 'AuthController@postNewPassword');

	$router->get('getPdf/{id}/{language}', 'AuthController@getPdf');
	$router->get('getEntityPdf/{id}/{language}', 'AuthController@getEntityPdf');

	$router->get('getQuaterlyPdf/{proposal_id}/{id}/{language}', 'AuthController@getQuaterlyPdf');

	$router->get('getUsersWithApprovedProposal', 'AuthController@getUsersWithApprovedProposal');
	$router->get('searchProponentList', 'AuthController@getUsersWithApprovedProposalPaging');

	$router->get('send', 'EmailController@send');

	//$router->get('adminUserList', 'UserController@allUsers');
	$router->post('fileUpload', 'AuthController@fileUpload');
	$router->get('fileList/{pid}', 'AuthController@fileList');
	$router->get('fileShow/{id}', 'AuthController@fileShow');
	$router->delete('fileDelete/{id}', 'AuthController@fileDelete');
	$router->get('getLogList', 'AuthController@get_log_list');

	// admin user api
	$router->get('getAssignedUsers', 'UserController@getAssignedUsers');
	$router->get('adminUserList', 'UserController@adminUserList');
	$router->get('allAdminUsersPaging', 'UserController@allAdminUsersPaging');
	$router->post('adminUserAdd', 'UserController@addAdminUser');
	$router->put('adminUserUpdate/{id}', 'UserController@updateAdminUser');
	$router->post('changePassword', 'UserController@post_change_password');

	
	$router->post('fileUpload', 'AuthController@fileUpload');

	$router->get('user_migrate', 'AuthController@user_migrate');
	
	//state api 
	$router->get('/states', 'StateController@index');
	//$router->post('/state', 'StateController@create');
	//$router->get('/state/{id}', 'StateController@show');
	//$router->put('/state/{id}', 'StateController@update');
	//$router->delete('/state/{id}', 'StateController@destroy');

	// city api
	$router->get('/cities', 'CityController@index');
	$router->get('/getCities', 'CityController@getCitySearch');

	//contact api
	$router->post('/addContact', 'ContactController@create');
	$router->post('/sendContact', 'ContactController@sendContact');
	$router->get('/getSearchCount', 'AuthController@getSearchCount');
});


//$router->group(['prefix'=>'api' , 'middleware' => 'auth' ], function() use($router){

$router->group(['prefix' => 'api'], function () use ($router) {

	//users
	$router->get('profile', 'UserController@profile');
	$router->get('users/{id}', 'UserController@singleUser');
	$router->get('userList', 'UserController@allUsers');
	$router->post('editStatus', 'UserController@editStatus');
	$router->post('updateUser', 'UserController@updateUser');
	$router->get('getAllUsers', 'UserController@getAllUsers');
	$router->post('addEntityMember', 'UserController@addEntityMember');
	$router->post('updateEntityMember/{id}','UserController@updateEntityMember');
	$router->get('singleMember/{id}','UserController@singleMember');
	$router->get('getMembers/{id}','UserController@getMembers');
	$router->delete('deleteMember/{id}', 'UserController@deleteMember');

	$router->post('moveLogo','UserController@moveLogo');


	//Proposal
	$router->get('/listProposal', 'ProposalController@index');
	$router->post('/addProposal', 'ProposalController@create');
	$router->get('/proposal/{id}', 'ProposalController@show');
	$router->post('/editProposal/{id}', 'ProposalController@update');
	$router->delete('/proposal/{id}', 'ProposalController@destroy');
	$router->delete('/proposalDoc/{file_id}', 'ProposalController@fileDelete');
	$router->post('/rejectProposal/{id}', 'ProposalController@rejectProposal');
	$router->post('/requestMoreInfo', 'ProposalController@requestMoreInfo');
	$router->post('/acceptProposal/{id}', 'ProposalController@acceptProposal');
	$router->post('/reprogramProposal/{id}', 'ProposalController@reprogramProposal');
	$router->post('/requestreprogramProposal/{id}', 'ProposalController@requestreprogramProposal');
	$router->post('/rejectGrant/{id}', 'ProposalController@rejectGrant');
	$router->post('/updateGrant/{id}', 'ProposalController@updateGrant');
	$router->delete('/deleteBudgetBreak/{id}', 'ProposalController@deleteBudgetBreak');
	$router->get('/pareoType', 'ProposalController@pareoType');
	$router->get('/pareoCategory/{id}', 'ProposalController@pareoCategory');
	$router->post('/paidStatus/{id}', 'ProposalController@paidStatus');
	$router->post('/waitingStatus/{id}', 'ProposalController@waitingStatus');
	

	//Proposal Notes
	$router->post('/addNotes', 'NotesController@create');
	$router->delete('/deleteNote/{id}', 'NotesController@delete');
	$router->get('/noteList/{proposal_id}', 'NotesController@list');

	//Proposal More Info
	$router->post('/addMoreinfo', 'MoreinfoController@create');
	$router->delete('/deleteMoreinfo/{id}', 'MoreinfoController@delete');
	$router->get('/moreinfoList/{proposal_id}', 'MoreinfoController@list');
	
	//Proposal attach doc from admin
	$router->post('/attachFile', 'ProposalController@moveFileByAdmin');
	$router->get('/getAttachFileList/{proposal_id}', 'ProposalController@getAttachFileList');
	
	//Dashboard APIs
	$router->get('/proposalCount', 'DashboardController@index');
	$router->get('/datewiseProposalCount', 'DashboardController@datewiseProposal');
	$router->get('/getRegistrationRequestCount', 'DashboardController@getRegistrationRequestCount');

	//Project APIs
	$router->get('/project', 'ProjectController@index');
	$router->get('/getProjetList', 'ProjectController@getProjetList');
	$router->get('/getBlockedProjetList', 'ProposalController@getBlockedProjetList');
	$router->post('/addProject', 'ProjectController@create');
	$router->put('/editProject/{id}', 'ProjectController@update');
	$router->get('/project/{id}', 'ProjectController@show');
	//$router->delete('/project/{id}', 'ProjectController@delete');

	//Quarterly Report
	$router->post('/addQuarterlyReport', 'QuaterlyreportController@create');
	$router->get('/getUserProposalDetails/{proposal_id}', 'QuaterlyreportController@getUserProposalDetails');
	$router->get('/getExpenditure/{proposal_id}/{budget_type}','QuaterlyreportController@getExpenditure');
	$router->post('/addExpenses','QuaterlyreportController@addExpenses');
	$router->get('/getExpenseList/{proposal_id}/{budget_type}/{quater}','QuaterlyreportController@getExpenseList');
	$router->get('/getSummaryList/{proposal_id}/{budget_type}/{quater}','QuaterlyreportController@getSummaryList');
	$router->delete('/deleteExpense/{id}','QuaterlyreportController@deleteExpense');
	$router->get('/getEvidences','QuaterlyreportController@getEvidences');
	$router->post('/moveEvidenseFile','QuaterlyreportController@moveEvidenseFile');
	$router->get('/getEvidencefileList/{proposal_id}/{quater}','QuaterlyreportController@getEvidencefileList');
	$router->delete('/evidencefileDelete/{id}','QuaterlyreportController@evidencefileDelete');
	$router->post('/updateExpense/{id}','QuaterlyreportController@updateExpense');
	$router->get('/getUpdateBugetAmount/{proposal_id}','QuaterlyreportController@getUpdateBugetAmount');
	
	
	//Assign proposal
	$router->post('/assignUser', 'AssignuserController@create');
	$router->get('/assignUser/{id}', 'AssignuserController@show');

	//role api
	$router->get('/getRole', 'RoleController@index');
	$router->post('/addRole', 'RoleController@create');
	$router->put('/editRolePermission/{id}', 'RoleController@update');
	$router->get('/role/{id}', 'RoleController@show');
	$router->delete('/deleteRolePermission/{id}', 'RoleController@destroy');

	//news api
	$router->get('/news', 'NewsController@index');
	$router->post('/addNews', 'NewsController@create');
	$router->post('/editNews/{id}', 'NewsController@update');
	$router->get('/news/{id}', 'NewsController@show');
	$router->delete('/news/{id}', 'NewsController@delete');
	$router->delete('newsFileDelete/{id}', 'NewsController@newsFileDelete');
	$router->get('/activeNews', 'NewsController@activeNews');
	$router->get('/newsDetails/{slug}', 'NewsController@newsDetails');
	$router->get('/videos', 'NewsController@videos');

	//video api
	$router->get('/video', 'VideoController@index');
	$router->post('/addVideo', 'VideoController@create');
	$router->post('/editVideo/{id}', 'VideoController@update');
	$router->get('/video/{id}', 'VideoController@show');
	$router->delete('/video/{id}', 'VideoController@delete');
	//$router->delete('videoFileDelete/{id}', 'VideoController@videoFileDelete');
	$router->get('/activeVideo', 'VideoController@activeVideo');

	
	//cms/pages api
	$router->get('/page', 'PageController@index');
	$router->post('/addPage', 'PageController@create');
	$router->post('/editPage/{id}', 'PageController@update');
	$router->get('/page/{id}', 'PageController@show');
	$router->delete('/page/{id}', 'PageController@delete');
	$router->get('/pagecontent', 'PageController@pageContent');

	$router->get('/products', 'ProductController@index');
	$router->post('/product', 'ProductController@create');
	$router->get('/product/{id}', 'ProductController@show');
	$router->put('/product/{id}', 'ProductController@update');
	$router->delete('/product/{id}', 'ProductController@destroy');

	//Refund api
	$router->get('/getProposal/{id}', 'RefundController@getProposal');
	$router->get('/refunds/{id}', 'RefundController@index');
	$router->post('/addRefund', 'RefundController@create');
	$router->post('/editRefund/{id}', 'RefundController@update');
	$router->get('/viewRefund/{id}', 'RefundController@show');
	$router->delete('/refund/{id}', 'RefundController@delete');


});

$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});


$router->get('updateRemaingBalance', 'ExampleController@updateRemaingBalance');
