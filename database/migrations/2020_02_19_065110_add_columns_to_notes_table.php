<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->string('org_note_file_name')->nullable()->after('note')->comment('Original File Name');
            $table->string('note_file_name')->nullable()->after('org_note_file_name');
            $table->text('note_file_path')->nullable()->after('note_file_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notes', function (Blueprint $table) {
            $table->dropColumn('org_note_file_name');
            $table->dropColumn('note_file_name');
            $table->dropColumn('note_file_path');
        });
    }
}
