<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city', 'state_code',
    ];

    public function state()
    {
        return $this->belongsTo('App\State','state_code');
    }
}