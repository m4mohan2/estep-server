<div style="float:left; width:100%; background:#EEEEEE;padding:30px 0;">
     <div style="max-width:100%; width:600px; padding:0px; margin:0 auto;">
        <div style="width:100%; background:#1F2C47; padding:20px 20px; float:left;"><img src="{{ $logo }}" width="80" height="80" style="border-radius:50%">
        </div>
        <div style="font-size:15px;float:left; width:100%; background:#fff; font-family:Tahoma, Geneva, sans-serif; text-align:left; color:#1F2C47; padding:20px;">
            <p><strong>Hi {{ $name }} ,</strong></p>
            <p>Thank you for signing up! Please find the login credentials which you can use to login.<p>
            <table align="left" border="0" cellpadding="0" cellspacing="0" style="width:100%">
              <tbody>
                <tr>
                  <td><b>Email :</b></td>
                  <td align="left">{{ $email }}</td>
                </tr>
                <tr>
                  <td><b>Password :</b></td>
                  <td align="left">{{ $password }}</td>
                </tr>
              </tbody>
            </table>
            <p>&nbsp;</p>
            <p><a href="{{ $link }}" target="_blank">Click here</a> to verify your email ID. Please note after the verification of the email ID the login request will be sent to admin for review. You'll be able to login once admin approves your login request.<p>

            <p>Thanks and regards,<br />
          	 Admin</p>
        </div>
        <div style="width:100%; background:#1F2C47; padding:30px 20px; float:left; text-align:center;color:#989898">
            <p>© Copyright 2019-20 CECFL - All Rights Reserved</p>
        </div>
     </div>
</div>