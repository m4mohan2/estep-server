<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportedExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reported_expenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('proposal_id')->comment('Proposal parent Id');
            $table->foreign('proposal_id')->references('id')->on('proposals')->onDelete('cascade');
            $table->unsignedBigInteger('bb_id')->comment('Budget Breakdown Id');
            $table->foreign('bb_id')->references('id')->on('budget_breakdowns')->onDelete('cascade');
            $table->string('quarter');
            $table->date('date');
            $table->double('expense_amount', 15, 8);
            $table->string('no_check')->nullable();
            $table->string('infavourof')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reported_expenses');
    }
}
