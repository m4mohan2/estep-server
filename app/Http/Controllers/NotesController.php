<?php 
namespace App\Http\Controllers;
use Illuminate\Support\Facades\File;

use App\Notes;

use Illuminate\Http\Request;

class NotesController extends Controller
{
    /**
     * Instantiate a new NewsController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notes = Notes::paginate(10);
        return response()->json($notes);
    }
    /**
     * Store a newly created notes.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'NotesBody' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $model = New Notes;

            $model->note        =  $request->input('NotesBody');
            $model->proposal_id =  $request->input('ProposalId');
            $model->created_by  =  $request->input('CreatedBy');
            $model->created_at  =  date('Y-m-d H:i:s');
            $model->updated_at  =  date('Y-m-d H:i:s');

            if($model->save()){
                // Logs
                $this->createProposalLogs($model->proposal_id, 'NOTE_ADD');
                
                $modelId = $model->id;
                if ($request->hasFile('note_file_name')) {
                
                    $file = $request->file('note_file_name');
                    
                    $fileCount = $this->moveFile($file,$modelId,$request->input('ProposalId'));
                }
            }
            

            \DB::commit();
            $message = 'Notes has been added successfully!';
            $alerttype = true;
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors.' '.'Please try again!';
            $alerttype = false;

            return response()->json([
                'message' => $message,
                'success' => $alerttype,
            ], 409);

        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }

    public function delete($id){
        try {
            $notes = Notes::find($id);
 
            $filename = $notes['note_file_path'];

            if (File::exists($filename)) {
                File::delete($filename);
            }
            
            $notes->delete();

            return response()->json([
                'message' => 'Notes removed successfully'
                ,'success' => true
            ],200);
        }catch(\Exception $e){
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function list($proposal_id)
    { 
        try{
            $notes = Notes::with('logUser')->where('proposal_id', '=', $proposal_id)->orderBy('id', 'DESC')->get();
            return response()->json([
                'data' => $notes,
                'resCode' => 200,
                'success'=> true,
            ], 200);
        }catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }


    protected function moveFile($file,$note_id,$proposal_id){

        $fileCount = count((array)$file);

        try {
            $note = Notes::find($note_id);

            $notMoveFileArr = array();

            $originalFileName = $file->getClientOriginalName();
            $filteredOrgName = preg_replace('/[^a-zA-Z0-9-_\.]/','', $originalFileName);
            $extension = strtolower($file->getClientOriginalExtension());
            $fileName = date('YmdHmsu').'-'.$filteredOrgName;

            $fileSize = $file->getSize();

            $fileSize = number_format($fileSize / 1048576,2);

            $destinationPath = "upload/notes/".$proposal_id."/".$note_id."/";
            $fullFilePath = $destinationPath.$fileName;
                
            if($file->move($destinationPath,$fileName)){

            if($fileSize > 4){

                $oldFullFilePath = $fullFilePath;

                $fileName = date('YmdHmsu').'-converted-'.$filteredOrgName;
                $fullFilePath = $destinationPath.$fileName;

                if($extension == 'pdf'){

                    shell_exec ('ps2pdf -dPDFSETTINGS=/default '.$oldFullFilePath.'  '.$fullFilePath.'');

                }else if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff'){

                    shell_exec ('convert -quality 50% '.$oldFullFilePath.'  '.$fullFilePath.'');

                } /*else{
                    rename( $orgFullFilePath, $fullFilePath);
                }*/

                if (File::exists($oldFullFilePath)) {
                    File::delete($oldFullFilePath);
                }
            }
            
            $note->org_note_file_name = $filteredOrgName;
            $note->note_file_name = $fileName;
            $note->note_file_path = $fullFilePath;

            $note->save();
                
            }else{
                $notMoveFileArr[] = $originalFileName;
            }
        return $notMoveFileArr;
        }catch(\Exception $e){
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

}