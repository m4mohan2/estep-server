<?php namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class AuditTrail extends Model
{ 
	public $timestamps = true;
    
}