<?php namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class EntityMembers extends Model
{ 
	public $timestamps = true;

	public function cityinfo()
    {
        return $this->hasOne('App\City','id', 'city');
    }
    
}