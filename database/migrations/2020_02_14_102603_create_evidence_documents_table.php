<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvidenceDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evidence_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('evidence_id');
            $table->unsignedBigInteger('proposal_id')->comment('Proposal parent Id');
            $table->foreign('proposal_id')->references('id')->on('proposals')->onDelete('cascade');
            $table->string('org_file_name')->comment('Original File name')->nullable();
            $table->string('file_name')->comment('File name')->nullable();
            $table->text('file_path')->comment('File path')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evidence_documents');
    }
}
