<?php namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class ProposalDocument extends Model
{ 
	public $timestamps = false;
    protected $fillable = ['user_id','proposal_id'];

    public function proposal()
    {
        return $this->belongsTo('App\Proposal');
    } 

    public function logUser()
    {
        return $this->hasOne('App\User','id','user_id');
    }
}

