<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Tightbudgetnotification extends Mailable
{
    use Queueable, SerializesModels;
    public $entity_name;
    public $project_name;
    public $proposal_id;
    public $requested_amount;
    public $approved_amount;
    public $logo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->entity_name = $data['entity_name'];
        $this->project_name = $data['project_name'];
        $this->proposal_id = $data['proposal_id'];
        $this->requested_amount = $data['requested_amount'];
        $this->approved_amount = $data['approved_amount'];
        $this->logo = env('LOGO_URL');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Tight budget notification')->view('emails.tightbudgetnotification');

        $data['entity_name'] = $this->entity_name;
        $data['project_name'] = $this->project_name;
        $data['proposal_id'] = $this->proposal_id;
        $data['requested_amount'] = $this->requested_amount;
        $data['approved_amount'] = $this->approved_amount;
        $data['logo'] = $this->logo;

        $this->withSwiftMessage(function ($data) {
            $data->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
        });
    }
}