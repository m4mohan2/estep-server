<?php
namespace App\Http\Controllers;
use App\Contacts;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;

use App\Mail\Contact;


class ContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /* $search = $request->get('search');

        if ($search!='') { 
            $Pages = Page::where('title_en','like','%'.$search.'%')
                                ->orWhere('title_es','like','%'.$search.'%')
                                ->orderBy('id', 'DESC')
                                ->paginate(10);
        }else{
            $Pages = Page::orderBy('id', 'DESC')->paginate(10);
        }

        return response()->json($Pages); */
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'ofsl_name' => 'required',
            'town' => 'required',
            'telephone' => 'required',
            'email' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $model = New Contacts;
            $model->name  =  $request->input('name');
            $model->ofsl_name  =  $request->input('ofsl_name');
            $model->town  = $request->input('town');
            $model->telephone  = $request->input('telephone');
            $model->email = $request->input('email');
            $model->comment = $request->input('comment');

            $data['name'] = $request->input('name');
            $data['ofsl_name'] = $request->input('ofsl_name');
            $data['town'] = $request->input('town');
            $data['telephone'] = $request->input('telephone');
            $data['email'] = $request->input('email');
            $data['comment'] = $request->input('comment');

            $model->save();
            $modelId = $model->id;

            $admin_email = 'edil@totalcontrolgps.com';
            $support_email = 'support@impactocomunitariopr.org';
            
            Mail::to($admin_email)->send(new Contact($data));
            Mail::to($support_email)->send(new Contact($data));

            \DB::commit();
            $message = 'Contact has been sent successfully!';
            $alerttype = true;
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors.' '.'Please try again!';
            $alerttype = false;

        }
        
        //Return message
        return response()->json([
            'message'    => $message,
            'success' => $alerttype,
        ], 201);
    }

    public function sendContact(Request $request)
    {
        //return response()->json($request->input('contactEmail'));
        $this->validate($request, [
            'name' => 'required',
            'ofsl_name' => 'required',
            'town' => 'required',
            'telephone' => 'required',
            'email' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $model = New Contacts;
            $model->name  =  $request->input('name');
            $model->ofsl_name  =  $request->input('ofsl_name');
            $model->town  = $request->input('town');
            $model->telephone  = $request->input('telephone');
            $model->email = $request->input('email');
            $model->comment = $request->input('comment');

            $data['name'] = $request->input('name');
            $data['ofsl_name'] = $request->input('ofsl_name');
            $data['town'] = $request->input('town');
            $data['telephone'] = $request->input('telephone');
            $data['email'] = $request->input('email');
            $data['comment'] = $request->input('comment');

            $model->save();
            $modelId = $model->id;
            
            $admin_email = $request->input('contactEmail');

            $support_email = 'support@impactocomunitariopr.org';
            
            Mail::to($admin_email)->send(new Contact($data));
            Mail::to($support_email)->send(new Contact($data));

            \DB::commit();
            $message = 'Contact has been sent successfully!';
            $alerttype = true;
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors.' '.'Please try again!';
            $alerttype = false;

        }
        
        //Return message
        return response()->json([
            'message'    => $message,
            'success' => $alerttype,
        ], 201);
    }


    public function show($id)
    {
        $model = Page::find($id);
        return response()->json($model);
    }

}
