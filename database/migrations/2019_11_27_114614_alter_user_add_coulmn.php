<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserAddCoulmn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('admin_state')->nullable()->after('id')->comment('Admin User State');
            $table->integer('admin_city')->nullable()->after('id')->comment('Admin User City');
            $table->text('admin_address')->nullable()->after('id')->comment('Admin User Address');
            $table->string('admin_phone')->nullable()->after('id')->comment('Admin User Phone');
            $table->string('admin_last_name')->nullable()->after('id')->comment('Admin User Last Name');
            $table->string('admin_first_name')->nullable()->after('id')->comment('Admin User First Name');
            $table->string('password_reset_token')->nullable()->after('password')->comment('Password reset token');
            $table->string('active_token')->nullable()->after('password')->comment('Account activation token');
            $table->tinyInteger('is_verified')->default('0')->after('password')->comment('0=>Not Verified,1=>Verified');
            $table->tinyInteger('activeStatus')->default('0')->after('password')->comment('0=>Inactive,1=>Active');
            $table->integer('roleId')->nullable()->after('id')->comment('Admin User Role');
            $table->string('vpass')->nullable();
            $table->bigInteger('created_by')->nullable();

            if (Schema::hasColumn('users', 'name')) {
                Schema::table('users', function (Blueprint $table) {
                    $table->dropColumn('name');
                });
            }

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('admin_state');
            $table->dropColumn('admin_city');
            $table->dropColumn('admin_address');
            $table->dropColumn('admin_phone');
            $table->dropColumn('admin_last_name');
            $table->dropColumn('admin_first_name');
            $table->dropColumn('password_reset_token');
            $table->dropColumn('active_token');
            $table->dropColumn('is_verified');
            $table->dropColumn('activeStatus');
            $table->dropColumn('roleId');
            $table->dropColumn('vpass');
            $table->dropColumn('created_by');
        });
    }
}
