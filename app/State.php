<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'state', 'state_code',
    ];

    public function cities()
    {
        return $this->hasMany('App\City','state_code', 'state_code');
    }
}