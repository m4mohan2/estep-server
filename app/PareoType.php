<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class PareoType extends Model
{ 
    public $timestamps = true;

   	public function PareoCategory()
    {
        return $this->hasMany('App\PareoCategory','type_id', 'id');
    } 
}
