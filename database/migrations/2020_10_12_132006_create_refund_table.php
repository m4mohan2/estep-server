<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRefundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refunds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->comment('User parrent Id');
            $table->unsignedBigInteger('proposal_id')->comment('Proposal parent Id');
            $table->text('expenditure_desc')->nullable();
            $table->double('expense_amount', 15, 8)->nullable();
            $table->string('check_no')->nullable();
            $table->string('infavourof')->nullable();
            $table->timestamp('expense_date')->nullable();
            $table->timestamp('refund_date')->nullable();
            $table->double('refund_amount', 15, 8)->nullable();
            $table->tinyInteger('refund_status')->default('0');
            $table->unsignedBigInteger('refund_by')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('proposal_id')->references('id')->on('proposals')->onDelete('cascade');
            $table->foreign('refund_by')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refunds');
    }
}
