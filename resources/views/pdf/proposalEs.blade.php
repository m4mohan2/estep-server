<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Formulario de propuesta</title>
</head>
<body style="padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5;" width="5%">
            <img src="https://www.impactocomunitariopr.org/assets/images/logo.png" width="85" height="85">
          </td>
          <td align="center" valign="top" style="border-bottom:solid 1px #e5e5e5;" width="95%">
            <h1>Formulario de propuesta</h1>
          </td>
        </tr>
    </table>
  </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding-top:20px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Entidad</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Fecha de aplicacion</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Subvención solicitada</td>
        </tr>
        <tr>
          <td align="left" valign="top" width="70%" style="word-wrap:break-word;"><label>{{ $proposal['proposalUser']->entity_name }}</label></td>
          <td align="left" valign="top"><label>{{ $proposal->date != null ? date('m-d-Y', strtotime($proposal->date)) : '--' }}</label></td>
          <td align="left" valign="top"><label>{{ $proposal->requested_grant>0 ? '$'.$proposal->requested_grant : '--' }}</label></td>
        </tr>
      </table>
      </td>
  </tr>

  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <!-- <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Grant sugerido</td> -->
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Subvención Aprobada</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">&nbsp;</td>
        </tr>
        <tr>
          <!-- <td align="left" valign="top" width="50%"><label>{{ $proposal->donation_grant>0 ? '$'.$proposal->donation_grant : '--' }}</label></td> -->
          <td align="left" valign="top"  width="50%">
            <label>{{ $proposal->approved_grant>0 ? '$'.$proposal->approved_grant : '--' }}</label>
          </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">CARNÉ DE IDENTIDAD</td>
        </tr>
        <tr>
          <td align="left" valign="top"><label>{{ $proposal->proposal_unique_id }}</label></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="30" align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top" style="text-decoration:underline;"><h2>Comienzo</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top"><label style="padding-bottom:10px; display:block;"><strong>Proyecto</strong></label>
      <label>{{ $project->projectNameEn }}</label></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><label style="padding-bottom:10px; display:block;"><strong>Ciudad</strong></label>
      <label>{{ $city->city }}</label></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><strong style="font-size:18px;">Comunidad o población a servir</strong>
      <p>Use las siguientes categorías para definir la comunidad a servir.</p>
      <label style="padding-bottom:10px; display:block;"><strong>Comunidad</strong></label>
      <label>{{ $proposal->community }}</label>
  </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><div>
        <h4 style="padding:0 0 10px 0; margin:0;">Población</h4>
          @if ($proposal->categories === 0)
          <label>Masculino</label>
          @elseif ($proposal->categories === 1)
          <label>Hembra</label>
          @else
          <label>Ambos</label>
          @endif
      </div>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><strong style="font-size:18px;">Grupo de edad</strong>
      <ul style="padding-left:0px;">
      @foreach($population as $plval)
        @if ($plval->population)
          <li style="list-style:none; display:inline-block;margin-bottom: 8px; margin-right: 10px;">{{ $plval->value }}</li>
        @endif
      @endforeach
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->population_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
        @endif
        @if($proposal->population_status == 2)
        <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
        @endif
        @if($proposal->population_status == 3)
        <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:30px; border-bottom:solid 1px #e5e5e5; padding-bottom:30px;">
        <tr>
          <td align="left" valign="top" width="70%" style="word-wrap:break-word;border-right:solid 1px #e5e5e5;"><h3>Declaración de necesidad</h3>
            <p>Describa la necesidad e importancia del programa o servicios comunitarios que su organización propone en este documento. Haga referencia a fuentes creíbles (publicadas) de información y datos (cualitativos y cuantitativos) para respaldar sus argumentos.</p>
          </td>
          <!-- <td align="left" valign="top" style="padding:0 0 0 15px;"><ul style="padding:0 0 0 0;">
              <li style="list-style:none; padding-bottom:5px;"><a href="#" style="text-decoration:none; color:#282828;">Instituto de Estadística de Puerto Rico</a></li>
              <li style="list-style:none; padding-bottom:5px;"><a href="#" style="text-decoration:none; color:#282828;">Observatorio de abuso de sustancias</a></li>
              <li style="list-style:none;padding-bottom:5px;"><a href="#" style="text-decoration:none; color:#282828;">Instituto de Desarrollo Juvenil</a></li>
              <li style="list-style:none;"><a href="#" style="text-decoration:none; color:#282828;">Proyecto PREHCO</a></li>
           </ul>
          </td> -->
        </tr>
        <tr>
          <td align="left" valign="top"style="word-wrap:break-word;border-right:solid 1px #e5e5e5;">
            <p>{{$proposal->necessity_statement}}</p>
          </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Clasificación</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Documento</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'necessity_statement_file')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>Declaración de necesidad</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->necessity_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
        @endif
        @if($proposal->necessity_status == 2)
        <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
        @endif
        @if($proposal->necessity_status == 3)
        <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" width="70%">
            <h2>Ingrese el enfoque programático.</h2>
            <p>¿Qué enfoque de inversión programática afectará su programa o servicio?</p>
            @if ($proposal->programmatic_approach === 'prog_opt1')
            <label>Fortalecimiento de nuestra cultura Proyectos artísticos y culturales para niños, jóvenes y adultos que aumentan las oportunidades de expresión creativa, tales como; clases, talleres y seminarios sobre teatro, danza, pintura, diseño, música, poesía, escritura y producción local de instrumentos musicales en comunidades desfavorecidas; y eventos enfocados en promover y fortalecer la cultura puertorriqueña.</label>
            @elseif ($proposal->programmatic_approach === 'prog_opt2')
            <label>Promoción de proyectos de conductas positivas y saludables que promuevan el bienestar físico, emocional y mental de personas, grupos y comunidades desfavorecidas; programas educativos dirigidos a la excelencia académica y la vida independiente; servicios de apoyo para individuos y familias sobrevivientes de situaciones de violencia y abuso; actividades organizadas destinadas a desarrollar habilidades de trabajo en equipo, estilos de vida saludables y servicio comunitario a través del deporte; Los programas comunitarios se centraron en el desarrollo de habilidades para reducir las vulnerabilidades a los desastres naturales y las prácticas para la protección de los recursos naturales y animales.</label>
            @elseif ($proposal->programmatic_approach === 'prog_opt3')
            <label>Impulsar la estabilidad financiera y los proyectos de desarrollo económico que promueven el desarrollo y la sostenibilidad económica de las personas, las familias y las comunidades a través de asesoramiento legal, capacitación sobre el manejo adecuado de las finanzas, habilidades laborales o cualquier otro servicio que aumente la empleabilidad de los jóvenes, adultos y personas de diversidad funcional; programas enfocados en la creación de empleo y el desarrollo de industrias comunitarias.</label>
            @endif
            </td>
          <td align="left" valign="top" style="padding-left:15px;">Proyectos artísticos y culturales para niños, jóvenes y adultos que aumentan las oportunidades de expresión creativa, tales como: clases, talleres, seminarios, teatro, danza, pintura, música, poesía, escritura, producción local e instrumentos musicales en comunidades desfavorecidas y eventos enfocados en promoviendo y fortaleciendo la cultura puertorriqueña.</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->programmatic_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
        @endif
        @if($proposal->programmatic_status == 2)
        <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
        @endif
        @if($proposal->programmatic_status == 3)
        <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="text-decoration:underline;"><h2>Pareo Registration</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding-top:20px;word-wrap:break-word;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Tipo</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Origen</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Categoría</td>
        </tr>
        <tr>
          <td align="left" valign="top"><label>{{ $pareo_types }}</label></td>
          <td align="left" valign="top" width="70%" style="word-wrap:break-word;"><label>{{ $proposal->pareo_origin}}</label></td>
          <td align="left" valign="top"><label>{{ $pareo_category}}</label></td>
        </tr>
      </table>
      </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Cálculo Pareo</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Subversión aprobada</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top" width="50%"><label>{{ $proposal->requested_grant>0 ? '$'. ($proposal->requested_grant * 20 )/100 : '--' }}</label></td>
          <td align="left" valign="top"  width="50%"><label>{{ $proposal->approved_grant>0 ? '$'.$proposal->approved_grant : '--' }}</label></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top" style="text-decoration:underline;"><h2>Programa</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="62%" align="left" valign="top">¿Qué objetivo busca alcanzar su servicio o programa comunitario en la comunidad y población propuestas?</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><p style="padding-bottom:20px;">Objetivo: Es el fin al que se dirigen los deseos de la organización. En proyectos, programas e intervenciones, los objetivos son los objetivos que se pretende lograr a largo plazo a través de los esfuerzos realizados. Estas metas deben estar vinculadas a la visión y misión, ya que aquellas que, junto con los objetivos, guiarán los esfuerzos hacia los logros esperados.</p>
      <label>{{ $proposal->goal_to_achieve }}</label></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->goal_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
        @endif
        @if($proposal->goal_status == 2)
        <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
        @endif
        @if($proposal->goal_status == 3)
        <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding-top:15px;"><p style="font-size:18px;">Describa los cambios y / o beneficios que se obtendrán con la implementación de su programa o servicio comunitario. Demuestre estos cambios utilizando las medidas seleccionadas por la NPO conocidas como objetivos.</p>
      <p>Los objetivos deben ser escritos utilizando el modelo SMART. Los resultados serán evaluados para determinar si se cumplieron los objetivos. Aunque diferentes fuentes definen objetivos SMART de diferentes maneras, la siguiente definición se usa comúnmente:</p>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="5" cellpadding="5">
        <tr>
          <td align="left" valign="top"><ul style="padding:0;">
              <li style="list-style:none; padding-bottom:10px;"><span style="padding-left:10px;">Específico - apunte a un área específica para el cambio</span> </li>
              <li style="list-style:none;padding-bottom:10px;"> <span style="padding-left:10px;">S Medible - cuantifique un indicador de progreso</span> </li>
              <li style="list-style:none;padding-bottom:10px;"> <span style="padding-left:10px;">S Asignable - especifique quién lo hará</span> </li>
            </ul></td>
          <td align="left" valign="top"><ul style="padding:0;">
              <li style="list-style:none; padding-bottom:10px;"> <span style="padding-left:10px;">Realista - establezca resultados realistas para lograr</span> </li>
              <li style="list-style:none;padding-bottom:10px;"><span style="padding-left:10px;">Relacionado con el tiempo - especifique cuándo se lograrán los resultados</span> </li>
            </ul></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><label>{{ $proposal->changes_or_benefits }}</label></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->changes_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
        @endif
        @if($proposal->changes_status == 2)
        <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
        @endif
        @if($proposal->changes_status == 3)
        <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="5" cellpadding="5" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;">
        <tr>
          <td align="left" valign="top" width="45%" style="word-wrap:break-word;"><h4>Explique qué actividades específicas realizará su servicio o programa comunitario.</h4>
            <p>Las actividades incluyen talleres, servicios, conferencias, capacitación, encuestas comunitarias, asesoramiento en el hogar, etc. llevado a cabo para proporcionar beneficios a la comunidad y la población atendida.</p>
            <label>{{ $proposal->specific_activities }}</label>
            <ul style="padding:20px 0 0 0;">
                  @if($proposal->specific_status == 1)
                  <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
                  @endif
                  @if($proposal->specific_status == 2)
                  <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
                  @endif
                  @if($proposal->specific_status == 3)
                  <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
                  @endif
            </ul></td>
          <td align="left" valign="top" width="45%" style="word-wrap:break-word;"><h4>Explique quién se verá afectado por su programa o servicio comunitario.</h4>
            <p style="padding-bottom: 16px;">Incluya participantes, personas, clientes, agencias a quienes su servicio comunitario o programa se beneficiarán</p>
            <label>{{ $proposal->who_will_affected }}</label>
            <ul style="padding:20px 0 0 0;">
                  @if($proposal->affected_status == 1)
                  <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
                  @endif
                  @if($proposal->affected_status == 2)
                  <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
                  @endif
                  @if($proposal->affected_status == 3)
                  <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
                  @endif
            </ul></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="5" cellpadding="5" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;">
        <tr>
          <td align="left" valign="top" width="45%" style="word-wrap:break-word;"><h4>Explique cómo pagará su programa o servicio comunitario propuesto el desarrollo del enfoque programático propuesto por su organización.</h4>
            <label>{{ $proposal->how_will_pay }}</label>
            <ul style="padding:20px 0 0 0;">
                  @if($proposal->pay_status == 1)
                  <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
                  @endif
                  @if($proposal->pay_status == 2)
                  <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
                  @endif
                  @if($proposal->pay_status == 3)
                  <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
                  @endif
            </ul></td>
          <td align="left" valign="top" width="45%" style="word-wrap:break-word;"><h4>Explique qué tipo de recursos necesitará su programa o servicio comunitario.</h4>
            <p style="padding-bottom: 16px;">Los recursos son los medios invertidos para permitirnos llevar a cabo actividades y obtener los resultados que queremos lograr. Por ejemplo, los recursos pueden incluir: personal (empleados, voluntarios), materiales, propiedad, transporte, investigación y tecnología, por nombrar algunos.</p>
            <label>{{ $proposal->resources_required }}</label>
            <ul style="padding:20px 0 0 0;">
                  @if($proposal->resources_status == 1)
                  <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
                  @endif
                  @if($proposal->resources_status == 2)
                  <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
                  @endif
                  @if($proposal->resources_status == 3)
                  <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
                  @endif
            </ul></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Clasificación</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Documento</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'work_plan_file')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>Plan de trabajo</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->work_plan_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
        @endif
        @if($proposal->work_plan_status == 2)
        <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
        @endif
        @if($proposal->work_plan_status == 3)
        <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Evaluaciones</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" width="50%" style="padding-right:15px;"><h4 style="margin-top:0px;">Describa la experiencia de su organización realizando evaluaciones de programas.</h4>
            <h4 style="padding-bottom:12px;">¿Su organización evalúa los programas mediante la recopilación y medición de datos relevantes?</h4>
            <label>{{ $proposal->collecting_relevant_data }}</label>
            <ul style="padding:20px 0 20px 0;">
              @if($proposal->collecting_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
              @endif
              @if($proposal->collecting_status == 2)
              <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
              @endif
              @if($proposal->collecting_status == 3)
              <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
              @endif
            </ul>
            <h4 style="padding-bottom:12px;">¿Quién en su organización evalúa los programas, políticas y prácticas?</h4>
            <label>{{ $proposal->who_evaluates }}</label>
            <ul style="padding:20px 0 20px 0;">
              @if($proposal->who_evaluates_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
              @endif
              @if($proposal->who_evaluates_status == 2)
              <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
              @endif
              @if($proposal->who_evaluates_status == 3)
              <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
              @endif
            </ul></td>
          <td align="left" valign="top" width="50%">
          <!-- <a href="#" style="font-size: 14px;line-height: 21px;color: #000;padding-left: 0;text-transform: uppercase;font-weight: 500;">Click here to download copy of entire Proposal Form.</a> -->
            <h4>¿Con qué frecuencia evalúa su organización sus programas y servicios?</h4>
            <label>{{ $proposal->how_often_organization_evaluates }}</label>
            <ul style="padding:20px 0 20px 0;">
              @if($proposal->organization_evaluates_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
              @endif
              @if($proposal->organization_evaluates_status == 2)
              <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
              @endif
              @if($proposal->organization_evaluates_status == 3)
              <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
              @endif
            </ul></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top"><strong style="font-size:18px;">¿Cuál de las siguientes afirmaciones describe mejor el tipo de evaluaciones que se llevan a cabo en su organización? Marque todo lo que corresponda.</strong>
      <ul style="padding-left:0px;">
      @if ($evaluations)
        @foreach($evaluations as $evval)
          @if ($evval->typeevaluation)
            <li>{{ $evval->value }}</li>
          @endif
        @endforeach
      @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->statements_describes_type_of_evaluations_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
        @endif
        @if($proposal->statements_describes_type_of_evaluations_status == 2)
        <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
        @endif
        @if($proposal->statements_describes_type_of_evaluations_status == 3)
        <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="1px solid" cellspacing="5" cellpadding="5">
        <tr>
          <td align="left" valign="top"><h4 style="margin-top:0px;">Tipo de evaluación a utilizar.</h4>
            <label>{{ $proposal->type_of_evaluation }}</label>
            <ul style="padding:20px 0 20px 0;">
              @if($proposal->type_of_evaluation_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
              @endif
              @if($proposal->type_of_evaluation_status == 2)
              <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
              @endif
              @if($proposal->type_of_evaluation_status == 3)
              <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
              @endif
            </ul></td>
          <td align="left" valign="top"><h4 style="margin-top:0px;">Tipo de datos a recopilar.</h4>
            <label>{{ $proposal->type_of_data }}</label>
            <ul style="padding:20px 0 20px 0;">
            @if($proposal->type_of_data_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
              @endif
              @if($proposal->type_of_data_status == 2)
              <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
              @endif
              @if($proposal->type_of_data_status == 3)
              <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
              @endif
            </ul></td>
          <td align="left" valign="top"><h4 style="margin-top:0px;">Tipo de evaluador a utilizar.</h4>
            <label>{{ $proposal->type_of_evaluator }}</label>
            <ul style="padding:20px 0 20px 0;">
            @if($proposal->type_of_evaluator_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
              @endif
              @if($proposal->type_of_evaluator_status == 2)
              <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
              @endif
              @if($proposal->type_of_evaluator_status == 3)
              <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
              @endif
            </ul></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Propiedad</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: solid 1px #e5e5e5; padding-bottom:20px;">
        <tr>
          <td align="left" valign="top" style="padding-right:15px;"><h4>Inventario de propiedades, materiales y equipos necesarios para ofrecer los servicios.</h4>
          </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">

        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'property_inventory_file')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;">
              <span style="color: #013b85; border-bottom: 1px solid #ddd;">Clasificación</span><br>
              {{ $necessity_statement_file->desc}}</td>

            @if ($necessity_statement_file->document_type == 'property_inventory_file' && $necessity_statement_file->monthly_payment !='')
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;">
                <span style="color: #013b85; border-bottom: 1px solid #ddd;">Pago Mensual</span><br>{{'$'.$necessity_statement_file->monthly_payment}}
              </td>
            @endif

            @if ($necessity_statement_file->document_type == 'property_inventory_file' && $necessity_statement_file->monthly_payment !='' && ($necessity_statement_file->rented  == 1 || $necessity_statement_file->rented  == 0) )
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;">
                <span style="color: #013b85; border-bottom: 1px solid #ddd;">Alquilada</span><br>{{$necessity_statement_file->rented == 1 ? "Si"  : "No"}}
              </td>
            @endif

             @if ($necessity_statement_file->document_type == 'property_inventory_file'  && $necessity_statement_file->org_file_name !='')
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;">
              <span style="color: #013b85; border-bottom: 1px solid #ddd;">Documento</span><br>
              {{ $necessity_statement_file->org_file_name }}
            </td>
            @endif
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->inventory_file_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
        @endif
        @if($proposal->inventory_file_status == 2)
        <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
        @endif
        @if($proposal->inventory_file_status == 3)
        <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Presupuesto</h2></td>
  </tr>
  <tr>
    <td style="color:#007bff; font-size:22px; padding-top:20px;"> Administrativo </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Clasificación</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Documento</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'adm_expence_file')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->desc}}</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td style="line-height:21px; padding-top:10px; padding-bottom:20px;">La descripción del presupuesto explica claramente cada partida de gastos solicitada. Debe desglosar el presupuesto presentado en detalle por empleado / salario, unidades / materiales, equipo que se comprará, actividades, etc., incluido el costo a cubrir y el tiempo en que Será desarrollado. Debe desglosar el presupuesto por artículo. Los gastos administrativos / operativos no pueden exceder el 40% del presupuesto total. El salario para el puesto de Director, Administrador o persona a cargo de las operaciones diarias de la OSFL no excederá del cincuenta al 50% de los gastos adm.</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%">
        <thead>
          <tr>
            <th width="50%" align="left" style="color: #013b85;">DESCRIPCIÓN DEL ARTÍCULO DE GASTOS</th>
            <th width="25%" align="left" style="color: #013b85;">GASTOS ESTIMADOS</th>
            <th width="25%" align="left" style="color: #013b85;">PRESUPUESTO AJUSTADO</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['budgetBreakdown'] as $bbVal)
          @if($bbVal->budget_type == 'Administrative')
          <tr>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->description }}</label></td>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd;padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->budget_amount>0 ? '$'.$bbVal->budget_amount : '--' }}</label></td>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->tight_budget>0 ? '$'.$bbVal->tight_budget : '--' }}</label></td>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->administrative_budget_file_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
        @endif
        @if($proposal->administrative_budget_file_status == 2)
        <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
        @endif
        @if($proposal->administrative_budget_file_status == 3)
        <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="color:#007bff; font-size:22px; padding-top:20px;">Directo</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Clasificación</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Documento</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'direct_expence_file')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->desc}}</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td style="line-height:21px; padding-top:10px; padding-bottom:20px;">La descripción del presupuesto explica claramente cada partida de gastos solicitada. Debe desglosar el presupuesto presentado en detalle por empleado / salario, unidades / materiales, equipo que se comprará, actividades, etc., incluido el costo a cubrir y el tiempo en que Será desarrollado. Debe desglosar el presupuesto por artículo. Recuerde que los gastos directos propuestos deben agregar un mínimo del 60% del presupuesto total.</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%">
        <thead>
          <tr>
            <th width="50%" align="left" style="color: #013b85;">DESCRIPCIÓN DEL ARTÍCULO DE GASTOS</th>
            <th width="25%" align="left" style="color: #013b85;">GASTOS ESTIMADOS</th>
            <th width="25%" align="left" style="color: #013b85;">PRESUPUESTO AJUSTADO</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['budgetBreakdown'] as $bbVal)
          @if($bbVal->budget_type == 'Direct')
          <tr>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->description }}</label></td>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd;padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->budget_amount>0 ? '$'.$bbVal->budget_amount : '--' }}</label></td>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->tight_budget>0 ? '$'.$bbVal->tight_budget : '--' }}</label></td>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->direct_budget_file_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
        @endif
        @if($proposal->direct_budget_file_status == 2)
        <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
        @endif
        @if($proposal->direct_budget_file_status == 3)
        <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
   <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Legal</h2></td>
  </tr>
 <tr>
    <td align="left" valign="top">
      <ul style="padding-left:0px;">
      @foreach ($legalArr as $key => $v)
          <li>{{ $v }}</li>
      @endforeach
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->legal_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->legal_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->legal_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Requisitos</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: solid 1px #e5e5e5; padding-bottom:20px;">
        <tr>
          <td align="left" valign="top" style="padding-right:15px;"><h4>Requisitos para la solicitud de propuesta. Se pueden solicitar algunos documentos en www.pr.gov</h4></td>
        </tr>
      </table>
      </td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Clasificación</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Documento</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'puerto_rico_state_department')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>DEPARTAMENTO DE ESTADO DE PUERTO RICO</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'internal_revenue_services')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>INTERNAL REVENUE SERVICES (IRS)</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'puerto_rico_state_insurance_fund_corporation')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>CORPORACIÓN DEL FONDO DEL SEGURO DEL ESTADO DE PUERTO RICO</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'municipal_income_collection_center')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>CENTRO DE RECAUDACIONES DE INGRESOS MUNICIPALES</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'department_of_finance')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>DEPARTAMENTO DE HACIENDA</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'department_of_labor_and_human_resources')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>DEPARTAMENTO DEL TRABAJO Y RECURSOS HUMANOS</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'health_department')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>DEPARTAMENTO DE SALUD</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'administration_of_regulations_and_permits')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>ADMINISTRACIÓN DE REGLAMENTOS Y PERMISOS</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'puerto_rico_fire_fighter_body')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>CUERPO DE BOMBEROS DE PUERTO RICO</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'custody_agency')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>AGENCIA CUSTODIO</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'additional_documents')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>DOCUMENTOS ADICIONALES</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'seguro_social')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>SEGURO SOCIAL</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'administration_for_child_support')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>ADMINISTRACION PARA EL SUSTENTO DE MENORES (ASUME)</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'puerto_rico_police')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>POLICIA DE PUERTO RICO</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'negotiated_conventions_of_san_jun')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>NEGOCIADO DE CONVENCIONES DE SAN JUN, PR</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'higher_education_council')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>CONSEJO SUPERIOR DE EDUCACION</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'osfl')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>OSFL</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
      <ul style="padding:20px 0 20px 0;">
          @if($proposal->requirement_file_status == 1)
          <li style="list-style:none; display:inline-block;"><b>Compila</b></li>
          @endif
          @if($proposal->requirement_file_status == 2)
          <li style="list-style:none; display:inline-block;"><b>No cumple</b></li>
          @endif
          @if($proposal->requirement_file_status == 3)
          <li style="list-style:none; display:inline-block;"><b>No aplica</b></li>
          @endif
      </ul>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Evidencia</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: solid 1px #e5e5e5; padding-bottom:20px;">
        <tr>
          <td align="left" valign="top" style="padding-right:15px;"><h4>Records of Other Documents for evidence. Required documents must be incorporated.</h4></td>
        </tr>
      </table>
      </td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Clasificación</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Documento</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($evidenceArr as $k => $v)
            <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{$k}}</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{$v}}</label>
          </tr>
        @endforeach
        </tbody>
      </table>
      <ul style="padding:20px 0 20px 0;">
          @if($proposal->evidence_doc_status == 1)
          <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
          @endif
          @if($proposal->evidence_doc_status == 2)
          <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
          @endif
          @if($proposal->evidence_doc_status == 3)
          <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
          @endif
      </ul>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Enviar</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding-bottom:20px;"><h3>Enviar</h3>
      <input type="checkbox" />
      Certifico que la información que proporcioné para esta propuesta es correcta y consiste en mi propio conocimiento personal para todos los propósitos legales y relevantes bajo pena o perjurio.</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="padding-right:15px;" width="60%"><label style="padding-bottom:10px; display:block;"><strong>Name</strong></label>
            <label>{{ $proposal->name != null ? $proposal->name : ''  }}</label></td>
          <td align="left" valign="top" style="padding-left:15px;"><label style="padding-bottom:10px; display:block;"><strong>Date</strong></label>
            <label>{{ $proposal->date != null ? date('m-d-Y', strtotime($proposal->date)) : '' }}</label></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
