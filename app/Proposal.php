<?php 

namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Proposal extends Model
{ 
    public $timestamps = true;

   	public function proposalDocuments()
    {
        return $this->hasMany('App\ProposalDocument');
    } 

    public function proposalUser()
    {
        return $this->hasOne('App\User','id','user_id')->where('user_type','=',1);
    } 

    public function assignUser()
    {
        return $this->hasOne('App\AssignUserToProposal','proposal_id','id')->orderBy('id', 'DESC');
    }

    public function cityinfo()
    {
        return $this->hasOne('App\City','id', 'city');
    }

    public function budgetBreakdown()
    {
        return $this->hasMany('App\BudgetBreakdown');
    }
    
    
    public function reportedExpenses()
    {
        return $this->hasMany('App\ReportedExpenses');
    } 

    public function proposalMember()
    {
        return $this->hasOne('App\EntityMembers','id','name');
    }

    public function logs()
    {
        return $this->hasMany('App\ProposalLog')->orderBy('id', 'DESC');
    } 

    public function project()
    {
        return $this->hasOne('App\Projects','id', 'project_id');
    }
}
