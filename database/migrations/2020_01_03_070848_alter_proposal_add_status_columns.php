<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterProposalAddStatusColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposals', function (Blueprint $table) {

            $table->enum('population_status',[0,1,2,3])->default(0)->after('population')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('necessity_status',[0,1,2,3])->default(0)->after('necessity_statement')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('programmatic_status',[0,1,2,3])->default(0)->after('programmatic_approach')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('goal_status',[0,1,2,3])->default(0)->after('goal_to_achieve')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('changes_status',[0,1,2,3])->default(0)->after('changes_or_benefits')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('specific_status',[0,1,2,3])->default(0)->after('specific_activities')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('affected_status',[0,1,2,3])->default(0)->after('who_will_affected')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('pay_status',[0,1,2,3])->default(0)->after('how_will_pay')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('resources_status',[0,1,2,3])->default(0)->after('resources_required')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('work_plan_status',[0,1,2,3])->default(0)->after('resources_required')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('collecting_status',[0,1,2,3])->default(0)->after('collecting_relevant_data')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('who_evaluates_status',[0,1,2,3])->default(0)->after('who_evaluates')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('organization_evaluates_status',[0,1,2,3])->default(0)->after('how_often_organization_evaluates')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('statements_describes_type_of_evaluations_status',[0,1,2,3])->default(0)->after('statements_describes_type_of_evaluations')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('type_of_evaluation_status',[0,1,2,3])->default(0)->after('type_of_evaluation')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('type_of_data_status',[0,1,2,3])->default(0)->after('type_of_data')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('type_of_evaluator_status',[0,1,2,3])->default(0)->after('type_of_evaluator')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('budget_file_status',[0,1,2,3])->default(0)->after('inventory_of_property')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('inventory_file_status',[0,1,2,3])->default(0)->after('inventory_of_property')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->enum('requirement_file_status',[0,1,2,3])->default(0)->after('inventory_of_property')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            
            



            /*$table->tinyInteger('population_status')->default(0)->after('population')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('necessity_status')->default(0)->after('necessity_statement')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('programmatic_status')->default(0)->after('programmatic_approach')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('goal_status')->default(0)->after('goal_to_achieve')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('changes_status')->default(0)->after('changes_or_benefits')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('specific_status')->default(0)->after('specific_activities')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('affected_status')->default(0)->after('who_will_affected')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('pay_status')->default(0)->after('how_will_pay')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('resources_status')->default(0)->after('resources_required')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('work_plan_status')->default(0)->after('resources_required')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('collecting_status')->default(0)->after('collecting_relevant_data')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('organization_evaluates_status')->default(0)->after('how_often_organization_evaluates')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('who_evaluates_status')->default(0)->after('who_evaluates')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('type_of_evaluation_status')->default(0)->after('type_of_evaluation')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('type_of_data_status')->default(0)->after('type_of_data')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('type_of_evaluator_status')->default(0)->after('type_of_evaluator')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');

            $table->tinyInteger('budget_file_status')->default(0)->after('inventory_of_property')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('inventory_file_status')->default(0)->after('inventory_of_property')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->tinyInteger('requirement_file_status')->default(0)->after('inventory_of_property')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');*/


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->dropColumn('population_status');
            $table->dropColumn('necessity_status');
            $table->dropColumn('programmatic_status');
            $table->dropColumn('goal_status');
            $table->dropColumn('changes_status');
            $table->dropColumn('specific_status');
            $table->dropColumn('affected_status');
            $table->dropColumn('pay_status');
            $table->dropColumn('resources_status');
            $table->dropColumn('work_plan_status');

            $table->dropColumn('collecting_status');
            $table->dropColumn('who_evaluates_status');
            $table->dropColumn('organization_evaluates_status');
            $table->dropColumn('statements_describes_type_of_evaluations_status');

            $table->dropColumn('type_of_evaluation_status');
            $table->dropColumn('type_of_data_status');
            $table->dropColumn('type_of_evaluator_status');

            $table->dropColumn('requirement_file_status');
            $table->dropColumn('inventory_file_status');
            $table->dropColumn('budget_file_status');
        });

    }
}
