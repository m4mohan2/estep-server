<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Refundnotification extends Mailable
{
    use Queueable, SerializesModels;
    public $entity_name;
    public $analyst_name;
    public $proposal_id;
    public $requested_amount;
    
    public $logo;
    public $copyright;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->entity_name = $data['entity_name'];
        $this->analyst_name = $data['analyst_name'];
        $this->proposal_id = $data['proposal_id'];
        $this->requested_amount = $data['requested_amount'];
        $this->logo = env('LOGO_URL');

        $copyYear = 2019; // Set your website start date
        $curYear = date('Y'); // Keeps the second year updated

        $copyright =  $copyYear . (($copyYear != $curYear) ? '-' . $curYear : '');

        $this->copyright = $copyright;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Refund notification')->view('emails.refund');

        $data['entity_name'] = $this->entity_name;
        $data['analyst_name'] = $this->analyst_name;
        $data['proposal_id'] = $this->proposal_id;
        $data['requested_amount'] = $this->requested_amount;
        $data['logo'] = $this->logo;

        $this->withSwiftMessage(function ($data) {
            $data->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
        });
    }
}