<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Projects;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');
        $all = $request->get('all');

        if ($search!='') { 
            $project = Projects::where('projectNameEn','like','%'.$search.'%')
                                ->orWhere('projectNameEs','like','%'.$search.'%')
                                ->orderBy('id', 'DESC')
                                ->paginate(10);
        }else if($all){

            $project = Projects::orderBy('id', 'DESC')->get();
        }
        else{
            $project = Projects::orderBy('id', 'DESC')->paginate(10);
        }

        return response()->json($project);
        /* return response()->json([
            'data' => $project ,
            'totalCount' => $project->count(), 
            'resCode' => 200,
            'success'=> true,
        ], 200);   */
    }

    public function getProjetList()
    {
        $project = Projects::where('status', 1)
                            ->where('startDate','<=', date('Y-m-d'))
                            ->where('endDate','>=', date('Y-m-d'))
                            ->orderBy('id', 'DESC')->get();
                            
        //return response()->json($project);
        return response()->json([
            'data' => $project ,
            'totalCount' => $project->count(), 
            'resCode' => 200,
            'success'=> true,
        ], 200);  
    }

     /**
     * Store a newly created project.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //return response()->json('Hello');
        $this->validate($request, [
            'projectNameEn' => 'required',
            'projectNameEs' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'period' => 'required',
            'status' => 'required'
        ]);

        $userId = Auth::user()->id;

        \DB::beginTransaction();
        try {
            $model = New Projects;

            $model->projectNameEn =  $request->input('projectNameEn');
            $model->projectNameEs =  $request->input('projectNameEs');
            $model->startDate   =  $request->input('startDate');
            $model->endDate     =  $request->input('endDate');
            $model->period      =  $request->input('period');
            $model->status      =  $request->input('status');
            $model->ipAddress   =  $this->get_client_ip();
            $model->createdBy   =  $userId;
            $model->created_at  =  date('Y-m-d H:i:s');
            $model->updated_at  =  date('Y-m-d H:i:s');

            $model->save();
            $modelId = $model->id;

            $this->insertAuditTrail('AUDIT_TRAIL.PROJECT','AUDIT_TRAIL.PROJECT_ADD','Project Name:  '.$request->input('projectNameEn'));

            \DB::commit();
            $message = 'Project has been added successfully!';
            $alerttype = true;
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors.' '.'Please try again!';
            $alerttype = false;

        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }

    /**
     * Update existing project.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $this->validate($request, [
            'projectNameEn' => 'required',
            'projectNameEs' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'period' => 'required',
            'status' => 'required'
        ]);

        $userId = Auth::user()->id;

        \DB::beginTransaction();
        try {

            $model = Projects::find($id);

            if(!empty($model)){
                $model->projectNameEn =  $request->input('projectNameEn');
                $model->projectNameEs =  $request->input('projectNameEs');
                $model->startDate   =  $request->input('startDate');
                $model->endDate     =  $request->input('endDate');
                $model->period      =  $request->input('period');
                $model->status      =  $request->input('status');
                $model->ipAddress   =  $this->get_client_ip();
                $model->updatedBy   =  $userId;
                $model->updated_at  =  date('Y-m-d H:i:s');
                
                $model->save();

                $this->insertAuditTrail('AUDIT_TRAIL.PROJECT','AUDIT_TRAIL.PROJECT_EDIT','Project Name:  '.$request->input('projectNameEn'));

                \DB::commit();
                $message = 'Projects has been updated successfully!';
                $alerttype = true;
            }else{
                $message = 'Projects not exists!';
                $alerttype = true;
            }
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors . ' '.'Please try again!';
            $alerttype = false;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }


    public function show($id)
    {
        $project = Projects::find($id);
        return response()->json($project);
    } 

    
    protected function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}