<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePushNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('push_notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('module_name');
            $table->bigInteger('module_id');
            $table->string('sub_module_name')->nullable();
            $table->bigInteger('sub_module_id')->nullable();
            $table->bigInteger('user_id');
            $table->tinyInteger('read_status')->comment('1=>Read,0=>Unread');;
            $table->datetime('read_datetime')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('push_notifications');
    }
}
