<?php namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Notes extends Model
{ 
	public $timestamps = false;
    
    public function logUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }
}