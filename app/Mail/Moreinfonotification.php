<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Moreinfonotification extends Mailable
{
    use Queueable, SerializesModels;
    public $entity_name;
    public $project_name;
    public $proposal_id;
    public $info_note;
    public $logo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->entity_name  = $data['entity_name'];
        $this->project_name = $data['project_name'];
        $this->proposal_id  = $data['proposal_id'];
        $this->info_note    = $data['info_note'];
        $this->logo         = env('LOGO_URL');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Analyst request notification')->view('emails.moreinfonotification');

        $data['entity_name']  = $this->entity_name;
        $data['project_name'] = $this->project_name;
        $data['proposal_id']  = $this->proposal_id;
        $data['info_note']    = $this->info_note;
        $data['logo']         = $this->logo;

        $this->withSwiftMessage(function ($data) {
            $data->getHeaders()
                ->addTextHeader('Custom-Header', 'HeaderValue');
        });
    }
}
