<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLegalColumnProposal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->enum('legal_status',[0,1,2,3])->default(0)->after('inventory_file_status')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
            $table->text('legal')->nullable()->after('inventory_file_status')->comment('Entity complies with the legal requirements');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->dropColumn('legal');
            $table->dropColumn('legal_status');
            
        });
    }
}
