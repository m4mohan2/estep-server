<?php
namespace App\Http\Controllers;

use App\BudgetBreakdown;
use App\EvidenceDocuments;
use App\QuaterlyReport;
use App\ReportedExpenses;
use App\Proposal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class QuaterlyreportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        /* $user_id = Auth::user()->id;
    $user_type = Auth::user()->user_type;

    if($user_type == 1){
    $proposals = $Allproposals->where('user_id','=',$user_id)->paginate(10);
    }else{
    $proposals = $Allproposals->where('application_status','!=','0')->paginate(10);
    } */

    }

    /**
     * Store a quartly report.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'proposal_id'           => 'required',
            //'administrative_budget' => 'required',
            'direct_budget'         => 'required',
        ]);

        \DB::beginTransaction();
        try {
            $count = DB::table('quaterly_reports')->where('proposal_id', '=', $request->input('proposal_id'))->count();

            //return response()->json(['count' => $count ], 400);

            if ($count > 0) {

                $model = QuaterlyReport::where('proposal_id', '=', $request->input('proposal_id'))->first();

                $model->proposal_id           = $request->input('proposal_id');
                $model->administrative_budget = $request->input('administrative_budget');
                $model->direct_budget         = $request->input('direct_budget');
                $model->ab_c1                 = $request->input('ab_c1');
                $model->ab_c2                 = $request->input('ab_c2');
                $model->ab_c3                 = $request->input('ab_c3');
                //$model->ab_c4      =  $request->input('ab_c4');
                $model->db_c1 = $request->input('db_c1');
                $model->db_c2 = $request->input('db_c2');
                $model->db_c3 = $request->input('db_c3');
                //$model->db_c4      =  $request->input('db_c4');
                $model->updated_at = date('Y-m-d H:i:s');

                if ($request->input('quater') == 1 && $request->input('reportstatus') != '') {
                    $model->reportstatus1 = $request->input('reportstatus');

                } else if ($request->input('quater') == 2 && $request->input('reportstatus') != '') {
                    $model->reportstatus2 = $request->input('reportstatus');

                } else if ($request->input('quater') == 3 && $request->input('reportstatus') != '') {
                    $model->reportstatus3 = $request->input('reportstatus');
                }

                $model->save();

                $this->insertAuditTrail('AUDIT_TRAIL.QUARTERLY_REPORT', 'AUDIT_TRAIL.QUARTERLY_REPORT_EDIT');

            } else {

                $model = new QuaterlyReport;

                $model->proposal_id           = $request->input('proposal_id');
                $model->administrative_budget = $request->input('administrative_budget');
                $model->direct_budget         = $request->input('direct_budget');
                $model->ab_c1                 = $request->input('ab_c1');
                $model->ab_c2                 = $request->input('ab_c2');
                $model->ab_c3                 = $request->input('ab_c3');
                //$model->ab_c4      =  $request->input('ab_c4');
                $model->db_c1 = $request->input('db_c1');
                $model->db_c2 = $request->input('db_c2');
                $model->db_c3 = $request->input('db_c3');
                //$model->db_c4      =  $request->input('db_c4');
                $model->created_at = date('Y-m-d H:i:s');
                $model->updated_at = date('Y-m-d H:i:s');

                $model->save();
                $modelId = $model->id;

                $this->insertAuditTrail('AUDIT_TRAIL.QUARTERLY_REPORT', 'AUDIT_TRAIL.QUARTERLY_REPORT_ADD');

            }

            /*if($request->input('reportstatus') == 1){
                $proposal = Proposal::find($request->input('proposal_id'));
                
                $reportExpensesDtl = ReportedExpenses::select('*', DB::raw('SUM(expense_amount) As expenseAmount'))
                ->where('proposal_id', $request->input('proposal_id'))
                ->get();
                
                $refund_amt= $proposal->approved_grant - $reportExpensesDtl[0]->expenseAmount;
                $proposal->refund_amt = $refund_amt;
                $proposal->save();
            }*/

            \DB::commit();
            $message   = 'Updated successfully!';
            $alerttype = true;
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message   = $allErrors . ' ' . 'Please try again!';
            $alerttype = false;

        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }

    /**
     * Get report details.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getUserProposalDetails(Request $request, $proposal_id)
    {
        try {
            $proposals = DB::table('proposals')
                ->leftJoin('projects', 'projects.id', '=', 'proposals.project_id')
                ->leftJoin('users', 'users.id', '=', 'proposals.user_id')
                ->leftJoin('quaterly_reports AS qr', 'qr.proposal_id', '=', 'proposals.id')
                ->where('proposals.id', '=', $proposal_id)
                ->select('proposals.id', 'proposals.proposal_unique_id', 'proposals.application_status', 'proposals.created_at', 'proposals.updated_at', 'projects.projectNameEn', 'projects.projectNameEs', 'projects.startDate', 'projects.endDate', 'projects.period', 'users.entity_name', 'users.entity_code', 'qr.administrative_budget', 'qr.direct_budget', 'qr.ab_c1', 'qr.ab_c2', 'qr.ab_c3', 'qr.db_c1', 'qr.db_c2', 'qr.db_c3', 'qr.reportstatus1', 'qr.reportstatus2', 'qr.reportstatus3')
                ->get();

            $proposals[0]->gross   = $proposals[0]->administrative_budget + $proposals[0]->direct_budget;
            $proposals[0]->q1total = $proposals[0]->ab_c1 + $proposals[0]->db_c1;
            $proposals[0]->q2total = $proposals[0]->ab_c2 + $proposals[0]->db_c2;
            $proposals[0]->q3total = $proposals[0]->ab_c3 + $proposals[0]->db_c3;
            //$proposals[0]->q4total = $proposals[0]->ab_c4 + $proposals[0]->db_c4;
            $proposals[0]->alltotal = $proposals[0]->q1total + $proposals[0]->q2total + $proposals[0]->q3total;
            
            $proposals[0]->net      = $proposals[0]->gross - $proposals[0]->alltotal;
            if( $proposals[0]->net < 0){
                $proposals[0]->net      = $proposals[0]->gross;

            }else{
                $proposals[0]->net      = $proposals[0]->gross - $proposals[0]->alltotal;
            }
          
            $proposals[0]->ab_total = $proposals[0]->ab_c1 + $proposals[0]->ab_c2 + $proposals[0]->ab_c3;
            $proposals[0]->db_total = $proposals[0]->db_c1 + $proposals[0]->db_c2 + $proposals[0]->db_c3;

            return response()->json([
                'data'    => $proposals,
                'success' => true,
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], 409);
        }
    }

    /**
     * Get Expenditure Break Down details.
     *
     * @return \Illuminate\Http\Response
     */
    public function getExpenditure($proposal_id, $budget_type)
    {
        try {

                $proposal = Proposal::find($proposal_id);

                $builder = DB::table('budget_breakdowns')
                //->select(DB::raw('id,description'))
                ->where('proposal_id', '=', $proposal_id)
                ->where('budget_type', '=', $budget_type)
                ->get();

                $expenditure=[];

                foreach($builder as $value){

                   if($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==0){

                    
                        if($value->remain_amount>0){
                        
                            $value->remain_amount = $value->remain_amount;

                        }else{

                            $value->remain_amount=$value->tight_budget;
                        }

                       
                        if($value->tight_budget>0){
                            $expenditure[] = array('id'=>$value->id,'description'=>$value->description, 'remain_amount'=>$value->remain_amount);
                        }
                       
                   }elseif($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==1){


                        if($value->remain_amount>0){
                            
                            $value->remain_amount = $value->remain_amount;

                        }else{

                            $value->remain_amount=$value->reprog_budget1;
                        }

                   
                        if($value->reprog_budget1>0){
                            $expenditure[] = array('id'=>$value->id,'description'=>$value->description,'remain_amount'=>$value->remain_amount);
                        }

                   }elseif($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==2){


                        if($value->remain_amount>0){
                            
                            $value->remain_amount = $value->remain_amount;

                        }else{

                            $value->remain_amount=$value->reprog_budget2;
                        }

                        if($value->reprog_budget2>0){
                            $expenditure[] = array('id'=>$value->id,'description'=>$value->description,'remain_amount'=>$value->remain_amount);
                        }

                   }elseif($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==3){
                   
                        if($value->remain_amount>0){
                            
                            $value->remain_amount = $value->remain_amount;

                        }else{

                            $value->remain_amount=$value->reprog_budget3;
                        }
                    
                    
                        if($value->reprog_budget3>0){
                            $expenditure[] = array('id'=>$value->id,'description'=>$value->description,'remain_amount'=>$value->remain_amount);
                        }
                    
                   }elseif($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==4){
                    
                        if($value->remain_amount>0){
                            
                            $value->remain_amount = $value->remain_amount;

                        }else{

                            $value->remain_amount=$value->reprog_budget4;
                        }
                    
                    
                        if($value->reprog_budget4>0){
                            $expenditure[] = array('id'=>$value->id,'description'=>$value->description,'remain_amount'=>$value->remain_amount);
                        }
                    
                   }elseif($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==5){
                   
                            
                        if($value->remain_amount>0){
                            
                            $value->remain_amount = $value->remain_amount;

                        }else{

                            $value->remain_amount=$value->reprog_budget5;
                        }
                    
                    
                        if($value->reprog_budget5>0){
                            $expenditure[] = array('id'=>$value->id,'description'=>$value->description,'remain_amount'=>$value->remain_amount);
                        }
                    
                   }  
                }

            return response()->json([
                'data'    => $expenditure,
                'success' => true,
            ], 200);

           
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], 409);
        }
    }

    /**
     * Store expenses.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addExpenses(Request $request)
    {
        $this->validate($request, [
            'proposal_id'    => 'required',
            'bb_id'          => 'required',
            'quarter'        => 'required',
            'expense_amount' => 'required',
            'no_check'       => 'required',
            'infavourof'     => 'required',
        ]);

        try {

            $remainAmount = 0;
            $alert        = '';

            $proposal = Proposal::find($request->proposal_id);

            $modelRECount = ReportedExpenses::where('bb_id', $request->bb_id)
                ->where('proposal_id', $request->proposal_id)
                ->count();

            $budgetAmountDtl = BudgetBreakdown::where('id', $request->bb_id)
            ->where('proposal_id', $request->proposal_id)
            ->get();

            //In add expenses, expense amount should not exceeds the budget amount


            if($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==0){
                       
                if($request->expense_amount>$budgetAmountDtl[0]->tight_budget){

                    return response()->json([
                        'message' =>  'Your Expense amount('.$request->expense_amount.') should not exceeds the budget amount ('.$budgetAmountDtl[0]->tight_budget.')',
                        'success' => true,
                    ], 200);
                   
                }
               
           }elseif($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==1){
           
                if($request->expense_amount>$budgetAmountDtl[0]->reprog_budget1){
                    return response()->json([
                        'message' => 'Your Expense amount('.$request->expense_amount.') should not exceeds the budget amount ('.$budgetAmountDtl[0]->reprog_budget1.')',
                        'success' => true,
                    ], 200);
                }

           }elseif($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==2){
           
                if($request->expense_amount>$budgetAmountDtl[0]->reprog_budget2){
                    return response()->json([
                        'message' => 'Your Expense amount('.$request->expense_amount.') should not exceeds the budget amount ('.$budgetAmountDtl[0]->reprog_budget2.')',
                        'success' => true,
                    ], 200);
                }

           }elseif($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==3){
           
                if($request->expense_amount>$budgetAmountDtl[0]->reprog_budget3){
                    return response()->json([
                        'message' => 'Your Expense amount('.$request->expense_amount.') should not exceeds the budget amount ('.$budgetAmountDtl[0]->reprog_budget3.')',
                        'success' => true,
                    ], 200);
                }
            
           }elseif($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==4){
            
                if($request->expense_amount>$budgetAmountDtl[0]->reprog_budget4){
                    return response()->json([
                        'message' => 'Your Expense amount('.$request->expense_amount.') should not exceeds the budget amount ('.$budgetAmountDtl[0]->reprog_budget4.')',
                        'success' => true,
                    ], 200);
                }
            
           }elseif($proposal->application_status ==5 && $proposal->reprogramStatus ==1 && $proposal->reprogramCount ==5){
           
                if($request->expense_amount>$budgetAmountDtl[0]->reprog_budget5){
                    return response()->json([
                        'message' => 'Your Expense amount( '.$request->expense_amount.' ) should not exceeds the budget amount ('.$budgetAmountDtl[0]->reprog_budget5.')',
                        'success' => true,
                    ], 200);
                }
            
           }  

         

            if ($modelRECount > 0) {

                /*if( ($proposal->requested_grant != $proposal->approved_grant) && 
                    $proposal->approved_grant>0 && 
                    (
                        $proposal->application_status ==1 || 
                        $proposal->application_status ==2 || 
                        $proposal->application_status ==5
                    )
                )
                {*/
                    $reportExpensesDtl = ReportedExpenses::select('*', DB::raw('SUM(expense_amount) As expenseAmount'))
                    ->where('bb_id', $request->bb_id)
                    ->where('proposal_id', $request->proposal_id)
                    ->get();

                    if($budgetAmountDtl[0]->reprog_budget5){
                        $actualBudgetAmount = $budgetAmountDtl[0]->reprog_budget5;
                    
                    } elseif($budgetAmountDtl[0]->reprog_budget4){
                      
                        $actualBudgetAmount = $budgetAmountDtl[0]->reprog_budget4;
                    
                    }elseif($budgetAmountDtl[0]->reprog_budget3){
                        
                        $actualBudgetAmount = $budgetAmountDtl[0]->reprog_budget3;
                    
                    }elseif($budgetAmountDtl[0]->reprog_budget2){
                       
                        $actualBudgetAmount = $budgetAmountDtl[0]->reprog_budget2;
                    
                    }elseif($budgetAmountDtl[0]->reprog_budget1){
                       
                        $actualBudgetAmount = $budgetAmountDtl[0]->reprog_budget1;
                    
                    }elseif($budgetAmountDtl[0]->tight_budget){

                        $actualBudgetAmount = $budgetAmountDtl[0]->tight_budget;
                    
                    } 

                    $remainAmount = $actualBudgetAmount - ($reportExpensesDtl[0]->expenseAmount +  $request->expense_amount);
                  

                //}

            } else {


                // Tight Budget status checking
               
                if($budgetAmountDtl[0]->reprog_budget5){
                    $remainAmount = $budgetAmountDtl[0]->reprog_budget5 - $request->expense_amount;
                
                } elseif($budgetAmountDtl[0]->reprog_budget4){
                  
                    $remainAmount = $budgetAmountDtl[0]->reprog_budget4 - $request->expense_amount;
                
                }elseif($budgetAmountDtl[0]->reprog_budget3){
                    
                    $remainAmount = $budgetAmountDtl[0]->reprog_budget3 -  $request->expense_amount;
                
                }elseif($budgetAmountDtl[0]->reprog_budget2){
                   
                    $remainAmount =$budgetAmountDtl[0]->reprog_budget2 -  $request->expense_amount;
                
                }elseif($budgetAmountDtl[0]->reprog_budget1){
                   
                    $remainAmount =  $budgetAmountDtl[0]->reprog_budget1 - $request->expense_amount;
                
                }elseif($budgetAmountDtl[0]->tight_budget){
                
                    $remainAmount = $budgetAmountDtl[0]->tight_budget - $request->expense_amount;
                } 

            }

            DB::insert('insert into reported_expenses (proposal_id, bb_id, quarter, date, expense_amount, alert, no_check, infavourof, created_at, updated_at) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [$request->input('proposal_id'), $request->input('bb_id'), $request->input('quarter'), $request->input('date'), $request->input('expense_amount'), $alert, $request->input('no_check'), $request->input('infavourof'), date('Y-m-d H:i:s'), date('Y-m-d H:i:s')]);

            DB::table('budget_breakdowns')
            ->where('id', $request->bb_id)
            ->update(['remain_amount' => $remainAmount]);

            $alert = $remainAmount > 0 ? 'bellow' : 'over';

            return response()->json([
                'message' => 'Added Successfully!!',
                'success' => true,
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], 409);
        }

    }

    /**
     * Get Expense List.
     *
     * @return \Illuminate\Http\Response
     */
    public function getExpenseList($proposal_id, $budget_type, $quater = '')
    {
        try {
            $expenses = DB::table('reported_expenses AS re')
                ->leftJoin('budget_breakdowns AS bb', 'bb.id', '=', 're.bb_id')
                ->where('bb.proposal_id', '=', $proposal_id)
                ->where('bb.budget_type', '=', $budget_type)
                ->where('re.quarter', '=', 'Q' . $quater)
                ->select('re.id', 're.bb_id', 're.quarter', 're.date', 're.expense_amount', 're.alert', 're.no_check', 're.infavourof', 'bb.description', 'bb.budget_type', 'bb.budget_amount', 'bb.tight_budget', 'bb.reprog_budget1', 'bb.reprog_budget2', 'bb.reprog_budget3', 'bb.reprog_budget4', 'bb.reprog_budget5')
                ->get();

            $excelData   = array();
            $pcnt        = 0;
            $statusNote  = '';
            $statusLabel = '';
            foreach ($expenses as $pval) {

                if ($expenses[$pcnt]->alert == 'bellow') {

                    $statusNote  = 'fa-check green';
                    $statusLabel = 'bellow_alert';
                }
                if ($expenses[$pcnt]->alert == 'over') {
                    $statusNote  = 'fa-exclamation-triangle red';
                    $statusLabel = 'over_alert';
                }

                $expenses[$pcnt]->note  = $statusNote;
                $expenses[$pcnt]->label = $statusLabel;

                $pcnt++;
            }

            $q1Sum = DB::table('reported_expenses AS re')
                ->leftJoin('budget_breakdowns AS bb', 'bb.id', '=', 're.bb_id')
                ->where('bb.proposal_id', '=', $proposal_id)
                ->where('bb.budget_type', '=', $budget_type)
                ->where('re.quarter', '=', 'Q1')
                ->sum('re.expense_amount');

            $q2Sum = DB::table('reported_expenses AS re')
                ->leftJoin('budget_breakdowns AS bb', 'bb.id', '=', 're.bb_id')
                ->where('bb.proposal_id', '=', $proposal_id)
                ->where('bb.budget_type', '=', $budget_type)
                ->where('re.quarter', '=', 'Q2')
                ->sum('re.expense_amount');

            $q3Sum = DB::table('reported_expenses AS re')
                ->leftJoin('budget_breakdowns AS bb', 'bb.id', '=', 're.bb_id')
                ->where('bb.proposal_id', '=', $proposal_id)
                ->where('bb.budget_type', '=', $budget_type)
                ->where('re.quarter', '=', 'Q3')
                ->sum('re.expense_amount');

            if ($budget_type == 'Administrative') {
                DB::table('quaterly_reports')
                    ->where('proposal_id', $proposal_id)
                    ->update(['ab_c1' => $q1Sum, 'ab_c2' => $q2Sum, 'ab_c3' => $q3Sum]);
            }

            if ($budget_type == 'Direct') {
                DB::table('quaterly_reports')
                    ->where('proposal_id', $proposal_id)
                    ->update(['db_c1' => $q1Sum, 'db_c2' => $q2Sum, 'db_c3' => $q3Sum]);
            }

            return response()->json([
                'data'    => $expenses,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], 409);
        }
    }

    /**
     * Get Summary List.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSummaryList($proposal_id, $budget_type, $quater = '')
    {
        try {
            $expenses = DB::table('reported_expenses AS re')
                ->leftJoin('budget_breakdowns AS bb', 'bb.id', '=', 're.bb_id')
                ->where('bb.proposal_id', '=', $proposal_id)
                ->where('bb.budget_type', '=', $budget_type)
                ->where('re.quarter', '=', 'Q' . $quater)
                ->select('re.id', 're.bb_id', 're.quarter', 're.date', 're.expense_amount', 're.alert', 're.no_check', 're.infavourof', 'bb.description', 'bb.budget_type', 'bb.budget_amount', 'bb.remain_amount', 'bb.tight_budget', 'bb.reprog_budget1', 'bb.reprog_budget2', 'bb.reprog_budget3', 'bb.reprog_budget4', 'bb.reprog_budget5')
                ->groupBy('bb_id')
                ->get();

            $excelData   = array();
            $pcnt        = 0;
            $statusNote  = '';
            $statusLabel = '';
            
            foreach ($expenses as $pval) {

                if ($expenses[$pcnt]->remain_amount > 0) {

                    $statusNote  = 'fa-check green';
                    $statusLabel = 'bellow_alert';
                }

                if ($expenses[$pcnt]->remain_amount < 0 ) {
                    $statusNote  = 'fa-exclamation-triangle red';
                    $statusLabel = 'over_alert';
                }

                $expenses[$pcnt]->note  = $statusNote;
                $expenses[$pcnt]->label = $statusLabel;

                $pcnt++;
            }


            return response()->json([
                'data'    => $expenses,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], 409);
        }
    }

    /**
     * Delete Expense.
     *
     */
    public function deleteExpense($id)
    {
        try {

            $modelRE = ReportedExpenses::find($id);
            $modelBB = BudgetBreakdown::find($modelRE->bb_id);

            $updateAmount = $modelRE->expense_amount + $modelBB->remain_amount;

            $modelRECount = ReportedExpenses::where('bb_id', $modelRE->bb_id)
                ->where('id', '!=', $id)
                ->count();

            //return response()->json(['Reported Expenses count' => $modelRECount, 'success'=> false], 400);

            if ($modelRECount == 0) {
                $updateAmount = 0;
            }

            $modelBB->remain_amount = $updateAmount;
            $modelBB->save();

            DB::table('reported_expenses')->delete($id);

            return response()->json([
                'message' => "Removed Successfully!!",
                'success' => true,
            ], 201);

        } catch (\Exception $e) {
            $allErrors = $e->getMessage();
            $message   = $allErrors . ' ' . 'Please try again!';
            return response()->json([
                'message' => $message,
                'success' => false,
            ], 409);
        }
    }

    public function updateExpense(Request $request, $id)
    {
        $this->validate($request, [
            'no_check'   => 'required',
            'infavourof' => 'required',
            'date'       => 'required',
        ]);

        \DB::beginTransaction();
        try {

            /* Delete old amount*/
            $modelRE = ReportedExpenses::find($id);
            $modelBB = BudgetBreakdown::find($modelRE->bb_id);

            $updateAmount = $modelRE->expense_amount + $modelBB->remain_amount;

            $modelRECount = ReportedExpenses::where('bb_id', $modelRE->bb_id)
                ->count();

            //return response()->json(['Reported Expenses count' => $modelRECount, 'success'=> false], 400);

            if ($modelRECount == 0) {
                $updateAmount = 0;
            }

            $modelBB->remain_amount = $updateAmount;
            $modelBB->save();
            /* Delete old amount*/

            /* Update with new amount*/
            if ($modelRECount == 0) {
                $remainAmount = 0;

            } else {
                $remainAmount = $modelBB->remain_amount - $request->expense_amount;
            }

            $modelBB->remain_amount = $remainAmount;
            $modelBB->save();
            /* Update with new amount*/

            $model             = ReportedExpenses::find($id);
            $model->no_check   = $request->input('no_check');
            $model->infavourof = $request->input('infavourof');
            $model->date       = $request->input('date');

            $model->expense_amount = $request->input('expense_amount');

            $model->save();
            $modelId = $model->id;

            \DB::commit();
            $message   = 'Updated successfully!';
            $alerttype = true;

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message   = $allErrors . ' ' . 'Please try again!';
            $alerttype = false;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }

    /**
     * Get Evidences.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEvidences()
    {
        try {
            //$evidences = DB::table('evidences')->get();

            $evidences = DB::table('evidences')->get()->groupBy(function ($data) {
                return $data->RO;
            });

            $i       = 0;
            $dataArr = array();

            foreach ($evidences as $rkey => $rval) {

                $dataArr[$i]['type'] = $rkey;
                $dataArr[$i]['list'] = $rval;

                $i++;
            }

            return response()->json([
                //'data'    => $evidences,
                'data'    => $dataArr,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], 409);
        }
    }

    public function moveEvidenseFile(Request $request)
    {

        $this->validate($request, [
            'proposal_id' => 'required',
            'evidence_id' => 'required',
            'quarter'     => 'required',
        ]);

        if ($request->hasFile('programmatic_report')) {

            $file        = $request->file('programmatic_report');
            $proposal_id = $request->input('proposal_id');
            $evidence_id = $request->input('evidence_id');
        }

        $fileCount = count((array) $file);

        try {
            $ed = new EvidenceDocuments;

            $notMoveFileArr = array();

            for ($i = 0; $i < $fileCount; $i++) {
                $originalFileName = $file[$i]->getClientOriginalName();
                $filteredOrgName  = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $originalFileName);
                $extension        = strtolower($file[$i]->getClientOriginalExtension());
                $fileName         = date('YmdHmsu') . '-' . $filteredOrgName;

                $fileSize = $file[$i]->getSize();

                $fileSize = number_format($fileSize / 1048576, 2);

                $destinationPath = "upload/evidences/" . $proposal_id . "/";
                $fullFilePath    = $destinationPath . $fileName;

                if ($file[$i]->move($destinationPath, $fileName)) {

                    if ($fileSize > 4) {

                        $oldFullFilePath = $fullFilePath;

                        $fileName     = date('YmdHmsu') . '-converted-' . $filteredOrgName;
                        $fullFilePath = $destinationPath . $fileName;

                        if ($extension == 'pdf') {

                            shell_exec('ps2pdf -dPDFSETTINGS=/default ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } else if ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff') {

                            shell_exec('convert -quality 50% ' . $oldFullFilePath . '  ' . $fullFilePath . '');

                        } /*else{
                        rename( $orgFullFilePath, $fullFilePath);
                        }*/

                        if (File::exists($oldFullFilePath)) {
                            File::delete($oldFullFilePath);
                        }
                    }

                    $edArr[] = [
                        'evidence_id'   => $evidence_id,
                        'proposal_id'   => $proposal_id,
                        'quarter'       => $request->input('quarter'),
                        'org_file_name' => $originalFileName,
                        'file_name'     => $fileName,
                        'file_path'     => $fullFilePath,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s'),
                    ];

                } else {
                    $notMoveFileArr[] = $originalFileName;
                }
            }
            $ed::insert($edArr);
            return response()->json([
                'message'     => 'Uploaded Successfully',
                'NotUploaded' => $notMoveFileArr,
                'success'     => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function getEvidencefileList($proposal_id, $quater = '')
    {
        try {
            $evidence = DB::table('evidence_documents AS ed')
                ->leftJoin('evidences AS ev', 'ev.id', '=', 'ed.evidence_id')
                ->where('ed.proposal_id', '=', $proposal_id)
                ->where('ed.quarter', '=', 'Q' . $quater)
                ->select('ev.evidence', 'ev.RO', 'ed.id', 'ed.org_file_name', 'ed.file_name', 'ed.file_path')
                ->get();
            return response()->json([
                'data'    => $evidence,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], 409);
        }
    }

    public function evidencefileDelete($id)
    {
        try {
            $ed = new EvidenceDocuments;

            $evidenceDoc = $ed::find($id);

            $filename = $evidenceDoc['file_path'];

            if (File::exists($filename)) {
                File::delete($filename);
                $evidenceDoc->delete();
                return response()->json([
                    'message' => 'Evidence document removed successfully',
                    'success' => true,
                ], 201);
            } else {
                return response()->json('File does not exist');
            }
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }



    public function getUpdateBugetAmount($proposal_id){
        try {
            $proposals = DB::table('budget_breakdowns')
                ->where('budget_breakdowns.proposal_id', '=', $proposal_id)
                ->get();

            if($proposals){

                // To show updated budget total in expense summary
                $adminBudgetDtl = DB::table('budget_breakdowns')
                ->select('*', DB::raw('SUM(tight_budget) As tightBudget'),DB::raw('SUM(reprog_budget1) As reprogBudget1'),DB::raw('SUM(reprog_budget2) As reprogBudget2'),DB::raw('SUM(reprog_budget3) As reprogBudget3'),DB::raw('SUM(reprog_budget4) As reprogBudget4'),DB::raw('SUM(reprog_budget5) As reprogBudget5'))
                ->where('budget_type', 'Administrative')
                ->where('proposal_id', $proposal_id)
                ->get();

                $directBudgetDtl = DB::table('budget_breakdowns')
                ->select('*', DB::raw('SUM(tight_budget) As tightBudget'),DB::raw('SUM(reprog_budget1) As reprogBudget1'),DB::raw('SUM(reprog_budget2) As reprogBudget2'),DB::raw('SUM(reprog_budget3) As reprogBudget3'),DB::raw('SUM(reprog_budget4) As reprogBudget4'),DB::raw('SUM(reprog_budget5) As reprogBudget5'))
                ->where('budget_type', 'Direct')
                ->where('proposal_id', $proposal_id)
                ->get();

               
                if($directBudgetDtl[0]->reprog_budget5){
                    $proposals[0]->administrative_budget = $adminBudgetDtl[0]->reprogBudget1;
                    $proposals[0]->direct_budget         = $directBudgetDtl[0]->reprogBudget1;
                
                } elseif($directBudgetDtl[0]->reprog_budget4){
                    $proposals[0]->administrative_budget = $adminBudgetDtl[0]->reprogBudget2;
                    $proposals[0]->direct_budget         = $directBudgetDtl[0]->reprogBudget2;
                
                }elseif($directBudgetDtl[0]->reprog_budget3){
                    $proposals[0]->administrative_budget = $adminBudgetDtl[0]->reprogBudget3;
                    $proposals[0]->direct_budget         = $directBudgetDtl[0]->reprogBudget3;
                
                }elseif($directBudgetDtl[0]->reprog_budget2){
                    $proposals[0]->administrative_budget = $adminBudgetDtl[0]->reprogBudget4;
                    $proposals[0]->direct_budget         = $directBudgetDtl[0]->reprogBudget4;
                
                }elseif($directBudgetDtl[0]->reprog_budget1){
                    $proposals[0]->administrative_budget = $adminBudgetDtl[0]->reprogBudget5;
                    $proposals[0]->direct_budget         = $directBudgetDtl[0]->reprogBudget5;
                
                }elseif($directBudgetDtl[0]->tight_budget){
                
                    $proposals[0]->administrative_budget = $adminBudgetDtl[0]->tightBudget;
                    $proposals[0]->direct_budget         = $directBudgetDtl[0]->tightBudget;
                }
              
            }else{
                $model = Proposal::find($proposal_id);
                $proposals[0]->administrative_budget = ($model->approved_grant * 40) / 100;
                $proposals[0]->direct_budget         = ($model->approved_grant * 60) / 100;

            }

            return response()->json([
                'data'    => $proposals,
                'success' => true,
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], 409);
        }


    }

}
