<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterConfirmation extends Mailable
{
    use Queueable, SerializesModels;
     /**
     * Create a new message instance.
     *
     * @return void
     */
    public $message_bdy;
    public $mail_subject;

    public function __construct($message_bdy)
    {
        $this->message_bdy = $message_bdy;
        //$this->mail_subject = $mail_subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->view('emails.RegisterConfirmationEmail');

        $message_bdy = $this->message_bdy;
        $data['name'] = $message_bdy['name'];
        $data['link'] = $message_bdy['link'];
        $data['email'] = $message_bdy['email'];
        $data['password'] = $message_bdy['password'];

        $this->withSwiftMessage(function ($data) {
            $data->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
        });
    }
}