<?php
namespace App\Http\Controllers;

use App\EvidenceDocuments;
use App\Refund;
use App\Mail\Refundnotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class RefundController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $proposal_id)
    {
        $pageSize = $request->input('pageSize') ? $request->input('pageSize') : '10';

        $select = Refund::where('proposal_id', $proposal_id)
            ->orderBy('id', 'DESC');

        return response()->json([
            'data'    => $select->paginate($pageSize),
            'success' => true,
        ], 200);

    }

    public function show($id)
    {
        $data = Refund::find($id);
        //return response()->json($data);

        return response()->json([
            'data'    => $data,
            'success' => true,
        ], 200);
    }

    /**
     * Store a quartly report.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'proposal_id'      => 'required',
            'expenditure_desc' => 'required',
            'expense_amount'   => 'required',
            'check_no'         => 'required',
            'infavourof'       => 'required',
            'expense_date'     => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $model = new Refund;

            $model->user_id          = Auth::user()->id;
            $model->proposal_id      = $request->input('proposal_id');
            $model->expenditure_desc = $request->input('expenditure_desc');
            $model->expense_amount   = $request->input('expense_amount');
            $model->check_no         = $request->input('check_no');
            $model->infavourof       = $request->input('infavourof');
            $model->expense_date     = $request->input('expense_date');

            $model->save();

            /*When the entity requests a refund, the system must send an email to the analyst with the entity info, the proposal number and the amount of the refund requested.
            https://trello.com/c/zPoMmLTS/66-refund
            */

            $entityDetails = DB::table('users')->where('id', $model->user_id)->first();

            $proposal = Proposal::find($model->proposal_id);

            $analyst = DB::table('assign_user_to_proposals AS aup')
                    ->leftJoin('users', 'users.id', '=', 'aup.user_id')
                    ->where('aup.proposal_id', '=', $model->proposal_id)
                    ->select('users.email', 'users.admin_first_name', 'users.admin_last_name')
                    ->first();

            $data['analyst_name']     = $analyst->admin_first_name . ' ' . $analyst->admin_last_name;
            $data['proposal_id']      = $proposal->proposal_unique_id;
            $data['entity_name']      = $entityDetails->entity_name;
            $data['requested_amount'] = $model->expense_amount;

            Mail::to($analyst->email)->send(new Refundnotification($data));

            $this->insertAuditTrail('AUDIT_TRAIL.PROPOSAL', 'AUDIT_TRAIL.PROPOSAL_REFUND_REQUEST', 'Application Id:  ' . $proposal->proposal_unique_id);

            \DB::commit();
            $message   = 'Refund requested successfully!';
            $alerttype = true;

        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message   = $allErrors . ' ' . 'Please try again!';
            $alerttype = false;

        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }

    /**
     * Update existing project.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'refund_amount' => 'required',
            'refund_status' => 'required',
        ]);

        $userId = Auth::user()->id;

        \DB::beginTransaction();
        try {

            $model = Refund::find($id);

            if (!empty($model)) {
                $model->refund_amount = $request->input('refund_amount');
                $model->refund_status = $request->input('refund_status');
                $model->refund_by     = $userId;
                $model->refund_date   = date('Y-m-d H:i:s');

                $model->save();

                $this->insertAuditTrail('AUDIT_TRAIL.PROPOSAL', 'AUDIT_TRAIL.PROPOSAL_REFUND', 'Application Id:  ' . $request->input('proposal_unique_id'));

                \DB::commit();
                $message   = 'Refund request updated successfully!';
                $alerttype = true;
            } else {
                $message   = 'Refund request not exists!';
                $alerttype = true;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message   = $allErrors . ' ' . 'Please try again!';
            $alerttype = false;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'update'  => true,
            'success' => $alerttype,
        ], 201);
    }

    /**
     * Get report details.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getProposal(Request $request, $proposal_id)
    {
        try {
            $proposals = DB::table('proposals')
                ->leftJoin('projects', 'projects.id', '=', 'proposals.project_id')
                ->leftJoin('users', 'users.id', '=', 'proposals.user_id')
                ->where('proposals.id', '=', $proposal_id)
                ->select('proposals.id', 'proposals.proposal_unique_id', 'proposals.application_status', 'proposals.created_at', 'proposals.updated_at', 'projects.projectNameEn', 'projects.projectNameEs', 'projects.startDate', 'projects.endDate', 'projects.period', 'users.entity_name', 'users.entity_code')
                ->get();

            

            return response()->json([
                'data'    => $proposals,
                'success' => true,
            ], 201);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'success' => false,
            ], 409);
        }
    }

    /**
     * Delete Expense.
     *
     */
    public function delete($id)
    {
        try {
            DB::table('refunds')->delete($id);

            return response()->json([
                'message' => "Removed Successfully!!",
                'success' => true,
            ], 201);

        } catch (\Exception $e) {
            $allErrors = $e->getMessage();
            $message   = $allErrors . ' ' . 'Please try again!';
            return response()->json([
                'message' => $message,
                'success' => false,
            ], 409);
        }
    }

    

}
