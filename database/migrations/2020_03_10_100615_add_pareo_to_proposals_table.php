<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPareoToProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->integer('pareo_type')->nullable()->after('programmatic_status');
            $table->text('pareo_origin')->nullable()->after('pareo_type');
            $table->integer('pareo_category')->nullable()->after('pareo_origin');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->dropColumn('pareo_type');
            $table->dropColumn('pareo_origin');
            $table->dropColumn('pareo_category');
            
        });
    }
}
