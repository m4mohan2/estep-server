<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('ipAddress')->nullable()->after('status');
            $table->unsignedBigInteger('createdBy')->after('ipAddress')->comment('User who created the project');
            $table->unsignedBigInteger('updatedBy')->nullable()->after('ipAddress')->comment('User who modified the project');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('ipAddress');
            $table->dropColumn('CreatedBy');
            $table->dropColumn('UpdatedBy');
        });
    }
}
