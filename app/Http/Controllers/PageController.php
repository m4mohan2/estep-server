<?php
namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->get('search');

        if ($search != '') {
            $Pages = Page::where('title_en', 'like', '%' . $search . '%')
                ->orWhere('title_es', 'like', '%' . $search . '%')
                ->orderBy('id', 'DESC')
                ->paginate(10);
        } else {
            $Pages = Page::orderBy('id', 'DESC')->paginate(10);
        }

        return response()->json($Pages);
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'title_en'       => 'required',
            'title_es'       => 'required',
            'description_en' => 'required',
            'description_es' => 'required',
            'status'         => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $model                 = new Page;
            $model->title_en       = $request->input('title_en');
            $model->title_es       = $request->input('title_es');
            $model->slug           = preg_replace("![^a-z0-9]+!i", "-", strtolower($model->title_en));
            $model->description_en = $request->input('description_en');
            $model->description_es = $request->input('description_es');
            $model->link_en        = $request->input('link_en');
            $model->link_es        = $request->input('link_es');
            $model->status         = $request->input('status');

            $page = Page::where('slug', '=', $model->slug)->first();
            if ($page === null) {
                // page doesn't exist
                $model->save();
                $modelId = $model->id;

                \DB::commit();
                $message   = 'Page has been added successfully!';
                $alerttype = true;
            } else {
                $message   = 'Page is already exist.';
                $alerttype = false;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message   = $allErrors . ' ' . 'Please try again!';
            $alerttype = false;

        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }

    public function show($id)
    {
        $model = Page::find($id);
        return response()->json($model);
    }

    public function pageContent(Request $request)
    {
        $slug = $request->input('short_code');
        if ($slug != '') {
            $model = Page::where('slug', $slug)->get();
            return response()->json($model);
        } else {
            return response()->json([
                'message' => 'Unable to access',
                'success' => false,
            ], 409);
        }
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title_en'       => 'required',
            'title_es'       => 'required',
            'description_en' => 'required',
            'description_es' => 'required',
            'status'         => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $model           = Page::find($id);
            $model->title_en = $request->input('title_en');
            $model->title_es = $request->input('title_es');
            //$model->slug   =  preg_replace("![^a-z0-9]+!i", "-", strtolower($model->title_en));
            $model->description_en = $request->input('description_en');
            $model->description_es = $request->input('description_es');
            $model->link_en        = $request->input('link_en');
            $model->link_es        = $request->input('link_es');
            $model->status         = $request->input('status');

            $page = Page::where('slug', '=', $model->slug)->where('id', '!=', $id)->first();

            if ($page === null) {
                // page doesn't exist
                $model->save();
                $modelId = $model->id;

                \DB::commit();
                $message   = 'Page has been updated successfully!';
                $alerttype = true;
            } else {
                $message   = 'Page is already exist.';
                $alerttype = false;
            }
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message   = $allErrors . ' ' . 'Please try again!';
            $alerttype = false;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }

    public function delete($id)
    {
        $model = Page::find($id);
        $model->delete();

        //Return message
        return response()->json([
            'message' => 'Removed Successfully!!',
            'success' => true,
        ], 201);
    }
}
