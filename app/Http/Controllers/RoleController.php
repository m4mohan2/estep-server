<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Http\Resources\Role as RoleResource;
use App\Http\Resources\RoleCollection;




class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
       /// $roles = Role::where('roleKey','!=','companies')->where('roleKey','!=','system-admin')->get();

        return response()->json([
            'data' => $roles ,
            'totalCount' => $roles->count(), 
            'resCode' => 200,
            'success'=> true,
        ], 200);

        // with collection method
        /*return response()->json([
            'data' => new RoleCollection($roles) ,
            'totalCount' => $roles->count(), 
            'resCode' => 200,
            'success'=> true,
        ], 200);*/
    }

    /**
     * Store a newly created role in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'roleName' => 'required|unique:roles',
            'facility' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $model = New Role;
            $model->roleName  =  $request->input('roleName');
            $model->facility  =  json_encode($request->input('facility'));
            $model->roleDescription  =  $request->input('roleDescription');
            $model->roleKey   =  preg_replace("![^a-z0-9]+!i", "-", strtolower($model->roleName));
            
            $model->save();
            $modelId = $model->id;
            
            //print_r($model);die;
           
            \DB::commit();
            $message = 'Record has been added successfully!';
            $alerttype = true;
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors.' '.'Please try again!';
            $alerttype = false;

        }
        
        //Return message
        return response()->json([
            'message'    => $message,
            'success' => $alerttype,
        ], 201);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        return response()->json($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'roleName' => 'required',
            'facility' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $model = Role::find($id);
            $model->roleName  =  $request->input('roleName');
            $model->facility  =  json_encode($request->input('facility'));
            $model->roleDescription  =  $request->input('roleDescription');
            
            $model->save();
            $modelId = $model->id;
            //print_r($model);die;
           
            \DB::commit();
            $message = 'Record has been updated successfully!';
            $alerttype = true;
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors . ' '.'Please try again!';
            $alerttype = false;
        }

        //Return message
        return response()->json([
            'message'    => $message,
            'success' => $alerttype,
        ], 201);
        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();
        return response()->json([
            'message'    => 'role removed successfully',
            'success' => true,
        ], 201);
        //return response()->json('role removed successfully');
    }
}
