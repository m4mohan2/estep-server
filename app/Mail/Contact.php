<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contact extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $ofsl_name;
    public $town;
    public $telephone;
    public $email;
    public $comment;
    public $logo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->name = $data['name'];
        $this->ofsl_name = $data['ofsl_name'];
        $this->town = $data['town'];
        $this->telephone = $data['telephone'];
        $this->email = $data['email'];
        $this->comment = $data['comment'];
        $this->logo = env('LOGO_URL');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('New Contact')->view('emails.contact');

        $data['name'] = $this->name;
        $data['ofsl_name'] = $this->ofsl_name;
        $data['town'] = $this->town;
        $data['telephone'] = $this->telephone;
        $data['email'] = $this->email;
        $data['comment'] = $this->comment;
        $data['logo'] = $this->logo;

        $this->withSwiftMessage(function ($data) {
            $data->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
        });
    }
}