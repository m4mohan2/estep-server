<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuaterlyReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quaterly_reports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('proposal_id')->comment('Proposal parent Id');
            $table->foreign('proposal_id')->references('id')->on('proposals')->onDelete('cascade');
            $table->double('administrative_budget', 15, 8)->nullable();
            $table->double('direct_budget', 15, 8)->nullable();
            $table->double('ab_c1', 15, 8)->nullable();
            $table->double('ab_c2', 15, 8)->nullable();
            $table->double('ab_c3', 15, 8)->nullable();
            $table->double('ab_c4', 15, 8)->nullable();
            $table->double('db_c1', 15, 8)->nullable();
            $table->double('db_c2', 15, 8)->nullable();
            $table->double('db_c3', 15, 8)->nullable();
            $table->double('db_c4', 15, 8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quaterly_reports');
    }
}
