<?php
namespace App\Http\Controllers;

use App\Mail\Moreinfonotification;
use App\MoreInfo;
use App\Proposal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MoreinfoController extends Controller
{
    /**
     * Instantiate a new MoreinfoController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $moreinfo = MoreInfo::paginate(10);
        return response()->json($moreinfo);
    }

    /**
     * Store a newly created More Info.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'MoreInfoBody' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $model = new MoreInfo;

            $model->info_note   = $request->input('MoreInfoBody');
            $model->proposal_id = $request->input('ProposalId');
            $model->created_by  = $request->input('CreatedBy');
            $model->created_at  = date('Y-m-d H:i:s');
            $model->updated_at  = date('Y-m-d H:i:s');

            $model->save();
            $modelId = $model->id;

            $proposal = Proposal::find($request->input('ProposalId'));

            //return response()->json($proposal->approved_grant);

            if( $proposal->approved_grant == 0 && ( $proposal->application_status!= 5 || $proposal->application_status!= 2) ){
                $proposal->application_status = '3';
            }

            $proposal->save();
            $proposalId = $proposal->id;

            $entityDetails  = DB::table('users')->where('id', $proposal->user_id)->first();
            $projectDetails = DB::table('projects')->where('id', $proposal->project_id)->first();

            $data['entity_name']  = $entityDetails->entity_name;
            $data['project_name'] = $projectDetails->projectNameEn;
            $data['proposal_id']  = $proposal->proposal_unique_id;
            $data['info_note']    = $request->input('MoreInfoBody');

            $entityEmailId = $entityDetails->email;
            //$entityEmailId = "mohank.das@indusnet.co.in";

            Mail::to($entityEmailId)->send(new Moreinfonotification($data));

            // Logs
            $this->createProposalLogs($proposalId, 'MORE_INFO_ADD');

            \DB::commit();
            $message   = 'More Info has been added successfully!';
            $alerttype = true;
        } catch (\Exception $e) {
            \DB::rollback();
            $allErrors = $e->getMessage();

            $message   = $allErrors . ' ' . 'Please try again!';
            $alerttype = false;

        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }

    public function delete($id)
    {
        $notes = MoreInfo::find($id);
        $notes->delete();

        return response()->json('More Info removed successfully');
    }

    public function list($proposal_id) {
        try {
            $notes = MoreInfo::with('logUser')->where('proposal_id', '=', $proposal_id)->orderBy('id', 'DESC')->get();
            return response()->json([
                'data'    => $notes,
                'resCode' => 200,
                'success' => true,
            ], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

}
