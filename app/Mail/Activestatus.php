<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Activestatus extends Mailable
{
    use Queueable, SerializesModels;
    public $name;
    public $logo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->logo = env('LOGO_URL');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Your account is activated')->view('emails.activestatus');

        //$data = $this->name;

        $data['name'] = $this->name;
        $data['logo'] = $this->logo;

        $this->withSwiftMessage(function ($data) {
            $data->getHeaders()
                    ->addTextHeader('Custom-Header', 'HeaderValue');
        });
    }
}