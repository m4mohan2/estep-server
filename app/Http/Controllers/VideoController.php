<?php 
namespace App\Http\Controllers;

use App\Video;
use App\VideoDocument;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);

class VideoController extends Controller
{
    /**
     * Instantiate a new VideoController instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $video = Video::orderBy('id', 'DESC')->paginate(10);
        return response()->json($video);
    }

     /**
     * Store a newly created video.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'videoCode' => 'required',
            'descriptions' => 'required',
            //'descriptionsSpanish' => 'required',
            'status' => 'required',
            'CreatedBy' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            $model = New Video;

            //$users = DB::table('users')->select('id')->get();
            $datetime = date('Y-m-d H:i:s');

            $model->videoCode          =  $request->input('videoCode');
            $model->descriptions           =  $request->input('descriptions');
            $model->descriptionsSpanish    =  $request->input('descriptionsSpanish');
            $model->status     =  $request->input('status');
            $model->ip_address =  $this->get_client_ip();
            $model->CreatedBy  =  $request->input('CreatedBy');
            $model->UpdatedBy  =  $request->input('UpdatedBy');
            $model->created_at =  date('Y-m-d H:i:s');
            $model->updated_at =  date('Y-m-d H:i:s');
            $model->save();
            
            //$modelId = $model->id;

           

            /* foreach($users AS $userVal){
                DB::insert('insert into push_notifications (module_name, module_id, user_id, read_status, created_at, updated_at) values (?, ?, ?, ?, ?, ?)', ['Video', $modelId,$userVal->id, 0, $datetime, $datetime]);
            } */

            //$this->insertAuditTrail('AUDIT_TRAIL.VIDEO','AUDIT_TRAIL.VIDEO_ADD','Video Title:  '.$request->input('videoCode'));

            \DB::commit();
            $message = 'Video has been added successfully!';
            $alerttype = true;
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors.' '.'Please try again!';
            $alerttype = false;

        }

        //Return message
        return response()->json([
            'message'   => $message,  
            'success'   => $alerttype,
        ], 201);
    }

    /**
     * Update existing video.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $this->validate($request, [
            'videoCode' => 'required',
            'descriptions' => 'required',
            'descriptionsSpanish' => 'required',
            'status' => 'required',
            'UpdatedBy' => 'required'
        ]);

        \DB::beginTransaction();
        try {

            $model = Video::find($id);

            if(!empty($model)){
                $model->videoCode          =  $request->input('videoCode');
                $model->descriptions       =  $request->input('descriptions');
                $model->descriptionsSpanish=  $request->input('descriptionsSpanish');
                $model->status             =  $request->input('status');
                $model->UpdatedBy          =  $request->input('UpdatedBy');
                $model->updated_at         =  date('Y-m-d H:i:s');
                
                $model->save();

                
                \DB::commit();
                $message = 'Video has been updated successfully!';
                $alerttype = true;
            }else{
                $message = 'Video not exists!';
                $alerttype = true;
            }
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors . ' '.'Please try again!';
            $alerttype = false;
        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }

    public function show($id)
    {
        $video = Video::find($id);
        return response()->json($video);
    }

    public function viewBySlug($slug)
    {
        
        $video = Video::where('slug', '=', $slug)->get();
        
        return response()->json($video);
    }


    public function delete($id){
        $video = Video::find($id);
        $video->delete(); 

        //$this->insertAuditTrail('AUDIT_TRAIL.VIDEO','AUDIT_TRAIL.VIDEO_DELETE','Video Title:  '.$video->videoCode);

        return response()->json([
            'message' => 'Video removed successfully',
            'success' => true,
        ], 200);
    }

    protected function moveFile($file,$video_id){

        $fileCount = count((array)$file);

        try {
            $nd = new VideoDocument;

            $notMoveFileArr = array();

            for ($i=0; $i<$fileCount; $i++) {
                    $originalFileName = $file[$i]->getClientOriginalName();
                    $filteredOrgName = preg_replace('/[^a-zA-Z0-9-_\.]/','', $originalFileName);
                    $extension = strtolower($file[$i]->getClientOriginalExtension());
                    $fileName = date('YmdHmsu').'-'.$filteredOrgName;

                    $fileSize = $file[$i]->getSize();

                    $fileSize = number_format($fileSize / 1048576,2);

                    $destinationPath = "upload/video_documents/".$video_id."/";
                    $fullFilePath = $destinationPath.$fileName;

                if($file[$i]->move($destinationPath,$fileName)){

                    if($fileSize > 6){

                        $oldFullFilePath = $fullFilePath;

                        $fileName = date('YmdHmsu').'-converted-'.$filteredOrgName;
                        $fullFilePath = $destinationPath.$fileName;

                        if($extension == 'pdf'){

                            shell_exec ('ps2pdf -dPDFSETTINGS=/default '.$oldFullFilePath.'  '.$fullFilePath.'');

                        }else if($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif' || $extension == 'tiff'){

                            shell_exec ('convert -quality 50% '.$oldFullFilePath.'  '.$fullFilePath.'');

                        } /*else{
                            rename( $orgFullFilePath, $fullFilePath);
                        }*/

                        if (File::exists($oldFullFilePath)) {
                            File::delete($oldFullFilePath);
                        }
                    }

                    $ndArr[] =[
                        'video_id'       => $video_id,
                        'doc_name'      => $fileName,
                        'doc_path'      => $fullFilePath,
                        'status'        => 1,
                        'created_at'    => date('Y-m-d H:i:s'),
                        'updated_at'    => date('Y-m-d H:i:s')
                       ]; 
                       
                }else{
                    $notMoveFileArr[] = $originalFileName;
                }
            }
            $nd::insert($ndArr);
        return $fileCount;
        }catch(\Exception $e){
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }

    public function videoFileDelete($id){
        try {
            $nd = new VideoDocument;

            $videoDoc = $nd::find($id);
 
            $filename = $videoDoc['file_path'];

            if (File::exists($filename)) {
                File::delete($filename);
                $videoDoc->delete();
                return response()->json('Video document removed successfully');
            }else{
                return response()->json('File does not exist');
            }
        }catch(\Exception $e){
            return response()->json(['error' => $e->getMessage()], 409);
        }
    }


     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function activeVideo()
    {
        $video = Video::where('status', '1')
                    ->orderBy('id', 'DESC')
                    ->paginate(10);
        return response()->json($video);
    }

    protected function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

 
}