<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->comment('User parrent Id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('proposal_unique_id')->nullable();
            $table->integer('city')->nullable();
            $table->string('town')->nullable();
            $table->string('community')->nullable();
            $table->text('population')->nullable();
            $table->enum('categories',[0,1,2])->nullable()->comment('0=>Male,1=>Female,2=>All');
            $table->text('necessity_statement')->nullable()->comment('Necessity Statement');
            $table->string('programmatic_approach')->nullable()->comment('Programmatic Approach');
            $table->text('goal_to_achieve')->nullable()->comment('What goal does your community service or program seek to achieve in the proposed community and population?');
            $table->text('changes_or_benefits')->nullable()->comment('Describe the changes and/or benefits that will be gained by the implementation of your community program or service.');
            $table->text('specific_activities')->nullable()->comment('Explain what specific activities will your community service or program do.');
            $table->text('who_will_affected')->nullable()->comment('Explain who will be affected by your community program or service.');
            $table->text('how_will_pay')->nullable()->comment('Explain how will your proposed community program or service will pay for the development of the programmatic approach proposed by your organization.');
            $table->text('resources_required')->nullable()->comment('Explain what kind of resources will be required by your community program or service.');
            $table->text('work_plan')->nullable()->comment('Detailed Work Plan following the requirements and format set forth on the template provided herein.');
            $table->text('collecting_relevant_data')->nullable()->comment('Does your organization evaluate programs by collecting and measuring relevant data?');
            $table->text('how_often_organization_evaluates')->nullable()->comment('How often does your organization evaluate its programs and services?');
            $table->text('who_evaluates')->nullable()->comment('Who in your organization evaluates the programs, policies and practices?');
            $table->text('statements_describes_type_of_evaluations')->nullable()->comment('Which of the following statements best describes the type of evaluations that are carried out in your organization?');
            $table->text('type_of_evaluation')->nullable()->comment('Type of evaluation to use.');
            $table->text('type_of_data')->nullable()->comment('Type of data to be collected.');
            $table->text('type_of_evaluator')->nullable()->comment('Type of evaluator to use.');
            $table->text('inventory_of_property')->nullable()->comment('Inventory of property, material and equipment required to offer the services');
            $table->string('name')->nullable()->comment('Name');
            //$table->date('date')->nullable();
            $table->timestamp('date')->default(\DB::raw('CURRENT_TIMESTAMP'))->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
