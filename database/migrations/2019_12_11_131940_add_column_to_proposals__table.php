<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->integer('completed_step')->nullable()->after('date')->comment('Current Application Step');
            $table->enum('application_status',[0,1,2,3,4,5,6,7,8,9,10])->nullable()->after('date')->comment('0=>Draft,1=>Submitted,2=>Waiting for approval,3=Need more Info,4=Rejected,5=>Accepted,6=>Evaluation Complete')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->dropColumn('completed_step');
            $table->dropColumn('application_status');
        });
    }
}
