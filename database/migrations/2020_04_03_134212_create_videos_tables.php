<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideosTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('videoCode');
            $table->text('descriptions');
            $table->text('descriptionsSpanish');
            $table->string('ip_address')->nullable();
            $table->enum('status',[0,1])->nullable()->comment('0=>Unpublish,1=>Published');
            $table->unsignedBigInteger('CreatedBy')->comment('User who creted the video');
            $table->unsignedBigInteger('UpdatedBy')->nullable()->comment('User who updamodified the video');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
