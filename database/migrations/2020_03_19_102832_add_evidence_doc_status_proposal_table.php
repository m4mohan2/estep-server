<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEvidenceDocStatusProposalTable extends Migration
{
    
    
   /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->enum('evidence_doc_status',[0,1,2,3])->default(0)->after('inventory_file_status')->comment('1=>Compiles,2=>Does not comply,3=>Not applicable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposals', function (Blueprint $table) {
            $table->dropColumn('evidence_doc_status');
        });
    }
}
