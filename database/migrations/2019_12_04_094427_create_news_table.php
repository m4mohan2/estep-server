<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('NewsTitle');
            $table->text('NewsBody');
            $table->enum('Status',[0,1,2])->nullable()->comment('0=>Unpublish,1=>Published,2=>Remove');
            $table->string('ip_address')->nullable();
            $table->unsignedBigInteger('CreatedBy')->comment('User who creted the news');
            $table->unsignedBigInteger('UpdatedBy')->nullable()->comment('User who updamodified the news');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
