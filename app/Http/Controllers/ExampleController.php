<?php

namespace App\Http\Controllers;
use App\ReportedExpenses;
use App\BudgetBreakdown;
use DB;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function updateRemaingBalance()
    {

        //$budgetBreakdowns = BudgetBreakdown::where('updated_at', '<', date('Y-m-d'))->get();
        $budgetBreakdowns = BudgetBreakdown::all();

        foreach($budgetBreakdowns as $budgetBreakdown){
          
            $reportExpensesDtl = ReportedExpenses::select('*', DB::raw('SUM(expense_amount) As expenseAmount'))
            ->where('bb_id',  $budgetBreakdown['id'])
            ->where('proposal_id', $budgetBreakdown['proposal_id'])
            ->get();

            if($reportExpensesDtl){
                $remainAmount = 0;
               
                    // Tight Budget status checking
    
                    if($budgetBreakdown['reprog_budget5']){
                        $remainAmount = $budgetBreakdown['reprog_budget5'] - $reportExpensesDtl[0]->expenseAmount;
    
                    } elseif($budgetBreakdown['reprog_budget4']){
                        $remainAmount = $budgetBreakdown['reprog_budget4'] - $reportExpensesDtl[0]->expenseAmount;
    
                    }elseif($budgetBreakdown['reprog_budget3']){
                        $remainAmount = $budgetBreakdown['reprog_budget3'] - $reportExpensesDtl[0]->expenseAmount;
    
                    }elseif($budgetBreakdown['reprog_budget2']){
                        $remainAmount = $budgetBreakdown['reprog_budget2'] - $reportExpensesDtl[0]->expenseAmount;
    
                    }elseif($budgetBreakdown['reprog_budget1']){
                        $remainAmount = $budgetBreakdown['reprog_budget1'] - $reportExpensesDtl[0]->expenseAmount;
    
                    }elseif($budgetBreakdown['tight_budget']){
    
                        $remainAmount = $budgetBreakdown['tight_budget'] - $reportExpensesDtl[0]->expenseAmount;
    
                    }else{
    
                        $remainAmount = $budgetBreakdown->remain_amount - $reportExpensesDtl[0]->expenseAmount;
                    }
    
                    DB::table('budget_breakdowns')
                    ->where('id',  $budgetBreakdown['id'])
                    ->update(['remain_amount' => $remainAmount]);
    
            }
               
        }

    }
}
