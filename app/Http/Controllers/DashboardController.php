<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Proposal;

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }

    /**
     * Get All proposal count.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    { 
        try{

            $userId = Auth::user()->id;
            $userType = Auth::user()->user_type;

            $startDate = $request->get('startDate');
            $endDate = $request->get('endDate');

            if($userType == 1){
                $allProposals = Proposal::where('application_status','>=','1')
                                        ->where('user_id','=',$userId)
                                        //->whereBetween('date', [$startDate, $endDate])
                                        ->whereNotNull('date')
                                        ->get();
                $approveProposals = Proposal::where('application_status','5')
                                            ->where('user_id','=',$userId)
                                            //->whereBetween('date', [$startDate, $endDate])
                                            ->whereNotNull('date')
                                            ->get();
                $rejectProposals = Proposal::where('application_status','4')
                                            ->where('user_id','=',$userId)
                                            //->whereBetween('date', [$startDate, $endDate])
                                            ->whereNotNull('date')
                                            ->get();
                $evaluateProposals = Proposal::whereIn('application_status', ['2', '3'])
                                            ->where('user_id','=',$userId)
                                            //->whereBetween('date', [$startDate, $endDate])
                                            ->whereNotNull('date')
                                            ->get();

            }else{
                $allProposals = Proposal::where('application_status','>=','1')
                                            //->whereBetween('date', [$startDate, $endDate])
                                            ->whereNotNull('date')
                                            ->get();
                $approveProposals = Proposal::where('application_status','5')
                                            //->whereBetween('date', [$startDate, $endDate])
                                            ->whereNotNull('date')
                                            ->get();
                $rejectProposals = Proposal::where('application_status','4')
                                            //->whereBetween('date', [$startDate, $endDate])
                                            ->whereNotNull('date')
                                            ->get();
                $evaluateProposals = Proposal::whereIn('application_status', ['2', '3', '6'])
                                            //->whereBetween('date', [$startDate, $endDate])
                                            ->whereNotNull('date')
                                            ->get();
            }
            $proposalCount = $allProposals->count();
            $approveProposalCount = $approveProposals->count();
            $rejectProposalCount = $rejectProposals->count();

            $proposalCountArr = array(
                array("Title" => "proposalCount","Name" => "Total Proposals","NameEs" => "Propuestas totales", "Count" => $proposalCount),
                array("Title" => "approveCount","Name" => "Approved Proposals","NameEs" => "Propuestas aprobadas","Count" => $approveProposalCount),
                array("Title" => "rejectCount","Name" => "Rejected Proposals","NameEs" => "Propuestas rechazadas","Count" => $rejectProposalCount)
            );

            if($userType != 1){
                $evaluateProposalCount = $evaluateProposals->count();
                $proposalCountArr[] = array("Title" => "evaluateCount","Name" => "Under Evaluation","NameEs" => "Bajo evaluación","Count" => $evaluateProposalCount);
            }

            return response()->json([
                'response' => $proposalCountArr,
                'success' => true
            ], 200);
        }
        catch (\Exception $e) {
            $allErrors =  $e->getMessage();
            $message = $allErrors . ' '.'Please try again!';
            return response()->json([
                'message' => $message,
                'success' => false,
            ], 405);
        }
    }

   

    /**
     * Get Start proposal count datewise.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function datewiseProposal(Request $request)
    { 
        try{

            $userId = Auth::user()->id;
            $userType = Auth::user()->user_type;
            $startDate = $request->get('startDate');
            $endDate = $request->get('endDate');
            $graphType = $request->get('type');

            //select month(`date`), year(`date`), count(*) as total from `proposals` where `application_status` >= 1 and `date` is not null group by month(`date`), year(`date`) 

            $dtArr = $this->range_date($startDate, $endDate);

            $select = DB::table('proposals')->select( DB::raw('MONTHNAME(`date`) as month'), DB::raw('YEAR(`date`) as year') , DB::raw('count(*) as total'));

            if($userType == 1){
                $select->where(function ($query) use ($userId){
                    $query->where('user_id', '=', $userId);
                });
            }

            if($graphType == 'approveCount'){
                $select->where(function ($query){
                    $query->where('application_status', '=', '5');
                });
            } else if($graphType == 'rejectCount'){
                $select->where(function ($query){
                    $query->where('application_status', '=', '4');
                });
            } else if($graphType == 'evaluateCount'){
                $select->where(function ($query){
                    $query->whereIn('application_status', ['2', '3', '6']);
                });
            } else {
                $select->where(function ($query){
                    $query->where('application_status','>=','1');
                });
            }

            $datewiseProposals = $select->whereNotNull('date')
                                        ->groupBy(\DB::raw('MONTHNAME(date)'), DB::raw('YEAR(`date`)'))
                                        ->get();

            //return response()->json($datewiseProposals);

            $finalProposalArr = array();
            $proposalCount = array();
            $proposalArr = array();
            //$i = 0;
            foreach($datewiseProposals as $dVal){
                $proposalCount[$dVal->month . ' ' .$dVal->year]['date'] = $dVal->month . ' ' .$dVal->year;
                $proposalCount[$dVal->month . ' ' .$dVal->year]['total'] = $dVal->total;
                //$i++;
            }

            //$j = 0;
            foreach($dtArr as $dateVal){
                $proposalArr[$dateVal]['date'] = $dateVal;
                $proposalArr[$dateVal]['total'] = 0;
                //$j++;
            }

            //$tempProposalArr = array_merge($proposalArr,$proposalCount);
            $tempProposalArr = $proposalCount;

            foreach($tempProposalArr as $tempVal){
                $finalProposalArr[] = $tempVal;
            }

            //Return message
            return response()->json([
                'message' => $finalProposalArr,
                'success' => true,
            ], 200);
        }
        catch (\Exception $e) {
            $allErrors =  $e->getMessage();
            $message = $allErrors . ' '.'Please try again!';
            return response()->json([
                'message' => $message,
                'success' => false,
            ], 405);
        }
    }

    private function range_date($first, $last) {
        $arr = array();
        $now = strtotime($first);
        $last = strtotime($last);
      
        while($now <= $last ) {
          $arr[] = date('Y-m-d', $now);
          $now = strtotime('+1 day', $now);
        }
      
        return $arr;
    }


    /**
     * Get Start registration request count datewise.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getRegistrationRequestCount(Request $request){
        try{
            $startDate = $request->get('startDate');
            $endDate = $request->get('endDate');
            $user_type = $request->get('user_type');
            //$activeStatus = $request->get('activeStatus');

            /*if($startDate == '' || $endDate == ''){
                $startDate = '2019-10-31';
                $endDate = '2020-01-16';
            }*/

            $totalCount = DB::table('users')->select(DB::raw('count(*) as total'))
                                            ->where('user_type', '=', $user_type)
                                            ->whereBetween('created_at', [$startDate, $endDate])
                                            ->get();

            $activeCount = DB::table('users')->select(DB::raw('count(*) as total'))
                                            ->where('user_type', '=', $user_type)
                                            ->where('activeStatus', '=', 1) 
                                            ->whereBetween('created_at', [$startDate, $endDate])
                                            ->get();

            $inactiveCount = DB::table('users')->select(DB::raw('count(*) as total'))
                                            ->where('user_type', '=', $user_type)
                                            ->where('activeStatus', '=', 0) 
                                            ->whereBetween('created_at', [$startDate, $endDate])
                                            ->get();

            //Return message
            return response()->json([
                'totalCount' => $totalCount,
                'activeCount' => $activeCount,
                'inactiveCount' => $inactiveCount,
                'success' => true,
            ], 200);
        }catch (\Exception $e) {
            $allErrors =  $e->getMessage();
            $message = $allErrors . ' '.'Please try again!';
            return response()->json([
                'message' => $message,
                'success' => false,
            ], 405);
        }
    }
}