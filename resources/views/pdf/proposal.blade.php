<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Proposal Form</title>
</head>
<body style="padding:0; margin:0; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5;" width="5%">
            <img src="https://www.impactocomunitariopr.org/assets/images/logo.png" width="85" height="85">
          </td>
          <td align="center" valign="top" style="border-bottom:solid 1px #e5e5e5;" width="95%">
            <h1>Proposal Form</h1>
          </td>
        </tr>
    </table>
  </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding-top:20px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Entity</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Application Date</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Grant Requested</td>
        </tr>
        <tr>
          <td align="left" valign="top" width="70%" style="word-wrap:break-word;"><label>{{ $proposal['proposalUser']->entity_name }}</label></td>
          <td align="left" valign="top"><label>{{ $proposal->date != null ? date('m-d-Y', strtotime($proposal->date)) : '--' }}</label></td>
          <td align="left" valign="top"><label>{{ $proposal->requested_grant>0 ? '$'.$proposal->requested_grant : '--' }}</label></td>
        </tr>
      </table>
      </td>
  </tr>

  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <!-- <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Grant Suggested</td> -->
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Grant Final</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">&nbsp;</td>
        </tr>
        <tr>
          <!-- <td align="left" valign="top" width="50%"><label>{{ $proposal->donation_grant>0 ? '$'.$proposal->donation_grant : '--' }}</label></td> -->
          <td align="left" valign="top"  width="50%">
            <label>{{ $proposal->approved_grant>0 ? '$'.$proposal->approved_grant : '--' }}</label>
          </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">ID</td>
        </tr>
        <tr>
          <td align="left" valign="top"><label>{{ $proposal->proposal_unique_id }}</label></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="30" align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top" style="text-decoration:underline;"><h2>Start</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top"><label style="padding-bottom:10px; display:block;"><strong>Project</strong></label>
      <label>{{ $project->projectNameEn }}</label></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><label style="padding-bottom:10px; display:block;"><strong>City</strong></label>
      <label>{{ $city->city }}</label></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><strong style="font-size:18px;">Community or Population to Serve</strong>
      <p>Use the following categories to define the community to serve.</p>
      <label style="padding-bottom:10px; display:block;"><strong>Community</strong></label>
      <label>{{ $proposal->community }}</label>
  </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><div>
        <h4 style="padding:0 0 10px 0; margin:0;">Population</h4>
          @if ($proposal->categories === 0)
          <label>Male</label>
          @elseif ($proposal->categories === 1)
          <label>Female</label>
          @else
          <label>Both</label>
          @endif
      </div>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><strong style="font-size:18px;">Age Group</strong>
      <ul style="padding-left:0px;">
      @foreach($population as $plval)
        @if ($plval->population)
          <li style="list-style:none; display:inline-block;margin-bottom: 8px; margin-right: 10px;">{{ $plval->value }}</li>
        @endif
      @endforeach
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->population_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->population_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->population_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:30px; border-bottom:solid 1px #e5e5e5; padding-bottom:30px;">
        <tr>
          <td align="left" valign="top" width="70%" style="word-wrap:break-word;border-right:solid 1px #e5e5e5;"><h3>Necessity Statement</h3>
            <p>Describe the need and importance for the community program or services that your organization proposes herein. Make reference to credible (published) sources of information and data (qualitative and quantitative) to support your arguments.</p>
          </td>
          <!-- <td align="left" valign="top" style="padding:0 0 0 15px;"><ul style="padding:0 0 0 0;">
              <li style="list-style:none; padding-bottom:5px;"><a href="#" style="text-decoration:none; color:#282828;">Puerto Rico Statistics Institute</a></li>
              <li style="list-style:none; padding-bottom:5px;"><a href="#" style="text-decoration:none; color:#282828;">Substance Abuse Observatory</a></li>
              <li style="list-style:none;padding-bottom:5px;"><a href="#" style="text-decoration:none; color:#282828;">Youth Development Institute</a></li>
              <li style="list-style:none;"><a href="#" style="text-decoration:none; color:#282828;">PREHCO Project</a></li>
            </ul>
          </td> -->
        </tr>
        <tr>
          <td align="left" valign="top"style="word-wrap:break-word;border-right:solid 1px #e5e5e5;">
            <p>{{$proposal->necessity_statement}}</p>
          </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Classification</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Document</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'necessity_statement_file')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>Necessity Statement</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->necessity_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->necessity_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->necessity_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" width="70%">
            <h2>Programatic approach.</h2>
            <p>What programmatic investment approach will impact your program or service?</p>
            @if ($proposal->programmatic_approach === 'prog_opt1')
            <label>Strengthening Our Culture Artistic and cultural projects for children, youth and adults that increase opportunities for creative expression such as; classes, workshops and seminars on theater, dance, painting, design, music, poetry, writing, and local production of musical instruments in disadvantaged communities; and events focused on promoting and strengthening Puerto Rican culture.</label>
            @elseif ($proposal->programmatic_approach === 'prog_opt2')
            <label>Promotion of Positive and Healthy Behaviors Projects that promote the physical, emotional and mental well-being of disadvantaged individuals, groups, and communities; educational programs aimed at academic excellence and independent living; support services for individuals and families survivors of situations of violence and abuse; organized activities aimed at developing teamwork skills, healthy lifestyles and community service through sports; Community programs focused on the development of skills to reduce vulnerabilities to natural disasters and practices for the protection of natural and animal resources.</label>
            @elseif ($proposal->programmatic_approach === 'prog_opt3')
            <label>Boosting Financial Stability and Economic Development Projects that promote the development and economic sustainability of individuals, families and communities through legal advice, training on the proper management of finances, occupational skills or any other service that increases the employability of young people, adults and people of functional diversity; programs focused on job creation and the development of community industries.</label>
            @endif
            </td>
          <td align="left" valign="top" style="padding-left:15px;">Artistic and cultural projects for children, youth and adults that increase opportunities for creative expression, such as: classes, workshops, seminars, theater, dance, painting, music, poetry, writing, local production and musical instruments in disadvantaged communities and events focused on promoting and strengthening Puerto Rican culture.</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->programmatic_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->programmatic_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->programmatic_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="text-decoration:underline;"><h2>Pareo Registration</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding-top:20px;word-wrap:break-word;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Type</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Origin</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Category</td>
        </tr>
        <tr>
          <td align="left" valign="top"><label>{{ $pareo_types }}</label></td>
          <td align="left" valign="top" width="70%" style="word-wrap:break-word;"><label>{{ $proposal->pareo_origin}}</label></td>
          <td align="left" valign="top"><label>{{ $pareo_category}}</label></td>
        </tr>
      </table>
      </td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Pareo Calculation</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">Approved Grant</td>
          <td align="left" valign="top" style="font-weight: 600;font-size: 16px; padding-bottom: 15px;">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top" width="50%"><label>{{ $proposal->requested_grant>0 ? '$'. ($proposal->requested_grant * 20 )/100 : '--' }}</label></td>
          <td align="left" valign="top"  width="50%"><label>{{ $proposal->approved_grant>0 ? '$'.$proposal->approved_grant : '--' }}</label></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top" style="text-decoration:underline;"><h2>Program</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="62%" align="left" valign="top">What goal does your community service or program seek to achieve in the proposed community and population?</td>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><p style="padding-bottom:20px;">Goal: it is the end to which the wishes of the organization are directed. In projects, programs and interventions, the goals are the goals that are intended to be achieved in the long term through the efforts made. These goals must be linked to the vision and mission, since those that, together with the objectives, will guide the efforts towards the expected achievements.</p>
      <label>{{ $proposal->goal_to_achieve }}</label></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->goal_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->goal_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->goal_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding-top:15px;"><p style="font-size:18px;">Describe the changes and/or benefits that will be gained by the implementation of your community program or service. Demonstrate these changes using the measures selected by the NPO known as objectives.</p>
      <p>The objectives must be written using the SMART model. Results will be evaluated to determine if the objectives were met. Although different sources define SMART objectives in different ways, the following definition is commonly used:</p></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="5" cellpadding="5">
        <tr>
          <td align="left" valign="top"><ul style="padding:0;">
              <li style="list-style:none; padding-bottom:10px;"><span style="padding-left:10px;">Specific - target a specific area for change</span> </li>
              <li style="list-style:none;padding-bottom:10px;"> <span style="padding-left:10px;">S Measurable - quantify an indicator of progress</span> </li>
              <li style="list-style:none;padding-bottom:10px;"> <span style="padding-left:10px;">S Assignable - specify who will do it</span> </li>
            </ul></td>
          <td align="left" valign="top"><ul style="padding:0;">
              <li style="list-style:none; padding-bottom:10px;"> <span style="padding-left:10px;">Realistic - state realistic results to be achieved</span> </li>
              <li style="list-style:none;padding-bottom:10px;"><span style="padding-left:10px;">Time-related - specify when the results will be achieved</span> </li>
            </ul></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><label>{{ $proposal->changes_or_benefits }}</label></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->changes_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->changes_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->changes_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="5" cellpadding="5" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;">
        <tr>
          <td align="left" valign="top" width="45%" style="word-wrap:break-word;"><h4>Explain what specific activities will your community service or program do.</h4>
            <p>Activities include workshops, services, conferences, training, community surveys, home counseling, etc.; carried out to provide benefits to the community and population served.</p>
            <label>{{ $proposal->specific_activities }}</label>
            <ul style="padding:20px 0 0 0;">
                  @if($proposal->specific_status == 1)
                  <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
                  @endif
                  @if($proposal->specific_status == 2)
                  <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
                  @endif
                  @if($proposal->specific_status == 3)
                  <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
                  @endif
            </ul></td>
          <td align="left" valign="top" width="45%" style="word-wrap:break-word;"><h4>Explain who will be affected by your community program or service?</h4>
            <p style="padding-bottom: 16px;">Include participants, people, clients, agencies to whom your community service or program will benefit</p>
            <label>{{ $proposal->who_will_affected }}</label>
            <ul style="padding:20px 0 0 0;">
                  @if($proposal->affected_status == 1)
                  <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
                  @endif
                  @if($proposal->affected_status == 2)
                  <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
                  @endif
                  @if($proposal->affected_status == 3)
                  <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
                  @endif
            </ul></td>
        </tr>
      </table></td>
  </tr>

  <tr>
    <td align="left" valign="top">
      <table width="100%" border="0" cellspacing="5" cellpadding="5" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;">
        <tr>
          <td align="left" valign="top" width="45%" style="word-wrap:break-word;"><h4>Explain how will your proposed community program or service will pay for the development of the programmatic approach proposed by your organization.</h4>
            <label>{{ $proposal->how_will_pay }}</label>
            <ul style="padding:20px 0 0 0;">
                  @if($proposal->pay_status == 1)
                  <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
                  @endif
                  @if($proposal->pay_status == 2)
                  <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
                  @endif
                  @if($proposal->pay_status == 3)
                  <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
                  @endif
            </ul>
          </td>
          <td align="left" valign="top" width="45%" style="word-wrap:break-word;"><h4>Explain what kind of resources will be required by your community program or service.</h4>
            <p style="padding-bottom: 16px;">Resources are the means invested to allow us to carry out activities and obtain the results we want to achieve. For example, resources might include: personnel (employees, volunteers), materials, property, transportation, research and technology, to name a few.</p>
            <label>{{ $proposal->resources_required }}</label>
            <ul style="padding:20px 0 0 0;">
                  @if($proposal->resources_status == 1)
                  <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
                  @endif
                  @if($proposal->resources_status == 2)
                  <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
                  @endif
                  @if($proposal->resources_status == 3)
                  <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
                  @endif
            </ul>
          </td>
        </tr>
      </table>
    </td>
  </tr>

  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Classification</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Document</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'work_plan_file')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>Work Plan</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->work_plan_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->work_plan_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->work_plan_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Evaluations</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" width="50%" style="padding-right:15px;">

            <h4 style="padding-bottom:12px;">Does your organization evaluate programs by collecting and measuring relevant data?</h4>
            <label>{{ $proposal->collecting_relevant_data }}</label>
            <ul style="padding:20px 0 20px 0;">
              @if($proposal->collecting_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
              @endif
              @if($proposal->collecting_status == 2)
              <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
              @endif
              @if($proposal->collecting_status == 3)
              <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
              @endif
            </ul>
            <h4 style="padding-bottom:12px;">Who in your organization evaluates the programs, policies and practices?</h4>
            <label>{{ $proposal->who_evaluates }}</label>
            <ul style="padding:20px 0 20px 0;">
              @if($proposal->who_evaluates_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
              @endif
              @if($proposal->who_evaluates_status == 2)
              <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
              @endif
              @if($proposal->who_evaluates_status == 3)
              <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
              @endif
            </ul></td>
          <td align="left" valign="top" width="50%">
            <h4>How often does your organization evaluate its programs and services?</h4>
            <label>{{ $proposal->how_often_organization_evaluates }}</label>
            <ul style="padding:20px 0 20px 0;">
              @if($proposal->organization_evaluates_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
              @endif
              @if($proposal->organization_evaluates_status == 2)
              <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
              @endif
              @if($proposal->organization_evaluates_status == 3)
              <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
              @endif
            </ul></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><h4>Which of the following statements best describes the type of evaluations that are carried out in your organization? Check all that apply.</h4>
      <ul style="padding-left:0px;">
        @if ($evaluations)
          @foreach($evaluations as $evval)
            @if ($evval->typeevaluation)
              <li>{{ $evval->value }}</li>
            @endif
          @endforeach
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->statements_describes_type_of_evaluations_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->statements_describes_type_of_evaluations_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->statements_describes_type_of_evaluations_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="1px solid" cellspacing="5" cellpadding="5">
        <tr>
          <td align="left" valign="top"><h4 style="margin-top:0px;">Type of evaluation to use.</h4>
            <label>{{ $proposal->type_of_evaluation }}</label>
            <ul style="padding:20px 0 20px 0;">
              @if($proposal->type_of_evaluation_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
              @endif
              @if($proposal->type_of_evaluation_status == 2)
              <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
              @endif
              @if($proposal->type_of_evaluation_status == 3)
              <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
              @endif
            </ul></td>
          <td align="left" valign="top"><h4 style="margin-top:0px;">Type of data to be collected.</h4>
            <label>{{ $proposal->type_of_data }}</label>
            <ul style="padding:20px 0 20px 0;">
            @if($proposal->type_of_data_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
              @endif
              @if($proposal->type_of_data_status == 2)
              <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
              @endif
              @if($proposal->type_of_data_status == 3)
              <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
              @endif
            </ul></td>
          <td align="left" valign="top"><h4 style="margin-top:0px;">Type of evaluator to use.</h4>
            <label>{{ $proposal->type_of_evaluator }}</label>
            <ul style="padding:20px 0 20px 0;">
            @if($proposal->type_of_evaluator_status == 1)
              <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
              @endif
              @if($proposal->type_of_evaluator_status == 2)
              <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
              @endif
              @if($proposal->type_of_evaluator_status == 3)
              <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
              @endif
            </ul></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Property</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: solid 1px #e5e5e5; padding-bottom:20px;">
        <tr>
          <td align="left" valign="top" style="padding-right:15px;"><h4>Inventory of property, material and equipment required to offer the services</h4>
          </td>

        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'property_inventory_file')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;">
              <span style="color: #013b85; border-bottom: 1px solid #ddd;">Classification</span><br>
              {{ $necessity_statement_file->desc}}</td>

            @if ($necessity_statement_file->document_type == 'property_inventory_file' && $necessity_statement_file->monthly_payment !='')
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;">
                <span style="color: #013b85; border-bottom: 1px solid #ddd;">Monthly payment</span><br>{{'$'.$necessity_statement_file->monthly_payment}}
              </td>
            @endif

            @if ($necessity_statement_file->document_type == 'property_inventory_file' && $necessity_statement_file->monthly_payment !='' && ($necessity_statement_file->rented  == 1 || $necessity_statement_file->rented  == 0) )
              <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;">
                <span style="color: #013b85; border-bottom: 1px solid #ddd;">Rented</span><br>{{$necessity_statement_file->rented == 1 ? "Yes"  : "No"}}
              </td>
            @endif

             @if ($necessity_statement_file->document_type == 'property_inventory_file'  && $necessity_statement_file->org_file_name !='')
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;">
              <span style="color: #013b85; border-bottom: 1px solid #ddd;">Document</span><br>
              {{ $necessity_statement_file->org_file_name }}
            </td>
            @endif
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->inventory_file_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->inventory_file_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->inventory_file_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Budget</h2></td>
  </tr>
  <tr>
    <td style="color:#007bff; font-size:22px; padding-top:20px;"> Administrative </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Classification</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Document</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'adm_expence_file')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->desc}}</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td style="line-height:21px; padding-top:10px; padding-bottom:20px;">The budget narrative clearly explains each item of expenditure requested.It must break down the budget presented in detail by employee/ salary, units/ materials, equipment to be purchased, activities, etc., including the cost to be covered and the time in which it will be developed. You must break down the budget by item. Administrative/Operational Expenses cannot exceed 40% of the total Budget. The salary for the position of Director, Administrator or person in charge of the daily operations of the NPO shall not exceed fifty 50% of the expenses adm.</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%">
        <thead>
          <tr>
            <th width="50%" align="left" style="color: #013b85;">DESCRIPTION OF THE EXPENDITURE ITEM</th>
            <th width="25%" align="left" style="color: #013b85;">ESTIMATED EXPENSE</th>
            <th width="25%" align="left" style="color: #013b85;">TIGHT BUDGET</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['budgetBreakdown'] as $bbVal)
          @if($bbVal->budget_type == 'Administrative')
          <tr>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->description }}</label></td>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd;padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->budget_amount>0 ? '$'.$bbVal->budget_amount : '--' }}</label></td>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->tight_budget>0 ? '$'.$bbVal->tight_budget : '--' }}</label></td>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->administrative_budget_file_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->administrative_budget_file_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->administrative_budget_file_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="color:#007bff; font-size:22px; padding-top:20px;">Direct</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Classification</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Document</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'direct_expence_file')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->desc}}</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
    </td>
  </tr>
  <tr>
    <td style="line-height:21px; padding-top:10px; padding-bottom:20px;">The budget narrative clearly explains each item of expenditure requested.It must break down the budget presented in detail by employee/ salary, units/ materials, equipment to be purchased,activities, etc., including the cost to be covered and the time in which it will be developed. You must break down the budget by item.Remember that the proposed Direct Expenses must add a minimum of 60% of the total Budget.</td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%">
        <thead>
          <tr>
            <th width="50%" align="left" style="color: #013b85;">DESCRIPTION OF THE EXPENDITURE ITEM</th>
            <th width="25%" align="left" style="color: #013b85;">ESTIMATED EXPENSE</th>
            <th width="25%" align="left" style="color: #013b85;">TIGHT BUDGET</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['budgetBreakdown'] as $bbVal)
          @if($bbVal->budget_type == 'Direct')
          <tr>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->description }}</label></td>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd;padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->budget_amount>0 ? '$'.$bbVal->budget_amount : '--' }}</label></td>
            <td style="border-bottom: 1px solid #ddd; border-top: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $bbVal->tight_budget>0 ? '$'.$bbVal->tight_budget : '--' }}</label></td>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->direct_budget_file_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->direct_budget_file_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->direct_budget_file_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Legal</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <ul style="padding-left:0px;">
      @foreach ($legalArr as $key => $v)
          <li>{{ $v }}</li>
      @endforeach
      </ul></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="border-bottom:solid 1px #e5e5e5; padding-bottom:20px;"><ul style="padding:0;">
        @if($proposal->legal_status == 1)
        <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
        @endif
        @if($proposal->legal_status == 2)
        <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
        @endif
        @if($proposal->legal_status == 3)
        <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
        @endif
      </ul></td>
  </tr>



  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Requirements</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: solid 1px #e5e5e5; padding-bottom:20px;">
        <tr>
          <td align="left" valign="top" style="padding-right:15px;"><h4>Requirements for the Request for Proposal.Some documents can be requested at www.pr.gov</h4></td>
        </tr>
      </table>
      </td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Classification</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Document</th>
          </tr>
        </thead>
        <tbody>
        @foreach($proposal['proposalDocuments'] as $necessity_statement_file)
          @if ($necessity_statement_file->document_type == 'puerto_rico_state_department')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>DEPARTAMENTO DE ESTADO DE PUERTO RICO</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'internal_revenue_services')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>INTERNAL REVENUE SERVICES (IRS)</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'puerto_rico_state_insurance_fund_corporation')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>CORPORACIÓN DEL FONDO DEL SEGURO DEL ESTADO DE PUERTO RICO</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'municipal_income_collection_center')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>CENTRO DE RECAUDACIONES DE INGRESOS MUNICIPALES</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'department_of_finance')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>DEPARTAMENTO DE HACIENDA</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'department_of_labor_and_human_resources')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>DEPARTAMENTO DEL TRABAJO Y RECURSOS HUMANOS</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'health_department')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>DEPARTAMENTO DE SALUD</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'administration_of_regulations_and_permits')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>ADMINISTRACIÓN DE REGLAMENTOS Y PERMISOS</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'puerto_rico_fire_fighter_body')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>CUERPO DE BOMBEROS DE PUERTO RICO</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'custody_agency')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>AGENCIA CUSTODIO</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'additional_documents')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>DOCUMENTOS ADICIONALES</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'seguro_social')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>SEGURO SOCIAL</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'administration_for_child_support')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>ADMINISTRACION PARA EL SUSTENTO DE MENORES (ASUME)</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'puerto_rico_police')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>POLICIA DE PUERTO RICO</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'negotiated_conventions_of_san_jun')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>NEGOCIADO DE CONVENCIONES DE SAN JUN, PR</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'higher_education_council')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>CONSEJO SUPERIOR DE EDUCACION</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
          @if ($necessity_statement_file->document_type == 'osfl')
          <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>OSFL</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{ $necessity_statement_file->org_file_name }}</label>
          </tr>
          @endif
        @endforeach
        </tbody>
      </table>
      <ul style="padding:20px 0 20px 0;">
          @if($proposal->requirement_file_status == 1)
          <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
          @endif
          @if($proposal->requirement_file_status == 2)
          <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
          @endif
          @if($proposal->requirement_file_status == 3)
          <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
          @endif
      </ul>
    </td>
  </tr>
<tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Evidence</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: solid 1px #e5e5e5; padding-bottom:20px;">
        <tr>
          <td align="left" valign="top" style="padding-right:15px;"><h4>Records of Other Documents for evidence. Required documents must be incorporated.</h4></td>
        </tr>
      </table>
      </td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <table width="100%">
        <thead>
          <tr>
            <th width="35%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Classification</th>
            <th width="65%" align="left" style="color: #013b85; border-bottom: 1px solid #ddd;">Document</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($evidenceArr as $k => $v)
            <tr>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{$k}}</label></td>
            <td style="border-bottom: 1px solid #ddd; padding-top:15px; padding-bottom:15px;"><label>{{$v}}</label>
          </tr>
        @endforeach
        </tbody>
      </table>
      <ul style="padding:20px 0 20px 0;">
          @if($proposal->evidence_doc_status == 1)
          <li style="list-style:none; display:inline-block;"><b>Compiles</b></li>
          @endif
          @if($proposal->evidence_doc_status == 2)
          <li style="list-style:none; display:inline-block;"><b>Does not comply</b></li>
          @endif
          @if($proposal->evidence_doc_status == 3)
          <li style="list-style:none; display:inline-block;"><b>Not applicable</b></li>
          @endif
      </ul>
    </td>
  </tr>



  <tr>
    <td align="left" valign="top"><h2 style="text-decoration:underline;">Submit</h2></td>
  </tr>
  <tr>
    <td align="left" valign="top" style="padding-bottom:20px;"><h3>Submit</h3>
      <input type="checkbox" />
      I certify that the information I provided for this proposal is correct and consists of my own personal knowledge for all legal and relevant purposes under penalty or perjury. </td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="left" valign="top" style="padding-right:15px;" width="60%"><label style="padding-bottom:10px; display:block;"><strong>Name</strong></label>
            <label>{{ $proposal->name != null ? $proposal->name : ''  }}</label></td>
          <td align="left" valign="top" style="padding-left:15px;"><label style="padding-bottom:10px; display:block;"><strong>Date</strong></label>
            <label>{{ $proposal->date != null ? date('m-d-Y', strtotime($proposal->date)) : '' }}</label></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
