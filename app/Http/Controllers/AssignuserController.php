<?php 
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\AssignUserToProposal;

class AssignuserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');    
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assignUser = AssignUserToProposal::orderBy('id', 'DESC')->paginate(10);
        return response()->json($assignUser);
    }

     /**
     * Assign user to project.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'proposal_id' => 'required',
            'user_id' => 'required',
            'createdBy' => 'required'
        ]);

        \DB::beginTransaction();
        try {
            /* $proposal_id = 1;
            $user_id = 3;
            $assignUsers = AssignUserToProposal::
                            where('proposal_id', '=', $proposal_id)
                            ->where('user_id', '!=', $user_id)
                            ->get();

            if(!empty($assignUsers)){
                foreach($assignUsers as $assignVal){
                     \DB::table('assign_user_to_proposals')
                        ->where('id', $assignVal['id'])
                        ->update(['status' => 0]); 
                    AssignUserToProposal::find($assignVal['id'])->update(['status' => 0]);
                }
            }

            $alredayAssignUser = AssignUserToProposal::
                                where('proposal_id', '=', $proposal_id)
                                ->where('user_id', '=', $user_id)
                                ->get()
                                ->pluck('user_id');
            */
            //if(empty($alredayAssignUser)){
                $model = New AssignUserToProposal;

                $model->proposal_id = $request->input('proposal_id');
                $model->user_id     = $request->input('user_id');
                $model->createdBy   = $request->input('createdBy');
                $model->status      = 1;
                $model->created_at  = date('Y-m-d H:i:s');
                $model->updated_at  = date('Y-m-d H:i:s');

                $model->save();
                $modelId = $model->id;
            //}

            \DB::commit();
            $message = 'Proposal has been assinged successfully!';
            //$message = $alredayAssignUser;
            $alerttype = true;
        }
        catch (\Exception $e) {
            \ DB::rollback();
            $allErrors =  $e->getMessage();

            $message = $allErrors.' '.'Please try again!';
            $alerttype = false;

        }

        //Return message
        return response()->json([
            'message' => $message,
            'success' => $alerttype,
        ], 201);
    }

    public function show($project_id)
    {
        $assignUser = AssignUserToProposal::where('proposal_id', '=', $proposal_id);
        return response()->json([
            'res' => $assignUser,
            'success' => $alerttype,
        ], 201);
    }

}