<div style="float:left; width:100%; background:#EEEEEE;padding:30px 0;">
     <div style="max-width:100%; width:600px; padding:0px; margin:0 auto;">
        <div style="width:100%; background:#1F2C47; padding:20px 20px; float:left;"><img src="{{ $logo }}" width="80" height="80" style="border-radius:50%">
        </div>
        <div style="font-size:15px;float:left; width:100%; background:#fff; font-family:Tahoma, Geneva, sans-serif; text-align:left; color:#1F2C47; padding:20px;">
            <p>Congrats {{ $entity_name }}</p>
            <p><b>${{ $approved_amount }}</b> grant alloted for your proposal <b>{{ $proposal_id }}</b> from the project <b>{{ $project_name }}</b>. Since your requested Grant amount <b>${{ $requested_amount }}</b> was greater than the approved amount you are requested to adjust the expenditure so that the grant gets final approval. Please take immediate measures to update your expenditure budget.</p>

            <p>Thanks and regards,<br />
          	 Admin</p>
        </div>
        <div style="width:100%; background:#1F2C47; padding:30px 20px; float:left; text-align:center;color:#989898">
            <p>© Copyright 2019-20 CECFL - All Rights Reserved</p>
        </div>
     </div>
</div>