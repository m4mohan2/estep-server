<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOrgFileNameToProposalDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('proposal_documents', function (Blueprint $table) {
            $table->string('org_file_name')->nullable()->after('desc')->comment('Original File Name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('proposal_documents', function (Blueprint $table) {
            $table->dropColumn('org_file_name');
        });
    }
}
