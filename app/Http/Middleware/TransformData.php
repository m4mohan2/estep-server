<?php
namespace App\Http\Middleware;
use Closure;
use Symfony\Component\HttpFoundation\ParameterBag;


class TransformData
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
        /*if ($request->isJson()) {
            $this->clean($request->json());
        } else {
            $this->clean($request->request);
        }*/
        $decrypted = $this->decrypt($request->EncryptionData);
        return $next($decrypted);
    }
    /**
    * Clean the request's data by removing mask from phonenumber.
    *
    * @param  \Symfony\Component\HttpFoundation\ParameterBag  $bag
    * @return void
    */
    private function clean(ParameterBag $bag)
    {
        $bag->replace($this->cleanData($bag->all()));
    }
    /**
    * Check the parameters and clean the number
    *
    * @param  array  $data
    * @return array
    */
    private function cleanData(array $data)
    {
        return collect($data)->map(function ($value, $key) {
            if ($key == 'payment_amount') {
                return preg_replace("/([^0-9])/", '', $value);
            } else {
                return $value;
            }
        })->all();
    }

    protected function evpKDF($password, $salt, $keySize = 8, $ivSize = 4, $iterations = 1, $hashAlgorithm = "md5") {
      $targetKeySize = $keySize + $ivSize;
      $derivedBytes = "";
      $numberOfDerivedWords = 0;
      $block = NULL;
      $hasher = hash_init($hashAlgorithm);
      while ($numberOfDerivedWords < $targetKeySize) {
          if ($block != NULL) {
              hash_update($hasher, $block);
          }
          hash_update($hasher, $password);
          hash_update($hasher, $salt);
          $block = hash_final($hasher, TRUE);
          $hasher = hash_init($hashAlgorithm);

          // Iterations
          for ($i = 1; $i < $iterations; $i++) {
              hash_update($hasher, $block);
              $block = hash_final($hasher, TRUE);
              $hasher = hash_init($hashAlgorithm);
          }

          $derivedBytes .= substr($block, 0, min(strlen($block), ($targetKeySize - $numberOfDerivedWords) * 4));

          $numberOfDerivedWords += strlen($block)/4;
      }

      return array(
          "key" => substr($derivedBytes, 0, $keySize * 4),
          "iv"  => substr($derivedBytes, $keySize * 4, $ivSize * 4)
      );
    }

    protected function decrypt($ciphertext) {
        $password = env('ENC_KEY', '');
        $ciphertext = base64_decode($ciphertext);
        if (substr($ciphertext, 0, 8) != "Salted__") {
            return false;
        }
        $salt = substr($ciphertext, 8, 8);
        $keyAndIV = $this->evpKDF($password, $salt);
        $decryptPassword = openssl_decrypt(
                substr($ciphertext, 16), 
                "aes-256-cbc",
                $keyAndIV["key"], 
                OPENSSL_RAW_DATA, // base64 was already decoded
                $keyAndIV["iv"]);

        return $decryptPassword;
    }
}